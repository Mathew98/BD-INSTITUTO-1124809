-- MySQL dump 10.13  Distrib 5.5.24, for Win32 (x86)
--
-- Host: localhost    Database: instituto
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acta`
--

DROP TABLE IF EXISTS `acta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acta` (
  `Cod_acta` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkcodasignatura` bigint(20) DEFAULT NULL,
  `fkdocalumno` bigint(20) DEFAULT NULL,
  `fkcodestadoacta` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkcodempleado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_acta`),
  KEY `actaalumno` (`fkdocalumno`),
  KEY `actaestadoacta` (`fkcodestadoacta`),
  KEY `actaestado` (`fkcodestado`),
  KEY `actaempleado` (`fkcodempleado`),
  CONSTRAINT `actaalumno` FOREIGN KEY (`fkdocalumno`) REFERENCES `alumno` (`Doc_alumno`),
  CONSTRAINT `actaempleado` FOREIGN KEY (`fkcodempleado`) REFERENCES `empleado` (`Cod_empleado`),
  CONSTRAINT `actaestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `actaestadoacta` FOREIGN KEY (`fkcodestadoacta`) REFERENCES `estadoacta` (`Cod_estado_acta`)
) ENGINE=InnoDB AUTO_INCREMENT=607 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acta`
--

LOCK TABLES `acta` WRITE;
/*!40000 ALTER TABLE `acta` DISABLE KEYS */;
INSERT INTO `acta` VALUES (303,3,102542,3,1,298),(304,4,102543,4,2,297),(305,5,102544,1,1,296),(306,6,102545,2,2,295),(307,7,102546,3,1,294),(308,8,102547,4,2,293),(309,9,102548,1,1,292),(310,10,102549,2,2,291),(311,11,1025410,3,1,290),(312,12,1025411,4,2,289),(313,13,1025412,1,1,288),(314,14,1025413,2,2,287),(315,15,1025414,3,1,286),(316,16,1025415,4,2,285),(317,17,1025416,1,1,284),(318,18,1025417,2,2,283),(319,19,1025418,3,1,282),(320,20,1025419,4,2,281),(321,21,1025420,1,1,280),(322,22,1025421,2,2,279),(323,23,1025422,3,1,278),(324,24,1025423,4,2,277),(325,25,1025424,1,1,276),(326,26,1025425,2,2,275),(327,27,1025426,3,1,274),(328,28,1025427,4,2,273),(329,29,1025428,1,1,272),(330,30,1025429,2,2,271),(331,31,1025430,3,1,270),(332,32,1025431,4,2,269),(333,33,1025432,1,1,268),(334,34,1025433,2,2,267),(335,35,1025434,3,1,266),(336,36,1025435,4,2,265),(337,37,1025436,1,1,264),(338,38,1025437,2,2,263),(339,39,1025438,3,1,262),(340,40,1025439,4,2,261),(341,41,1025440,1,1,260),(342,42,1025441,2,2,259),(343,43,1025442,3,1,258),(344,44,1025443,4,2,257),(345,45,1025444,1,1,256),(346,46,1025445,2,2,255),(347,47,1025446,3,1,254),(348,48,1025447,4,2,253),(349,49,1025448,1,1,252),(350,50,1025449,2,2,251),(351,51,1025450,3,1,250),(352,52,1025451,4,2,249),(353,53,1025452,1,1,248),(354,54,1025453,2,2,247),(355,55,1025454,3,1,246),(356,56,1025455,4,2,245),(357,57,1025456,1,1,244),(358,58,1025457,2,2,243),(359,59,1025458,3,1,242),(360,60,1025459,4,2,241),(361,61,1025460,1,1,240),(362,62,1025461,2,2,239),(363,63,1025462,3,1,238),(364,64,1025463,4,2,237),(365,65,1025464,1,1,236),(366,66,1025465,2,2,235),(367,67,1025466,3,1,234),(368,68,1025467,4,2,233),(369,69,1025468,1,1,232),(370,70,1025469,2,2,231),(371,71,1025470,3,1,230),(372,72,1025471,4,2,229),(373,73,1025472,1,1,228),(374,74,1025473,2,2,227),(375,75,1025474,3,1,226),(376,76,1025475,4,2,225),(377,77,1025476,1,1,224),(378,78,1025477,2,2,223),(379,79,1025478,3,1,222),(380,80,1025479,4,2,221),(381,81,1025480,1,1,220),(382,82,1025481,2,2,219),(383,83,1025482,3,1,218),(384,84,1025483,4,2,217),(385,85,1025484,1,1,216),(386,86,1025485,2,2,215),(387,87,1025486,3,1,214),(388,88,1025487,4,2,213),(389,89,1025488,1,1,212),(390,90,1025489,2,2,211),(391,91,1025490,3,1,210),(392,92,1025491,4,2,209),(393,93,1025492,1,1,208),(394,94,1025493,2,2,207),(395,95,1025494,3,1,206),(396,96,1025495,4,2,205),(397,97,1025496,1,1,204),(398,98,1025497,2,2,203),(399,99,1025498,3,1,202),(400,100,1025499,4,2,201),(401,101,10254100,1,1,200),(402,102,10254101,2,2,199),(403,103,10254102,3,1,198),(404,104,10254103,4,2,197),(405,105,10254104,1,1,196),(406,106,10254105,2,2,195),(407,107,10254106,3,1,194),(408,108,10254107,4,2,193),(409,109,10254108,1,1,192),(410,110,10254109,2,2,191),(411,111,10254110,3,1,190),(412,112,10254111,4,2,189),(413,113,10254112,1,1,188),(414,114,10254113,2,2,187),(415,115,10254114,3,1,186),(416,116,10254115,4,2,185),(417,117,10254116,1,1,184),(418,118,10254117,2,2,183),(419,119,10254118,3,1,182),(420,120,10254119,4,2,181),(421,121,10254120,1,1,180),(422,122,10254121,2,2,179),(423,123,10254122,3,1,178),(424,124,10254123,4,2,177),(425,125,10254124,1,1,176),(426,126,10254125,2,2,175),(427,127,10254126,3,1,174),(428,128,10254127,4,2,173),(429,129,10254128,1,1,172),(430,130,10254129,2,2,171),(431,131,10254130,3,1,170),(432,132,10254131,4,2,169),(433,133,10254132,1,1,168),(434,134,10254133,2,2,167),(435,135,10254134,3,1,166),(436,136,10254135,4,2,165),(437,137,10254136,1,1,164),(438,138,10254137,2,2,163),(439,139,10254138,3,1,162),(440,140,10254139,4,2,161),(441,141,10254140,1,1,160),(442,142,10254141,2,2,159),(443,143,10254142,3,1,158),(444,144,10254143,4,2,157),(445,145,10254144,1,1,156),(446,146,10254145,2,2,155),(447,147,10254146,3,1,154),(448,148,10254147,4,2,153),(449,149,10254148,1,1,152),(450,150,10254149,2,2,151),(451,151,10254150,3,1,150),(452,152,10254151,4,2,149),(453,153,10254152,1,1,148),(454,154,10254153,2,2,147),(455,155,10254154,3,1,146),(456,156,10254155,4,2,145),(457,157,10254156,1,1,144),(458,158,10254157,2,2,143),(459,159,10254158,3,1,142),(460,160,10254159,4,2,141),(461,161,10254160,1,1,140),(462,162,10254161,2,2,139),(463,163,10254162,3,1,138),(464,164,10254163,4,2,137),(465,165,10254164,1,1,136),(466,166,10254165,2,2,135),(467,167,10254166,3,1,134),(468,168,10254167,4,2,133),(469,169,10254168,1,1,132),(470,170,10254169,2,2,131),(471,171,10254170,3,1,130),(472,172,10254171,4,2,129),(473,173,10254172,1,1,128),(474,174,10254173,2,2,127),(475,175,10254174,3,1,126),(476,176,10254175,4,2,125),(477,177,10254176,1,1,124),(478,178,10254177,2,2,123),(479,179,10254178,3,1,122),(480,180,10254179,4,2,121),(481,181,10254180,1,1,120),(482,182,10254181,2,2,119),(483,183,10254182,3,1,118),(484,184,10254183,4,2,117),(485,185,10254184,1,1,116),(486,186,10254185,2,2,115),(487,187,10254186,3,1,114),(488,188,10254187,4,2,113),(489,189,10254188,1,1,112),(490,190,10254189,2,2,111),(491,191,10254190,3,1,110),(492,192,10254191,4,2,109),(493,193,10254192,1,1,108),(494,194,10254193,2,2,107),(495,195,10254194,3,1,106),(496,196,10254195,4,2,105),(497,197,10254196,1,1,104),(498,198,10254197,2,2,103),(499,199,10254198,3,1,102),(500,200,10254199,4,2,101),(501,201,10254200,1,1,100),(502,202,10254201,2,2,99),(503,203,10254202,3,1,98),(504,204,10254203,4,2,97),(505,205,10254204,1,1,96),(506,206,10254205,2,2,95),(507,207,10254206,3,1,94),(508,208,10254207,4,2,93),(509,209,10254208,1,1,92),(510,210,10254209,2,2,91),(511,211,10254210,3,1,90),(512,212,10254211,4,2,89),(513,213,10254212,1,1,88),(514,214,10254213,2,2,87),(515,215,10254214,3,1,86),(516,216,10254215,4,2,85),(517,217,10254216,1,1,84),(518,218,10254217,2,2,83),(519,219,10254218,3,1,82),(520,220,10254219,4,2,81),(521,221,10254220,1,1,80),(522,222,10254221,2,2,79),(523,223,10254222,3,1,78),(524,224,10254223,4,2,77),(525,225,10254224,1,1,76),(526,226,10254225,2,2,75),(527,227,10254226,3,1,74),(528,228,10254227,4,2,73),(529,229,10254228,1,1,72),(530,230,10254229,2,2,71),(531,231,10254230,3,1,70),(532,232,10254231,4,2,69),(533,233,10254232,1,1,68),(534,234,10254233,2,2,67),(535,235,10254234,3,1,66),(536,236,10254235,4,2,65),(537,237,10254236,1,1,64),(538,238,10254237,2,2,63),(539,239,10254238,3,1,62),(540,240,10254239,4,2,61),(541,241,10254240,1,1,60),(542,242,10254241,2,2,59),(543,243,10254242,3,1,58),(544,244,10254243,4,2,57),(545,245,10254244,1,1,56),(546,246,10254245,2,2,55),(547,247,10254246,3,1,54),(548,248,10254247,4,2,53),(549,249,10254248,1,1,52),(550,250,10254249,2,2,51),(551,251,10254250,3,1,50),(552,252,10254251,4,2,49),(553,253,10254252,1,1,48),(554,254,10254253,2,2,47),(555,255,10254254,3,1,46),(556,256,10254255,4,2,45),(557,257,10254256,1,1,44),(558,258,10254257,2,2,43),(559,259,10254258,3,1,42),(562,262,10254261,2,2,39),(563,263,10254262,3,1,38),(564,264,10254263,4,2,37),(565,265,10254264,1,1,36),(566,266,10254265,2,2,35),(567,267,10254266,3,1,34),(568,268,10254267,4,2,33),(569,269,10254268,1,1,32),(570,270,10254269,2,2,31),(571,271,10254270,3,1,30),(572,272,10254271,4,2,29),(573,273,10254272,1,1,28),(574,274,10254273,2,2,27),(575,275,10254274,3,1,26),(576,276,10254275,4,2,25),(577,277,10254276,1,1,24),(578,278,10254277,2,2,23),(579,279,10254278,3,1,22),(580,280,10254279,4,2,21),(581,281,10254280,1,1,20),(582,282,10254281,2,2,19),(583,283,10254282,3,1,18),(584,284,10254283,4,2,17),(585,285,10254284,1,1,16),(586,286,10254285,2,2,15),(587,287,10254286,3,1,14),(588,288,10254287,4,2,13),(589,289,10254288,1,1,12),(590,290,10254289,2,2,11),(591,291,10254290,3,1,10),(592,292,10254291,4,2,9),(593,293,10254292,1,1,8),(594,294,10254293,2,2,7),(595,295,10254294,3,1,6),(596,296,10254295,4,2,5),(597,297,10254296,1,1,4),(598,298,10254297,2,2,3),(599,299,10254298,3,1,2),(600,300,10254299,4,2,1),(603,3,102542,3,1,298),(604,4,102543,4,2,297),(605,5,102544,1,1,296),(606,6,102545,2,2,295);
/*!40000 ALTER TABLE `acta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `Doc_alumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_alumno` varchar(30) DEFAULT NULL,
  `Apellido_alumno` varchar(30) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `correo` varchar(70) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkcodestadoalumno` bigint(20) DEFAULT NULL,
  `fkcodmatricula` bigint(20) DEFAULT NULL,
  `fkcodciudad` bigint(20) DEFAULT NULL,
  `fkcodtipodoc` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Doc_alumno`),
  KEY `alumnoestado` (`fkcodestado`),
  KEY `alumnoestadoalumno` (`fkcodestadoalumno`),
  KEY `alumnomatricula` (`fkcodmatricula`),
  KEY `alumnociudad` (`fkcodciudad`),
  KEY `alumnotipodoc` (`fkcodtipodoc`),
  CONSTRAINT `alumnociudad` FOREIGN KEY (`fkcodciudad`) REFERENCES `ciudad` (`cod_ciudad`),
  CONSTRAINT `alumnoestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `alumnoestadoalumno` FOREIGN KEY (`fkcodestadoalumno`) REFERENCES `estadoalumno` (`Cod_estado_alumno`),
  CONSTRAINT `alumnomatricula` FOREIGN KEY (`fkcodmatricula`) REFERENCES `matricula` (`Cod_matricula`),
  CONSTRAINT `alumnotipodoc` FOREIGN KEY (`fkcodtipodoc`) REFERENCES `tipodocumento` (`Cod_tipo_doc`)
) ENGINE=InnoDB AUTO_INCREMENT=10254304 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (102542,'Sebasti?n','Josu?',3867001,'alummno2@instituto.com',2,2,2,2,2),(102543,'Alejandro','Crist?bal',3867002,'alummno3@instituto.com',1,3,3,3,3),(102544,'Mat?as','Ciro',3867003,'alummno4@instituto.com',2,4,4,4,1),(102545,'Diego','JuanDavid',3867004,'alummno5@instituto.com',1,5,5,5,2),(102546,'Samuel','GARCIA',3867005,'alummno6@instituto.com',2,6,6,6,3),(102547,'Nicol?s','GONZALEZ',3867006,'alummno7@instituto.com',1,1,7,7,1),(102548,'Daniel','RODRIGUEZ',3867007,'alummno8@instituto.com',2,2,8,8,2),(102549,'Mart?n','FERNANDEZ',3867008,'alummno9@instituto.com',1,3,9,9,3),(1025410,'Benjam?n','LOPEZ',3867009,'alummno10@instituto.com',2,4,10,10,1),(1025411,'Emiliano','MARTINEZ',3867010,'alummno11@instituto.com',1,5,11,11,2),(1025412,'Leonardo','SANCHEZ',3867011,'alummno12@instituto.com',2,6,12,12,3),(1025413,'Joaqu?n','EREZ',3867012,'alummno13@instituto.com',1,1,13,13,1),(1025414,'Lucas','GOMEZ',3867013,'alummno14@instituto.com',2,2,14,14,2),(1025415,'Iker','MARTIN',3867014,'alummno15@instituto.com',1,3,15,15,3),(1025416,'Gabriel','JIMENEZ',3867015,'alummno16@instituto.com',2,4,16,16,1),(1025417,'Thiago','RUIZ',3867016,'alummno17@instituto.com',1,5,17,17,2),(1025418,'Adri?n','HERNANDEZ',3867017,'alummno18@instituto.com',2,6,18,18,3),(1025419,'Bruno','DIAZ',3867018,'alummno19@instituto.com',1,1,19,19,1),(1025420,'Dylan','MORENO',3867019,'alummno20@instituto.com',2,2,20,20,2),(1025421,'Tom?s','ALVAREZ',3867020,'alummno21@instituto.com',1,3,21,21,3),(1025422,'David','MU?OZ',3867021,'alummno22@instituto.com',2,4,22,22,1),(1025423,'Agust?n','ROMERO',3867022,'alummno23@instituto.com',1,5,23,23,2),(1025424,'Ian','ALONSO',3867023,'alummno24@instituto.com',2,6,24,24,3),(1025425,'Ethan','GUTIERREZ',3867024,'alummno25@instituto.com',1,1,25,25,1),(1025426,'Felipe','NAVARRO',3867025,'alummno26@instituto.com',2,2,26,26,2),(1025427,'Maximiliano','TORRES',3867026,'alummno27@instituto.com',1,3,27,27,3),(1025428,'Ericnue','DOMINGUEZ',3867027,'alummno28@instituto.com',2,4,28,28,1),(1025429,'Hugo','VAZQUEZ',3867028,'alummno29@instituto.com',1,5,29,29,2),(1025430,'Pablo','RAMOS',3867029,'alummno30@instituto.com',2,6,30,30,3),(1025431,'Luca','GIL',3867030,'alummno31@instituto.com',1,1,31,31,1),(1025432,'Rodrigo','RAMIREZ',3867031,'alummno32@instituto.com',2,2,32,32,2),(1025433,'Ignacio','SERRANO',3867032,'alummno33@instituto.com',1,3,33,33,3),(1025434,'Sim?n','BLANCO',3867033,'alummno34@instituto.com',2,4,34,34,1),(1025435,'Carlos','SUAREZ',3867034,'alummno35@instituto.com',1,5,35,35,2),(1025436,'Javier','MOLINA',3867035,'alummno36@instituto.com',2,6,36,36,3),(1025437,'Juan','MORALES',3867036,'alummno37@instituto.com',1,1,37,37,1),(1025438,'Isaac','ORTEGA',3867037,'alummno38@instituto.com',2,2,38,38,2),(1025439,'Santino','DELGADO',3867038,'alummno39@instituto.com',1,3,39,39,3),(1025440,'Manuel','CASTRO',3867039,'alummno40@instituto.com',2,4,40,40,1),(1025441,'Jer?nimo','ORTIZ',3867040,'alummno41@instituto.com',1,5,41,41,2),(1025442,'Emmanuel','RUBIO',3867041,'alummno42@instituto.com',2,6,42,42,3),(1025443,'Aar?n','MARIN',3867042,'alummno43@instituto.com',1,1,43,43,1),(1025444,'?ngel','SANZ',3867043,'alummno44@instituto.com',2,2,44,44,2),(1025445,'Dante','NU?EZ',3867044,'alummno45@instituto.com',1,3,45,45,3),(1025446,'Gael','IGLESIAS',3867045,'alummno46@instituto.com',2,4,46,46,1),(1025447,'Vicente','MEDINA',3867046,'alummno47@instituto.com',1,5,47,47,2),(1025448,'Juan','GARRIDO',3867047,'alummno48@instituto.com',2,6,48,48,3),(1025449,'Sebasti?n','SANTOS',3867048,'alummno49@instituto.com',1,1,49,49,1),(1025450,'Liam','CASTILLO',3867049,'alummno50@instituto.com',2,2,50,50,2),(1025451,'Dami?n','CORTES',3867050,'alummno51@instituto.com',1,3,51,51,3),(1025452,'Leo','LOZANO',3867051,'alummno52@instituto.com',2,4,52,52,1),(1025453,'Francisco','GUERRERO',3867052,'alummno53@instituto.com',1,5,53,53,2),(1025454,'Alonso','CANO',3867053,'alummno54@instituto.com',2,6,54,54,3),(1025455,'Christopher','PRIETO',3867054,'alummno55@instituto.com',1,1,55,55,1),(1025456,'?lvaro','MENDEZ',3867055,'alummno56@instituto.com',2,2,56,56,2),(1025457,'Bautista','CALVO',3867056,'alummno57@instituto.com',1,3,57,57,3),(1025458,'Miguel?ngel','CRUZ',3867057,'alummno58@instituto.com',2,4,58,58,1),(1025459,'Valentino','GALLEGO',3867058,'alummno59@instituto.com',1,5,59,59,2),(1025460,'Rafael','VIDAL',3867059,'alummno60@instituto.com',2,6,60,60,3),(1025461,'Andr?s','LEON',3867060,'alummno61@instituto.com',1,1,61,61,1),(1025462,'Franco','HERRERA',3867061,'alummno62@instituto.com',2,2,62,62,2),(1025463,'Fernando','MARQUEZ',3867062,'alummno63@instituto.com',1,3,63,63,3),(1025464,'Le?n','PE?A',3867063,'alummno64@instituto.com',2,4,64,64,1),(1025465,'Oliver','CABRERA',3867064,'alummno65@instituto.com',1,5,65,65,2),(1025466,'Emilio','FLORES',3867065,'alummno66@instituto.com',2,6,66,66,3),(1025467,'Marcos','CAMPOS',3867066,'alummno67@instituto.com',1,1,67,67,1),(1025468,'Juli?n','VEGA',3867067,'alummno68@instituto.com',2,2,68,68,2),(1025469,'JuanJos?','DIEZ',3867068,'alummno69@instituto.com',1,3,69,69,3),(1025470,'Pedro','FUENTES',3867069,'alummno70@instituto.com',2,4,70,70,1),(1025471,'Alexander','CARRASCO',3867070,'alummno71@instituto.com',1,5,71,71,2),(1025472,'Lorenzo','CABALLERO',3867071,'alummno72@instituto.com',2,6,72,72,3),(1025473,'Mario','NIETO',3867072,'alummno73@instituto.com',1,1,73,73,1),(1025474,'Sergio','REYES',3867073,'alummno74@instituto.com',2,2,74,74,2),(1025475,'M?ximo','AGUILAR',3867074,'alummno75@instituto.com',1,3,75,75,3),(1025476,'Cristian','PASCUAL',3867075,'alummno76@instituto.com',2,4,76,76,1),(1025477,'Esteban','HERRERO',3867076,'alummno77@instituto.com',1,5,77,77,2),(1025478,'El?as','SANTANA',3867077,'alummno78@instituto.com',2,6,78,78,3),(1025479,'Antonio','LORENZO',3867078,'alummno79@instituto.com',1,1,79,79,1),(1025480,'Luciano','HIDALGO',3867079,'alummno80@instituto.com',2,2,80,80,2),(1025481,'Noah','MONTERO',3867080,'alummno81@instituto.com',1,3,81,81,3),(1025482,'Jorge','IBA?EZ',3867081,'alummno82@instituto.com',2,4,82,82,1),(1025483,'Enzo','GIMENEZ',3867082,'alummno83@instituto.com',1,5,83,83,2),(1025484,'Axel','FERRER',3867083,'alummno84@instituto.com',2,6,84,84,3),(1025485,'Salvador','DURAN',3867084,'alummno85@instituto.com',1,1,85,85,1),(1025486,'Marc','VICENTE',3867085,'alummno86@instituto.com',2,2,86,86,2),(1025487,'Derek','BENITEZ',3867086,'alummno87@instituto.com',1,3,87,87,3),(1025488,'JuanMart?n','MORA',3867087,'alummno88@instituto.com',2,4,88,88,1),(1025489,'Joelnuevo','SANTIAGO',3867088,'alummno89@instituto.com',1,5,89,89,2),(1025490,'Juan','ARIAS',3867089,'alummno90@instituto.com',2,6,90,90,3),(1025491,'Diego','VARGAS',3867090,'alummno91@instituto.com',1,1,91,91,1),(1025492,'Gonzalo','CARMONA',3867091,'alummno92@instituto.com',2,2,92,92,2),(1025493,'Kevin','CRESPO',3867092,'alummno93@instituto.com',1,3,93,93,3),(1025494,'Alan','ROMAN',3867093,'alummno94@instituto.com',2,4,94,94,1),(1025495,'Eduardo','PASTOR',3867094,'alummno95@instituto.com',1,5,95,95,2),(1025496,'Miguel','SOTO',3867095,'alummno96@instituto.com',2,6,96,96,3),(1025497,'Iv?n','SAEZ',3867096,'alummno97@instituto.com',1,1,97,97,1),(1025498,'Josu?','VELASCO',3867097,'alummno98@instituto.com',2,2,98,98,2),(1025499,'Crist?bal','SOLER',3867098,'alummno99@instituto.com',1,3,99,99,3),(10254100,'Ciro','MOYA',3867099,'alummno100@instituto.com',2,4,100,100,1),(10254101,'JuanDavid','ESTEBAN',3867100,'alummno101@instituto.com',1,5,101,1,2),(10254102,'Santiago','PARRA',3867101,'alummno102@instituto.com',2,6,102,2,3),(10254103,'Mateo','BRAVO',3867102,'alummno103@instituto.com',1,1,103,3,1),(10254104,'Sebasti?n','GALLARDO',3867103,'alummno104@instituto.com',2,2,104,4,2),(10254105,'Alejandro','ROJAS',3867104,'alummno105@instituto.com',1,3,105,5,3),(10254106,'Mat?as','GARCIA',3867105,'alummno106@instituto.com',2,4,106,6,1),(10254107,'Diego','GONZALEZ',3867106,'alummno107@instituto.com',1,5,107,7,2),(10254108,'Samuel','RODRIGUEZ',3867107,'alummno108@instituto.com',2,6,108,8,3),(10254109,'Nicol?s','FERNANDEZ',3867108,'alummno109@instituto.com',1,1,109,9,1),(10254110,'Daniel','LOPEZ',3867109,'alummno110@instituto.com',2,2,110,10,2),(10254111,'Mart?n','MARTINEZ',3867110,'alummno111@instituto.com',1,3,111,11,3),(10254112,'Benjam?n','SANCHEZ',3867111,'alummno112@instituto.com',2,4,112,12,1),(10254113,'Emiliano','EREZ',3867112,'alummno113@instituto.com',1,5,113,13,2),(10254114,'Leonardo','GOMEZ',3867113,'alummno114@instituto.com',2,6,114,14,3),(10254115,'Joaqu?n','MARTIN',3867114,'alummno115@instituto.com',1,1,115,15,1),(10254116,'Lucas','JIMENEZ',3867115,'alummno116@instituto.com',2,2,116,16,2),(10254117,'Iker','RUIZ',3867116,'alummno117@instituto.com',1,3,117,17,3),(10254118,'Gabriel','HERNANDEZ',3867117,'alummno118@instituto.com',2,4,118,18,1),(10254119,'Thiago','DIAZ',3867118,'alummno119@instituto.com',1,5,119,19,2),(10254120,'Adri?n','MORENO',3867119,'alummno120@instituto.com',2,6,120,20,3),(10254121,'Bruno','ALVAREZ',3867120,'alummno121@instituto.com',1,1,121,21,1),(10254122,'Dylan','MU?OZ',3867121,'alummno122@instituto.com',2,2,122,22,2),(10254123,'Tom?s','ROMERO',3867122,'alummno123@instituto.com',1,3,123,23,3),(10254124,'David','ALONSO',3867123,'alummno124@instituto.com',2,4,124,24,1),(10254125,'Agust?n','GUTIERREZ',3867124,'alummno125@instituto.com',1,5,125,25,2),(10254126,'Ian','NAVARRO',3867125,'alummno126@instituto.com',2,6,126,26,3),(10254127,'Ethan','TORRES',3867126,'alummno127@instituto.com',1,1,127,27,1),(10254128,'Felipe','DOMINGUEZ',3867127,'alummno128@instituto.com',2,2,128,28,2),(10254129,'Maximiliano','VAZQUEZ',3867128,'alummno129@instituto.com',1,3,129,29,3),(10254130,'Ericnue','RAMOS',3867129,'alummno130@instituto.com',2,4,130,30,1),(10254131,'Hugo','GIL',3867130,'alummno131@instituto.com',1,5,131,31,2),(10254132,'Pablo','RAMIREZ',3867131,'alummno132@instituto.com',2,6,132,32,3),(10254133,'Luca','SERRANO',3867132,'alummno133@instituto.com',1,1,133,33,1),(10254134,'Rodrigo','BLANCO',3867133,'alummno134@instituto.com',2,2,134,34,2),(10254135,'Ignacio','SUAREZ',3867134,'alummno135@instituto.com',1,3,135,35,3),(10254136,'Sim?n','MOLINA',3867135,'alummno136@instituto.com',2,4,136,36,1),(10254137,'Carlos','MORALES',3867136,'alummno137@instituto.com',1,5,137,37,2),(10254138,'Javier','ORTEGA',3867137,'alummno138@instituto.com',2,6,138,38,3),(10254139,'Juan','DELGADO',3867138,'alummno139@instituto.com',1,1,139,39,1),(10254140,'Isaac','CASTRO',3867139,'alummno140@instituto.com',2,2,140,40,2),(10254141,'Santino','ORTIZ',3867140,'alummno141@instituto.com',1,3,141,41,3),(10254142,'Manuel','RUBIO',3867141,'alummno142@instituto.com',2,4,142,42,1),(10254143,'Jer?nimo','MARIN',3867142,'alummno143@instituto.com',1,5,143,43,2),(10254144,'Emmanuel','SANZ',3867143,'alummno144@instituto.com',2,6,144,44,3),(10254145,'Aar?n','NU?EZ',3867144,'alummno145@instituto.com',1,1,145,45,1),(10254146,'?ngel','IGLESIAS',3867145,'alummno146@instituto.com',2,2,146,46,2),(10254147,'Dante','MEDINA',3867146,'alummno147@instituto.com',1,3,147,47,3),(10254148,'Gael','GARRIDO',3867147,'alummno148@instituto.com',2,4,148,48,1),(10254149,'Vicente','SANTOS',3867148,'alummno149@instituto.com',1,5,149,49,2),(10254150,'Juan','CASTILLO',3867149,'alummno150@instituto.com',2,6,150,50,3),(10254151,'Sebasti?n','CORTES',3867150,'alummno151@instituto.com',1,1,151,51,1),(10254152,'Liam','LOZANO',3867151,'alummno152@instituto.com',2,2,152,52,2),(10254153,'Dami?n','GUERRERO',3867152,'alummno153@instituto.com',1,3,153,53,3),(10254154,'Leo','CANO',3867153,'alummno154@instituto.com',2,4,154,54,1),(10254155,'Francisco','PRIETO',3867154,'alummno155@instituto.com',1,5,155,55,2),(10254156,'Alonso','MENDEZ',3867155,'alummno156@instituto.com',2,6,156,56,3),(10254157,'Christopher','CALVO',3867156,'alummno157@instituto.com',1,1,157,57,1),(10254158,'?lvaro','CRUZ',3867157,'alummno158@instituto.com',2,2,158,58,2),(10254159,'Bautista','GALLEGO',3867158,'alummno159@instituto.com',1,3,159,59,3),(10254160,'Miguel?ngel','VIDAL',3867159,'alummno160@instituto.com',2,4,160,60,1),(10254161,'Valentino','LEON',3867160,'alummno161@instituto.com',1,5,161,61,2),(10254162,'Rafael','HERRERA',3867161,'alummno162@instituto.com',2,6,162,62,3),(10254163,'Andr?s','MARQUEZ',3867162,'alummno163@instituto.com',1,1,163,63,1),(10254164,'Franco','PE?A',3867163,'alummno164@instituto.com',2,2,164,64,2),(10254165,'Fernando','CABRERA',3867164,'alummno165@instituto.com',1,3,165,65,3),(10254166,'Le?n','FLORES',3867165,'alummno166@instituto.com',2,4,166,66,1),(10254167,'Oliver','CAMPOS',3867166,'alummno167@instituto.com',1,5,167,67,2),(10254168,'Emilio','VEGA',3867167,'alummno168@instituto.com',2,6,168,68,3),(10254169,'Marcos','DIEZ',3867168,'alummno169@instituto.com',1,1,169,69,1),(10254170,'Juli?n','FUENTES',3867169,'alummno170@instituto.com',2,2,170,70,2),(10254171,'JuanJos?','CARRASCO',3867170,'alummno171@instituto.com',1,3,171,71,3),(10254172,'Pedro','CABALLERO',3867171,'alummno172@instituto.com',2,4,172,72,1),(10254173,'Alexander','NIETO',3867172,'alummno173@instituto.com',1,5,173,73,2),(10254174,'Lorenzo','REYES',3867173,'alummno174@instituto.com',2,6,174,74,3),(10254175,'Mario','AGUILAR',3867174,'alummno175@instituto.com',1,1,175,75,1),(10254176,'Sergio','PASCUAL',3867175,'alummno176@instituto.com',2,2,176,76,2),(10254177,'M?ximo','HERRERO',3867176,'alummno177@instituto.com',1,3,177,77,3),(10254178,'Cristian','SANTANA',3867177,'alummno178@instituto.com',2,4,178,78,1),(10254179,'Esteban','LORENZO',3867178,'alummno179@instituto.com',1,5,179,79,2),(10254180,'El?as','HIDALGO',3867179,'alummno180@instituto.com',2,6,180,80,3),(10254181,'Antonio','MONTERO',3867180,'alummno181@instituto.com',1,1,181,81,1),(10254182,'Luciano','IBA?EZ',3867181,'alummno182@instituto.com',2,2,182,82,2),(10254183,'Noah','GIMENEZ',3867182,'alummno183@instituto.com',1,3,183,83,3),(10254184,'Jorge','FERRER',3867183,'alummno184@instituto.com',2,4,184,84,1),(10254185,'Enzo','DURAN',3867184,'alummno185@instituto.com',1,5,185,85,2),(10254186,'Axel','VICENTE',3867185,'alummno186@instituto.com',2,6,186,86,3),(10254187,'Salvador','BENITEZ',3867186,'alummno187@instituto.com',1,1,187,87,1),(10254188,'Marc','MORA',3867187,'alummno188@instituto.com',2,2,188,88,2),(10254189,'Derek','SANTIAGO',3867188,'alummno189@instituto.com',1,3,189,89,3),(10254190,'JuanMart?n','ARIAS',3867189,'alummno190@instituto.com',2,4,190,90,1),(10254191,'Joelnuevo','VARGAS',3867190,'alummno191@instituto.com',1,5,191,91,2),(10254192,'Juan','CARMONA',3867191,'alummno192@instituto.com',2,6,192,92,3),(10254193,'Diego','CRESPO',3867192,'alummno193@instituto.com',1,1,193,93,1),(10254194,'Gonzalo','ROMAN',3867193,'alummno194@instituto.com',2,2,194,94,2),(10254195,'Kevin','PASTOR',3867194,'alummno195@instituto.com',1,3,195,95,3),(10254196,'Alan','SOTO',3867195,'alummno196@instituto.com',2,4,196,96,1),(10254197,'Eduardo','SAEZ',3867196,'alummno197@instituto.com',1,5,197,97,2),(10254198,'Miguel','VELASCO',3867197,'alummno198@instituto.com',2,6,198,98,3),(10254199,'Iv?n','SOLER',3867198,'alummno199@instituto.com',1,1,199,99,1),(10254200,'Josu?','MOYA',3867199,'alummno200@instituto.com',2,2,200,100,2),(10254201,'Crist?bal','ESTEBAN',3867200,'alummno201@instituto.com',1,3,201,1,3),(10254202,'Ciro','PARRA',3867201,'alummno202@instituto.com',2,4,202,2,1),(10254203,'JuanDavid','BRAVO',3867202,'alummno203@instituto.com',1,5,203,3,2),(10254204,'Santiago','GALLARDO',3867203,'alummno204@instituto.com',2,6,204,4,3),(10254205,'Mateo','ROJAS',3867204,'alummno205@instituto.com',1,1,205,5,1),(10254206,'Sebasti?n','GARCIA',3867205,'alummno206@instituto.com',2,2,206,6,2),(10254207,'Alejandro','GONZALEZ',3867206,'alummno207@instituto.com',1,3,207,7,3),(10254208,'Mat?as','RODRIGUEZ',3867207,'alummno208@instituto.com',2,4,208,8,1),(10254209,'Diego','FERNANDEZ',3867208,'alummno209@instituto.com',1,5,209,9,2),(10254210,'Samuel','LOPEZ',3867209,'alummno210@instituto.com',2,6,210,10,3),(10254211,'Nicol?s','MARTINEZ',3867210,'alummno211@instituto.com',1,1,211,11,1),(10254212,'Daniel','SANCHEZ',3867211,'alummno212@instituto.com',2,2,212,12,2),(10254213,'Mart?n','EREZ',3867212,'alummno213@instituto.com',1,3,213,13,3),(10254214,'Benjam?n','GOMEZ',3867213,'alummno214@instituto.com',2,4,214,14,1),(10254215,'Emiliano','MARTIN',3867214,'alummno215@instituto.com',1,5,215,15,2),(10254216,'Leonardo','JIMENEZ',3867215,'alummno216@instituto.com',2,6,216,16,3),(10254217,'Joaqu?n','RUIZ',3867216,'alummno217@instituto.com',1,1,217,17,1),(10254218,'Lucas','HERNANDEZ',3867217,'alummno218@instituto.com',2,2,218,18,2),(10254219,'Iker','DIAZ',3867218,'alummno219@instituto.com',1,3,219,19,3),(10254220,'Gabriel','MORENO',3867219,'alummno220@instituto.com',2,4,220,20,1),(10254221,'Thiago','ALVAREZ',3867220,'alummno221@instituto.com',1,5,221,21,2),(10254222,'Adri?n','MU?OZ',3867221,'alummno222@instituto.com',2,6,222,22,3),(10254223,'Bruno','ROMERO',3867222,'alummno223@instituto.com',1,1,223,23,1),(10254224,'Dylan','ALONSO',3867223,'alummno224@instituto.com',2,2,224,24,2),(10254225,'Tom?s','GUTIERREZ',3867224,'alummno225@instituto.com',1,3,225,25,3),(10254226,'David','NAVARRO',3867225,'alummno226@instituto.com',2,4,226,26,1),(10254227,'Agust?n','TORRES',3867226,'alummno227@instituto.com',1,5,227,27,2),(10254228,'Ian','DOMINGUEZ',3867227,'alummno228@instituto.com',2,6,228,28,3),(10254229,'Ethan','VAZQUEZ',3867228,'alummno229@instituto.com',1,1,229,29,1),(10254230,'Felipe','RAMOS',3867229,'alummno230@instituto.com',2,2,230,30,2),(10254231,'Maximiliano','GIL',3867230,'alummno231@instituto.com',1,3,231,31,3),(10254232,'Ericnue','RAMIREZ',3867231,'alummno232@instituto.com',2,4,232,32,1),(10254233,'Hugo','SERRANO',3867232,'alummno233@instituto.com',1,5,233,33,2),(10254234,'Pablo','BLANCO',3867233,'alummno234@instituto.com',2,6,234,34,3),(10254235,'Luca','SUAREZ',3867234,'alummno235@instituto.com',1,1,235,35,1),(10254236,'Rodrigo','MOLINA',3867235,'alummno236@instituto.com',2,2,236,36,2),(10254237,'Ignacio','MORALES',3867236,'alummno237@instituto.com',1,3,237,37,3),(10254238,'Sim?n','ORTEGA',3867237,'alummno238@instituto.com',2,4,238,38,1),(10254239,'Carlos','DELGADO',3867238,'alummno239@instituto.com',1,5,239,39,2),(10254240,'Javier','CASTRO',3867239,'alummno240@instituto.com',2,6,240,40,3),(10254241,'Juan','ORTIZ',3867240,'alummno241@instituto.com',1,1,241,41,1),(10254242,'Isaac','RUBIO',3867241,'alummno242@instituto.com',2,2,242,42,2),(10254243,'Santino','MARIN',3867242,'alummno243@instituto.com',1,3,243,43,3),(10254244,'Manuel','SANZ',3867243,'alummno244@instituto.com',2,4,244,44,1),(10254245,'Jer?nimo','NU?EZ',3867244,'alummno245@instituto.com',1,5,245,45,2),(10254246,'Emmanuel','IGLESIAS',3867245,'alummno246@instituto.com',2,6,246,46,3),(10254247,'Aar?n','MEDINA',3867246,'alummno247@instituto.com',1,1,247,47,1),(10254248,'?ngel','GARRIDO',3867247,'alummno248@instituto.com',2,2,248,48,2),(10254249,'Dante','SANTOS',3867248,'alummno249@instituto.com',1,3,249,49,3),(10254250,'Gael','CASTILLO',3867249,'alummno250@instituto.com',2,4,250,50,1),(10254251,'Vicente','CORTES',3867250,'alummno251@instituto.com',1,5,251,51,2),(10254252,'Juan','LOZANO',3867251,'alummno252@instituto.com',2,6,252,52,3),(10254253,'Sebasti?n','GUERRERO',3867252,'alummno253@instituto.com',1,1,253,53,1),(10254254,'Liam','CANO',3867253,'alummno254@instituto.com',2,2,254,54,2),(10254255,'Dami?n','PRIETO',3867254,'alummno255@instituto.com',1,3,255,55,3),(10254256,'Leo','MENDEZ',3867255,'alummno256@instituto.com',2,4,256,56,1),(10254257,'Francisco','CALVO',3867256,'alummno257@instituto.com',1,5,257,57,2),(10254258,'Alonso','CRUZ',3867257,'alummno258@instituto.com',2,6,258,58,3),(10254261,'Bautista','LEON',3867260,'alummno261@instituto.com',1,3,261,61,3),(10254262,'Miguel?ngel','HERRERA',3867261,'alummno262@instituto.com',2,4,262,62,1),(10254263,'Valentino','MARQUEZ',3867262,'alummno263@instituto.com',1,5,263,63,2),(10254264,'Rafael','PE?A',3867263,'alummno264@instituto.com',2,6,264,64,3),(10254265,'Andr?s','CABRERA',3867264,'alummno265@instituto.com',1,1,265,65,1),(10254266,'Franco','FLORES',3867265,'alummno266@instituto.com',2,2,266,66,2),(10254267,'Fernando','CAMPOS',3867266,'alummno267@instituto.com',1,3,267,67,3),(10254268,'Le?n','VEGA',3867267,'alummno268@instituto.com',2,4,268,68,1),(10254269,'Oliver','DIEZ',3867268,'alummno269@instituto.com',1,5,269,69,2),(10254270,'Emilio','FUENTES',3867269,'alummno270@instituto.com',2,6,270,70,3),(10254271,'Marcos','CARRASCO',3867270,'alummno271@instituto.com',1,1,271,71,1),(10254272,'Juli?n','CABALLERO',3867271,'alummno272@instituto.com',2,2,272,72,2),(10254273,'JuanJos?','NIETO',3867272,'alummno273@instituto.com',1,3,273,73,3),(10254274,'Pedro','REYES',3867273,'alummno274@instituto.com',2,4,274,74,1),(10254275,'Alexander','AGUILAR',3867274,'alummno275@instituto.com',1,5,275,75,2),(10254276,'Lorenzo','PASCUAL',3867275,'alummno276@instituto.com',2,6,276,76,3),(10254277,'Mario','HERRERO',3867276,'alummno277@instituto.com',1,1,277,77,1),(10254278,'Sergio','SANTANA',3867277,'alummno278@instituto.com',2,2,278,78,2),(10254279,'M?ximo','LORENZO',3867278,'alummno279@instituto.com',1,3,279,79,3),(10254280,'Cristian','HIDALGO',3867279,'alummno280@instituto.com',2,4,280,80,1),(10254281,'Esteban','MONTERO',3867280,'alummno281@instituto.com',1,5,281,81,2),(10254282,'El?as','IBA?EZ',3867281,'alummno282@instituto.com',2,6,282,82,3),(10254283,'Antonio','GIMENEZ',3867282,'alummno283@instituto.com',1,1,283,83,1),(10254284,'Luciano','FERRER',3867283,'alummno284@instituto.com',2,2,284,84,2),(10254285,'Noah','DURAN',3867284,'alummno285@instituto.com',1,3,285,85,3),(10254286,'Jorge','VICENTE',3867285,'alummno286@instituto.com',2,4,286,86,1),(10254287,'Enzo','BENITEZ',3867286,'alummno287@instituto.com',1,5,287,87,2),(10254288,'Axel','MORA',3867287,'alummno288@instituto.com',2,6,288,88,3),(10254289,'Salvador','SANTIAGO',3867288,'alummno289@instituto.com',1,1,289,89,1),(10254290,'Marc','ARIAS',3867289,'alummno290@instituto.com',2,2,290,90,2),(10254291,'Derek','VARGAS',3867290,'alummno291@instituto.com',1,3,291,91,3),(10254292,'JuanMart?n','CARMONA',3867291,'alummno292@instituto.com',2,4,292,92,1),(10254293,'Joelnuevo','CRESPO',3867292,'alummno293@instituto.com',1,5,293,93,2),(10254294,'Juan','ROMAN',3867293,'alummno294@instituto.com',2,6,294,94,3),(10254295,'Diego','PASTOR',3867294,'alummno295@instituto.com',1,1,295,95,1),(10254296,'Gonzalo','SOTO',3867295,'alummno296@instituto.com',2,2,296,96,2),(10254297,'Kevin','SAEZ',3867296,'alummno297@instituto.com',1,3,297,97,3),(10254298,'Alan','VELASCO',3867297,'alummno298@instituto.com',2,4,298,98,1),(10254299,'Eduardo','SOLER',3867298,'alummno299@instituto.com',1,5,299,99,2),(10254300,'Eduardo','SOLER',3867298,'alummno299@instituto.com',1,5,299,99,2),(10254301,'Alan','VELASCO',3867297,'alummno298@instituto.com',2,4,298,98,1),(10254302,'Kevin','SAEZ',3867296,'alummno297@instituto.com',1,3,297,97,3),(10254303,'Gonzalo','SOTO',3867295,'alummno296@instituto.com',2,2,296,96,2);
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `Cod_area` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) DEFAULT NULL,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_area`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Humanidades','Asignaturas de religion y etic'),(2,'Ciencias Naturales','Asignaturas de fisica,quimica,'),(3,'Matematicas','Asignaturas de calculo, algebr'),(4,'Recreacion y deportes','Asignaturas de cultura fisica');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `Cod_asignatura` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_asiganturadenominacion` varchar(50) DEFAULT NULL,
  `fkdocalumno` bigint(20) DEFAULT NULL,
  `fkcodcurso` bigint(20) DEFAULT NULL,
  `fkcodcampus` bigint(20) DEFAULT NULL,
  `fkcodtitulacion` bigint(20) DEFAULT NULL,
  `fkcodconvocatoria` bigint(20) DEFAULT NULL,
  `fkcodgrupo` bigint(20) DEFAULT NULL,
  `fkcodempleado` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkcodarea` bigint(20) DEFAULT NULL,
  `fkcodcalificacion` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_asignatura`),
  KEY `asignaturaalumno` (`fkdocalumno`),
  KEY `asignaturacurso` (`fkcodcurso`),
  KEY `asignaturacampus` (`fkcodcampus`),
  KEY `asignaturatitulacion` (`fkcodtitulacion`),
  KEY `asignaturaconvocatoria` (`fkcodconvocatoria`),
  KEY `asignaturacalificacion` (`fkcodcalificacion`),
  KEY `asignaturagrupo` (`fkcodgrupo`),
  KEY `asignaturaempleado` (`fkcodempleado`),
  KEY `asignaturaestado` (`fkcodestado`),
  KEY `asignaturaarea` (`fkcodarea`),
  CONSTRAINT `asignaturaalumno` FOREIGN KEY (`fkdocalumno`) REFERENCES `alumno` (`Doc_alumno`),
  CONSTRAINT `asignaturaarea` FOREIGN KEY (`fkcodarea`) REFERENCES `area` (`Cod_area`),
  CONSTRAINT `asignaturacalificacion` FOREIGN KEY (`fkcodcalificacion`) REFERENCES `calificacion` (`Cod_calificacion`),
  CONSTRAINT `asignaturacampus` FOREIGN KEY (`fkcodcampus`) REFERENCES `campus` (`Cod_campus`),
  CONSTRAINT `asignaturaconvocatoria` FOREIGN KEY (`fkcodconvocatoria`) REFERENCES `convocatoria` (`Cod_convocatoria`),
  CONSTRAINT `asignaturacurso` FOREIGN KEY (`fkcodcurso`) REFERENCES `curso` (`Cod_curso`),
  CONSTRAINT `asignaturaempleado` FOREIGN KEY (`fkcodempleado`) REFERENCES `empleado` (`Cod_empleado`),
  CONSTRAINT `asignaturaestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `asignaturagrupo` FOREIGN KEY (`fkcodgrupo`) REFERENCES `grupo` (`Cod_grupo`),
  CONSTRAINT `asignaturatitulacion` FOREIGN KEY (`fkcodtitulacion`) REFERENCES `titulacion` (`Cod_titulacion`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES (2,'Francesa',102542,2,2,2,2,2,2,2,2,2),(3,'Historiadelaliteraturafrancesa:EdadMediayiento',102543,3,3,3,3,3,3,1,3,3),(4,'Historiadelaliteraturafrancesa:SiglosXIXyXX',102544,4,4,4,4,4,4,2,4,4),(5,'Historiadelaliteraturafrancesa:SiglosXVIIyXVIII',102545,5,5,5,5,5,5,1,1,5),(6,'Lexicograf?aySem?nticaFrancesas',102546,6,6,6,6,6,6,2,2,6),(7,'LiteraturaAlemana:L?rica',102547,7,7,7,7,7,7,1,3,7),(8,'LiteraturaFrancesayPensamiento:SiglosXVIIyXVIII',102548,8,8,8,1,8,8,2,4,8),(9,'Teor?asCr?ticasyAn?lisisdeTextosLiterariosesI',102549,9,9,9,2,9,9,1,1,9),(10,'Alem?n',1025410,10,10,10,3,10,10,2,2,10),(11,'Alemania,AustriaySuiza:identidadesculturalesyneshi',1025411,11,11,11,4,11,11,1,3,11),(12,'Alemania,AustriaySuiza:identidadesculturalesyneshi',1025412,12,12,12,5,12,12,2,4,12),(13,'An?lisisdeTextos',1025413,13,13,13,6,13,13,1,1,13),(14,'ItalianosLiterarios',1025414,14,14,14,7,14,14,2,2,14),(15,'Contemp.',1025415,15,15,15,1,15,15,1,3,15),(16,'An?lisisdelDiscursoenLenguaInglesa',1025416,16,16,16,2,16,16,2,4,16),(17,'An?lisisdelDiscursoLiterarioenLenguaInglesa',1025417,17,17,17,3,17,17,1,1,17),(18,'An?lisisdeldiscursoydiscursosocial',1025418,18,18,18,4,18,18,2,2,18),(19,'An?lisisdeldiscursoylenguahablada',1025419,19,19,19,5,19,19,1,3,19),(20,'An?lisiseinterpretaci?ndetextosliterariosalemanes',1025420,20,20,20,6,20,20,2,4,20),(21,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',1025421,21,21,21,7,21,21,1,1,21),(22,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',1025422,22,22,22,1,22,22,2,2,22),(23,'An?lisisFilol?icoDeTextosRomancesMedievales',1025423,23,23,23,2,23,23,1,3,23),(24,'An?lisisLing??sticaDiacr?nicodeTextosLiterarios',1025424,24,24,24,3,24,24,2,4,24),(25,'An?lisisling??sticodetextoscient?fico-t?cnicosenAn',1025425,25,25,25,4,25,25,1,1,25),(26,'?rabeHablado',1025426,26,26,26,5,26,26,2,2,26),(27,'?rabeablado',1025427,27,27,27,6,27,27,1,3,27),(28,'?rabeI',1025428,28,28,28,7,28,28,2,4,28),(29,'?rabeII',1025429,29,29,29,1,29,29,1,1,29),(30,'?rabeIII',1025430,30,30,30,2,30,30,2,2,30),(31,'?rabeIV',1025431,31,31,31,3,31,31,1,3,31),(32,'?rabeMarroqu?',1025432,32,32,32,4,32,32,2,4,32),(33,'?rabeMarroqu?',1025433,33,33,33,5,33,33,1,1,33),(34,'?rabeV',1025434,34,34,34,6,34,34,2,2,34),(35,'?rabeVI',1025435,35,35,35,7,35,35,1,3,35),(36,'?rabeVII',1025436,36,36,36,1,36,36,2,4,36),(37,'?rabeVIII',1025437,37,37,37,2,37,37,1,1,37),(38,'Arameo',1025438,38,38,38,3,38,38,2,2,38),(39,'Arameo(Sir?aco)Cl?sicoOriental',1025439,39,39,39,4,39,39,1,3,39),(40,'ArameoModerno',1025440,40,40,40,5,40,40,2,4,40),(41,'ArameoModernoI',1025441,41,41,41,6,41,41,1,1,41),(42,'ArameoModernoII',1025442,42,42,42,7,42,42,2,2,42),(43,'Arqueolog?aCl?sica',1025443,43,43,43,1,43,43,1,3,43),(44,'ArteIsl?micoeHispanomusulm?n',1025444,44,44,44,2,44,44,2,4,44),(45,'Catal?nI',1025445,45,45,45,3,45,45,1,1,45),(46,'Catal?nII',1025446,46,46,46,4,46,46,2,2,46),(47,'Cl?sicosdelaliteraturabrasile?a',1025447,47,47,47,5,47,47,1,3,47),(48,'Cl?sicosdelaliteraturaportuguesaI',1025448,48,48,48,6,48,48,2,4,48),(49,'Cl?sicosdeliteraturaportuguesaII',1025449,49,49,49,7,49,49,1,1,49),(50,'ComentariodeTextosAlemanes',1025450,50,50,50,1,50,50,2,2,50),(51,'ComentariodeTextosdelasLiteraturasHisp?nicas',1025451,51,51,51,2,51,51,1,3,51),(52,'ComentariodeTextosHist?ricosenLenguaInglesa',1025452,52,52,52,3,52,52,2,4,52),(53,'ComentariodeTextosItalianos',1025453,53,53,53,4,53,53,1,1,53),(54,'ComentarioFilol?gicodeTextos',1025454,54,54,54,5,54,54,2,2,54),(55,'ComentarioFilol?gicodeTextos',1025455,55,55,55,6,55,55,1,3,55),(56,'ComentarioFilol?gicodeTextos',1025456,56,56,56,7,56,56,2,4,56),(57,'ComentarioLing??stico',1025457,57,57,57,1,57,57,1,1,57),(58,'ComentarioLiterariodeTextosFranceses',1025458,58,58,58,2,58,58,2,2,58),(59,'ComentarioLiterariodeTextosFrancesesI',1025459,59,59,59,3,59,59,1,3,59),(60,'ComentariosdeTextosPo?ticosLatinosdela?pocaCl?sica',1025460,60,60,60,4,60,60,2,4,60),(61,'HistoriadelaLengua',1025461,61,61,61,5,61,61,1,1,61),(62,'Francesa',1025462,62,62,62,6,62,62,2,2,62),(63,'Historiadelaliteraturafrancesa:EdadMediayiento',1025463,63,63,63,7,63,63,1,3,63),(64,'Historiadelaliteraturafrancesa:SiglosXIXyXX',1025464,64,64,64,1,64,64,2,4,64),(65,'Historiadelaliteraturafrancesa:SiglosXVIIyXVIII',1025465,65,65,65,2,65,65,1,1,65),(66,'Lexicograf?aySem?nticaFrancesas',1025466,66,66,66,3,66,66,2,2,66),(67,'LiteraturaAlemana:L?rica',1025467,67,67,67,4,67,67,1,3,67),(68,'LiteraturaFrancesayPensamiento:SiglosXVIIyXVIII',1025468,68,68,68,5,68,68,2,4,68),(69,'Teor?asCr?ticasyAn?lisisdeTextosLiterariosesI',1025469,69,69,69,6,69,69,1,1,69),(70,'Alem?n',1025470,70,70,70,7,70,70,2,2,70),(71,'Alemania,AustriaySuiza:identidadesculturalesyneshi',1025471,71,71,71,1,71,71,1,3,71),(72,'Alemania,AustriaySuiza:identidadesculturalesyneshi',1025472,72,72,72,2,72,72,2,4,72),(73,'An?lisisdeTextos',1025473,73,73,73,3,73,73,1,1,73),(74,'ItalianosLiterarios',1025474,74,74,74,4,74,74,2,2,74),(75,'Contemp.',1025475,75,75,75,5,75,75,1,3,75),(76,'An?lisisdelDiscursoenLenguaInglesa',1025476,76,76,76,6,76,76,2,4,76),(77,'An?lisisdelDiscursoLiterarioenLenguaInglesa',1025477,77,77,77,7,77,77,1,1,77),(78,'An?lisisdeldiscursoydiscursosocial',1025478,78,78,78,1,78,78,2,2,78),(79,'An?lisisdeldiscursoylenguahablada',1025479,79,79,79,2,79,79,1,3,79),(80,'An?lisiseinterpretaci?ndetextosliterariosalemanes',1025480,80,80,80,3,80,80,2,4,80),(81,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',1025481,81,81,81,4,81,81,1,1,81),(82,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',1025482,82,82,82,5,82,82,2,2,82),(83,'An?lisisFilol?icoDeTextosRomancesMedievales',1025483,83,83,83,6,83,83,1,3,83),(84,'An?lisisLing??sticaDiacr?nicodeTextosLiterarios',1025484,84,84,84,7,84,84,2,4,84),(85,'An?lisisling??sticodetextoscient?fico-t?cnicosenAn',1025485,85,85,85,1,85,85,1,1,85),(86,'?rabeHablado',1025486,86,86,86,2,86,86,2,2,86),(87,'?rabeablado',1025487,87,87,87,3,87,87,1,3,87),(88,'?rabeI',1025488,88,88,88,4,88,88,2,4,88),(89,'?rabeII',1025489,89,89,89,5,89,89,1,1,89),(90,'?rabeIII',1025490,90,90,90,6,90,90,2,2,90),(91,'?rabeIV',1025491,91,91,91,7,91,91,1,3,91),(92,'?rabeMarroqu?',1025492,92,92,92,1,92,92,2,4,92),(93,'?rabeMarroqu?',1025493,93,93,93,2,93,93,1,1,93),(94,'?rabeV',1025494,94,94,94,3,94,94,2,2,94),(95,'?rabeVI',1025495,95,95,95,4,95,95,1,3,95),(96,'?rabeVII',1025496,96,96,96,5,96,96,2,4,96),(97,'?rabeVIII',1025497,97,97,97,6,97,97,1,1,97),(98,'Arameo',1025498,98,98,98,7,98,98,2,2,98),(100,'ArameoModerno',10254100,100,100,100,2,100,100,2,4,100),(102,'ArameoModernoII',10254102,102,102,102,4,102,102,2,2,2),(103,'Arqueolog?aCl?sica',10254103,103,103,103,5,103,103,1,3,3),(104,'ArteIsl?micoeHispanomusulm?n',10254104,104,104,104,6,104,104,2,4,4),(105,'Catal?nI',10254105,105,105,105,7,105,105,1,1,5),(106,'Catal?nII',10254106,106,106,106,1,106,106,2,2,6),(107,'Cl?sicosdelaliteraturabrasile?a',10254107,107,107,107,2,107,107,1,3,7),(108,'Cl?sicosdelaliteraturaportuguesaI',10254108,108,108,108,3,108,108,2,4,8),(109,'Cl?sicosdeliteraturaportuguesaII',10254109,109,109,109,4,109,109,1,1,9),(110,'ComentariodeTextosAlemanes',10254110,110,110,110,5,110,110,2,2,10),(111,'ComentariodeTextosdelasLiteraturasHisp?nicas',10254111,111,111,111,6,111,111,1,3,11),(112,'ComentariodeTextosHist?ricosenLenguaInglesa',10254112,112,112,112,7,112,112,2,4,12),(113,'ComentariodeTextosItalianos',10254113,113,113,113,1,113,113,1,1,13),(114,'ComentarioFilol?gicodeTextos',10254114,114,114,114,2,114,114,2,2,14),(115,'ComentarioFilol?gicodeTextos',10254115,115,115,115,3,115,115,1,3,15),(116,'ComentarioFilol?gicodeTextos',10254116,116,116,116,4,116,116,2,4,16),(117,'ComentarioLing??stico',10254117,117,117,117,5,117,117,1,1,17),(118,'ComentarioLiterariodeTextosFranceses',10254118,118,118,118,6,118,118,2,2,18),(119,'ComentarioLiterariodeTextosFrancesesI',10254119,119,119,119,7,119,119,1,3,19),(120,'ComentariosdeTextosPo?ticosLatinosdela?pocaCl?sica',10254120,120,120,120,1,120,120,2,4,20),(121,'HistoriadelaLengua',10254121,121,121,121,2,121,121,1,1,21),(122,'Francesa',10254122,122,122,122,3,122,122,2,2,22),(123,'Historiadelaliteraturafrancesa:EdadMediayiento',10254123,123,123,123,4,123,123,1,3,23),(124,'Historiadelaliteraturafrancesa:SiglosXIXyXX',10254124,124,124,124,5,124,124,2,4,24),(125,'Historiadelaliteraturafrancesa:SiglosXVIIyXVIII',10254125,125,125,125,6,125,125,1,1,25),(126,'Lexicograf?aySem?nticaFrancesas',10254126,126,126,126,7,126,126,2,2,26),(127,'LiteraturaAlemana:L?rica',10254127,127,127,127,1,127,127,1,3,27),(128,'LiteraturaFrancesayPensamiento:SiglosXVIIyXVIII',10254128,128,128,128,2,128,128,2,4,28),(129,'Teor?asCr?ticasyAn?lisisdeTextosLiterariosesI',10254129,129,129,129,3,129,129,1,1,29),(130,'Alem?n',10254130,130,130,130,4,130,130,2,2,30),(131,'Alemania,AustriaySuiza:identidadesculturalesyneshi',10254131,131,131,131,5,131,131,1,3,31),(132,'Alemania,AustriaySuiza:identidadesculturalesyneshi',10254132,132,132,132,6,132,132,2,4,32),(133,'An?lisisdeTextos',10254133,133,133,133,7,133,133,1,1,33),(134,'ItalianosLiterarios',10254134,134,134,134,1,134,134,2,2,34),(135,'Contemp.',10254135,135,135,135,2,135,135,1,3,35),(136,'An?lisisdelDiscursoenLenguaInglesa',10254136,136,136,136,3,136,136,2,4,36),(137,'An?lisisdelDiscursoLiterarioenLenguaInglesa',10254137,137,137,137,4,137,137,1,1,37),(138,'An?lisisdeldiscursoydiscursosocial',10254138,138,138,138,5,138,138,2,2,38),(139,'An?lisisdeldiscursoylenguahablada',10254139,139,139,139,6,139,139,1,3,39),(140,'An?lisiseinterpretaci?ndetextosliterariosalemanes',10254140,140,140,140,7,140,140,2,4,40),(141,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',10254141,141,141,141,1,141,141,1,1,41),(142,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',10254142,142,142,142,2,142,142,2,2,42),(143,'An?lisisFilol?icoDeTextosRomancesMedievales',10254143,143,143,143,3,143,143,1,3,43),(144,'An?lisisLing??sticaDiacr?nicodeTextosLiterarios',10254144,144,144,144,4,144,144,2,4,44),(145,'An?lisisling??sticodetextoscient?fico-t?cnicosenAn',10254145,145,145,145,5,145,145,1,1,45),(146,'?rabeHablado',10254146,146,146,146,6,146,146,2,2,46),(147,'?rabeablado',10254147,147,147,147,7,147,147,1,3,47),(148,'?rabeI',10254148,148,148,148,1,148,148,2,4,48),(149,'?rabeII',10254149,149,149,149,2,149,149,1,1,49),(150,'?rabeIII',10254150,150,150,150,3,150,150,2,2,50),(151,'?rabeIV',10254151,151,151,151,4,151,151,1,3,51),(152,'?rabeMarroqu?',10254152,152,152,152,5,152,152,2,4,52),(153,'?rabeMarroqu?',10254153,153,153,153,6,153,153,1,1,53),(154,'?rabeV',10254154,154,154,154,7,154,154,2,2,54),(155,'?rabeVI',10254155,155,155,155,1,155,155,1,3,55),(156,'?rabeVII',10254156,156,156,156,2,156,156,2,4,56),(157,'?rabeVIII',10254157,157,157,157,3,157,157,1,1,57),(158,'Arameo',10254158,158,158,158,4,158,158,2,2,58),(159,'Arameo(Sir?aco)Cl?sicoOriental',10254159,159,159,159,5,159,159,1,3,59),(160,'ArameoModerno',10254160,160,160,160,6,160,160,2,4,60),(161,'ArameoModernoI',10254161,161,161,161,7,161,161,1,1,61),(162,'ArameoModernoII',10254162,162,162,162,1,162,162,2,2,62),(163,'Arqueolog?aCl?sica',10254163,163,163,163,2,163,163,1,3,63),(164,'ArteIsl?micoeHispanomusulm?n',10254164,164,164,164,3,164,164,2,4,64),(165,'Catal?nI',10254165,165,165,165,4,165,165,1,1,65),(166,'Catal?nII',10254166,166,166,166,5,166,166,2,2,66),(167,'Cl?sicosdelaliteraturabrasile?a',10254167,167,167,167,6,167,167,1,3,67),(168,'Cl?sicosdelaliteraturaportuguesaI',10254168,168,168,168,7,168,168,2,4,68),(169,'Cl?sicosdeliteraturaportuguesaII',10254169,169,169,169,1,169,169,1,1,69),(170,'ComentariodeTextosAlemanes',10254170,170,170,170,2,170,170,2,2,70),(171,'ComentariodeTextosdelasLiteraturasHisp?nicas',10254171,171,171,171,3,171,171,1,3,71),(172,'ComentariodeTextosHist?ricosenLenguaInglesa',10254172,172,172,172,4,172,172,2,4,72),(173,'ComentariodeTextosItalianos',10254173,173,173,173,5,173,173,1,1,73),(174,'ComentarioFilol?gicodeTextos',10254174,174,174,174,6,174,174,2,2,74),(175,'ComentarioFilol?gicodeTextos',10254175,175,175,175,7,175,175,1,3,75),(176,'ComentarioFilol?gicodeTextos',10254176,176,176,176,1,176,176,2,4,76),(177,'ComentarioLing??stico',10254177,177,177,177,2,177,177,1,1,77),(178,'ComentarioLiterariodeTextosFranceses',10254178,178,178,178,3,178,178,2,2,78),(179,'ComentarioLiterariodeTextosFrancesesI',10254179,179,179,179,4,179,179,1,3,79),(180,'ComentariosdeTextosPo?ticosLatinosdela?pocaCl?sica',10254180,180,180,180,5,180,180,2,4,80),(181,'HistoriadelaLengua',10254181,181,181,181,6,181,181,1,1,81),(182,'Francesa',10254182,182,182,182,7,182,182,2,2,82),(183,'Historiadelaliteraturafrancesa:EdadMediayiento',10254183,183,183,183,1,183,183,1,3,83),(184,'Historiadelaliteraturafrancesa:SiglosXIXyXX',10254184,184,184,184,2,184,184,2,4,84),(185,'Historiadelaliteraturafrancesa:SiglosXVIIyXVIII',10254185,185,185,185,3,185,185,1,1,85),(186,'Lexicograf?aySem?nticaFrancesas',10254186,186,186,186,4,186,186,2,2,86),(187,'LiteraturaAlemana:L?rica',10254187,187,187,187,5,187,187,1,3,87),(188,'LiteraturaFrancesayPensamiento:SiglosXVIIyXVIII',10254188,188,188,188,6,188,188,2,4,88),(189,'Teor?asCr?ticasyAn?lisisdeTextosLiterariosesI',10254189,189,189,189,7,189,189,1,1,89),(190,'Alem?n',10254190,190,190,190,1,190,190,2,2,90),(191,'Alemania,AustriaySuiza:identidadesculturalesyneshi',10254191,191,191,191,2,191,191,1,3,91),(192,'Alemania,AustriaySuiza:identidadesculturalesyneshi',10254192,192,192,192,3,192,192,2,4,92),(193,'An?lisisdeTextos',10254193,193,193,193,4,193,193,1,1,93),(194,'ItalianosLiterarios',10254194,194,194,194,5,194,194,2,2,94),(195,'Contemp.',10254195,195,195,195,6,195,195,1,3,95),(196,'An?lisisdelDiscursoenLenguaInglesa',10254196,196,196,196,7,196,196,2,4,96),(197,'An?lisisdelDiscursoLiterarioenLenguaInglesa',10254197,197,197,197,1,197,197,1,1,97),(198,'An?lisisdeldiscursoydiscursosocial',10254198,198,198,198,2,198,198,2,2,98),(199,'An?lisisdeldiscursoylenguahablada',10254199,199,199,199,3,199,199,1,3,99),(200,'An?lisiseinterpretaci?ndetextosliterariosalemanes',10254200,200,200,200,4,200,200,2,4,100),(202,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',10254202,202,202,202,6,202,202,2,2,2),(203,'An?lisisFilol?icoDeTextosRomancesMedievales',10254203,203,203,203,7,203,203,1,3,3),(204,'An?lisisLing??sticaDiacr?nicodeTextosLiterarios',10254204,204,204,204,1,204,204,2,4,4),(205,'An?lisisling??sticodetextoscient?fico-t?cnicosenAn',10254205,205,205,205,2,205,205,1,1,5),(206,'?rabeHablado',10254206,206,206,206,3,206,206,2,2,6),(207,'?rabeablado',10254207,207,207,207,4,207,207,1,3,7),(208,'?rabeI',10254208,208,208,208,5,208,208,2,4,8),(209,'?rabeII',10254209,209,209,209,6,209,209,1,1,9),(210,'?rabeIII',10254210,210,210,210,7,210,210,2,2,10),(211,'?rabeIV',10254211,211,211,211,1,211,211,1,3,11),(212,'?rabeMarroqu?',10254212,212,212,212,2,212,212,2,4,12),(213,'?rabeMarroqu?',10254213,213,213,213,3,213,213,1,1,13),(214,'?rabeV',10254214,214,214,214,4,214,214,2,2,14),(215,'?rabeVI',10254215,215,215,215,5,215,215,1,3,15),(216,'?rabeVII',10254216,216,216,216,6,216,216,2,4,16),(217,'?rabeVIII',10254217,217,217,217,7,217,217,1,1,17),(218,'Arameo',10254218,218,218,218,1,218,218,2,2,18),(219,'Arameo(Sir?aco)Cl?sicoOriental',10254219,219,219,219,2,219,219,1,3,19),(220,'ArameoModerno',10254220,220,220,220,3,220,220,2,4,20),(221,'ArameoModernoI',10254221,221,221,221,4,221,221,1,1,21),(222,'ArameoModernoII',10254222,222,222,222,5,222,222,2,2,22),(223,'Arqueolog?aCl?sica',10254223,223,223,223,6,223,223,1,3,23),(224,'ArteIsl?micoeHispanomusulm?n',10254224,224,224,224,7,224,224,2,4,24),(225,'Catal?nI',10254225,225,225,225,1,225,225,1,1,25),(226,'Catal?nII',10254226,226,226,226,2,226,226,2,2,26),(227,'Cl?sicosdelaliteraturabrasile?a',10254227,227,227,227,3,227,227,1,3,27),(228,'Cl?sicosdelaliteraturaportuguesaI',10254228,228,228,228,4,228,228,2,4,28),(229,'Cl?sicosdeliteraturaportuguesaII',10254229,229,229,229,5,229,229,1,1,29),(230,'ComentariodeTextosAlemanes',10254230,230,230,230,6,230,230,2,2,30),(231,'ComentariodeTextosdelasLiteraturasHisp?nicas',10254231,231,231,231,7,231,231,1,3,31),(232,'ComentariodeTextosHist?ricosenLenguaInglesa',10254232,232,232,232,1,232,232,2,4,32),(233,'ComentariodeTextosItalianos',10254233,233,233,233,2,233,233,1,1,33),(234,'ComentarioFilol?gicodeTextos',10254234,234,234,234,3,234,234,2,2,34),(235,'ComentarioFilol?gicodeTextos',10254235,235,235,235,4,235,235,1,3,35),(236,'ComentarioFilol?gicodeTextos',10254236,236,236,236,5,236,236,2,4,36),(237,'ComentarioLing??stico',10254237,237,237,237,6,237,237,1,1,37),(238,'ComentarioLiterariodeTextosFranceses',10254238,238,238,238,7,238,238,2,2,38),(239,'ComentarioLiterariodeTextosFrancesesI',10254239,239,239,239,1,239,239,1,3,39),(240,'ComentariosdeTextosPo?ticosLatinosdela?pocaCl?sica',10254240,240,240,240,2,240,240,2,4,40),(241,'HistoriadelaLengua',10254241,241,241,241,3,241,241,1,1,41),(242,'Francesa',10254242,242,242,242,4,242,242,2,2,42),(243,'Historiadelaliteraturafrancesa:EdadMediayiento',10254243,243,243,243,5,243,243,1,3,43),(244,'Historiadelaliteraturafrancesa:SiglosXIXyXX',10254244,244,244,244,6,244,244,2,4,44),(245,'Historiadelaliteraturafrancesa:SiglosXVIIyXVIII',10254245,245,245,245,7,245,245,1,1,45),(246,'Lexicograf?aySem?nticaFrancesas',10254246,246,246,246,1,246,246,2,2,46),(247,'LiteraturaAlemana:L?rica',10254247,247,247,247,2,247,247,1,3,47),(248,'LiteraturaFrancesayPensamiento:SiglosXVIIyXVIII',10254248,248,248,248,3,248,248,2,4,48),(249,'Teor?asCr?ticasyAn?lisisdeTextosLiterariosesI',10254249,249,249,249,4,249,249,1,1,49),(250,'Alem?n',10254250,250,250,250,5,250,250,2,2,50),(251,'Alemania,AustriaySuiza:identidadesculturalesyneshi',10254251,251,251,251,6,251,251,1,3,51),(252,'Alemania,AustriaySuiza:identidadesculturalesyneshi',10254252,252,252,252,7,252,252,2,4,52),(253,'An?lisisdeTextos',10254253,253,253,253,1,253,253,1,1,53),(254,'ItalianosLiterarios',10254254,254,254,254,2,254,254,2,2,54),(255,'Contemp.',10254255,255,255,255,3,255,255,1,3,55),(256,'An?lisisdelDiscursoenLenguaInglesa',10254256,256,256,256,4,256,256,2,4,56),(257,'An?lisisdelDiscursoLiterarioenLenguaInglesa',10254257,257,257,257,5,257,257,1,1,57),(258,'An?lisisdeldiscursoydiscursosocial',10254258,258,258,258,6,258,258,2,2,58),(261,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',10254261,261,261,261,2,261,261,1,1,61),(262,'An?lisiseinterpretaci?ndetextosliterariosalemanesI',10254262,262,262,262,3,262,262,2,2,62),(263,'An?lisisFilol?icoDeTextosRomancesMedievales',10254263,263,263,263,4,263,263,1,3,63),(264,'An?lisisLing??sticaDiacr?nicodeTextosLiterarios',10254264,264,264,264,5,264,264,2,4,64),(265,'An?lisisling??sticodetextoscient?fico-t?cnicosenAn',10254265,265,265,265,6,265,265,1,1,65),(266,'?rabeHablado',10254266,266,266,266,7,266,266,2,2,66),(267,'?rabeablado',10254267,267,267,267,1,267,267,1,3,67),(268,'?rabeI',10254268,268,268,268,2,268,268,2,4,68),(269,'?rabeII',10254269,269,269,269,3,269,269,1,1,69),(270,'?rabeIII',10254270,270,270,270,4,270,270,2,2,70),(271,'?rabeIV',10254271,271,271,271,5,271,271,1,3,71),(272,'?rabeMarroqu?',10254272,272,272,272,6,272,272,2,4,72),(273,'?rabeMarroqu?',10254273,273,273,273,7,273,273,1,1,73),(274,'?rabeV',10254274,274,274,274,1,274,274,2,2,74),(275,'?rabeVI',10254275,275,275,275,2,275,275,1,3,75),(276,'?rabeVII',10254276,276,276,276,3,276,276,2,4,76),(277,'?rabeVIII',10254277,277,277,277,4,277,277,1,1,77),(278,'Arameo',10254278,278,278,278,5,278,278,2,2,78),(279,'Arameo(Sir?aco)Cl?sicoOriental',10254279,279,279,279,6,279,279,1,3,79),(280,'ArameoModerno',10254280,280,280,280,7,280,280,2,4,80),(281,'ArameoModernoI',10254281,281,281,281,1,281,281,1,1,81),(282,'ArameoModernoII',10254282,282,282,282,2,282,282,2,2,82),(283,'Arqueolog?aCl?sica',10254283,283,283,283,3,283,283,1,3,83),(284,'ArteIsl?micoeHispanomusulm?n',10254284,284,284,284,4,284,284,2,4,84),(285,'Catal?nI',10254285,285,285,285,5,285,285,1,1,85),(286,'Catal?nII',10254286,286,286,286,6,286,286,2,2,86),(287,'Cl?sicosdelaliteraturabrasile?a',10254287,287,287,287,7,287,287,1,3,87),(288,'Cl?sicosdelaliteraturaportuguesaI',10254288,288,288,288,1,288,288,2,4,88),(289,'Cl?sicosdeliteraturaportuguesaII',10254289,289,289,289,2,289,289,1,1,89),(290,'ComentariodeTextosAlemanes',10254290,290,290,290,3,290,290,2,2,90),(291,'ComentariodeTextosdelasLiteraturasHisp?nicas',10254291,291,291,291,4,291,291,1,3,91),(292,'ComentariodeTextosHist?ricosenLenguaInglesa',10254292,292,292,292,5,292,292,2,4,92),(293,'ComentariodeTextosItalianos',10254293,293,293,293,6,293,293,1,1,93),(294,'ComentarioFilol?gicodeTextos',10254294,294,294,294,7,294,294,2,2,94),(295,'ComentarioFilol?gicodeTextos',10254295,295,295,295,1,295,295,1,3,95),(296,'ComentarioFilol?gicodeTextos',10254296,296,296,296,2,296,296,2,4,96),(297,'ComentarioLing??stico',10254297,297,297,297,3,297,297,1,1,97),(298,'ComentarioLiterariodeTextosFranceses',10254298,298,298,298,4,298,298,2,2,98),(299,'ComentarioLiterariodeTextosFrancesesI',10254299,299,299,299,5,299,299,1,3,99),(300,'ComentariosdeTextosPo?ticosLatinosdela?pocaCl?sica',10254300,300,300,300,6,300,300,2,4,100),(302,'Francesa',102542,2,2,2,2,2,2,2,2,2),(303,'Historiadelaliteraturafrancesa:EdadMediayiento',102543,3,3,3,3,3,3,1,3,3),(304,'Historiadelaliteraturafrancesa:SiglosXIXyXX',102544,4,4,4,4,4,4,2,4,4),(305,'Historiadelaliteraturafrancesa:SiglosXVIIyXVIII',102545,5,5,5,5,5,5,1,1,5),(306,'Lexicograf?aySem?nticaFrancesas',102546,6,6,6,6,6,6,2,2,6),(308,'Francesa',102542,2,2,2,2,2,2,2,2,2);
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calificacion`
--

DROP TABLE IF EXISTS `calificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificacion` (
  `Cod_calificacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` double DEFAULT NULL,
  `fkCod_estado_calificacion` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_calificacion`),
  KEY `califestcalif` (`fkCod_estado_calificacion`),
  CONSTRAINT `califestcalif` FOREIGN KEY (`fkCod_estado_calificacion`) REFERENCES `estadocalificacion` (`Cod_estado_calif`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calificacion`
--

LOCK TABLES `calificacion` WRITE;
/*!40000 ALTER TABLE `calificacion` DISABLE KEYS */;
INSERT INTO `calificacion` VALUES (2,0,5),(3,0.1,5),(4,0.2,5),(5,0.3,5),(6,0.4,5),(7,0.5,5),(8,0.6,5),(9,0.7,5),(10,0.8,5),(11,0.9,5),(12,1,5),(13,1.1,5),(14,1.2,5),(15,1.3,5),(16,1.4,5),(17,1.5,5),(18,1.6,5),(19,1.7,5),(20,1.8,5),(21,1.9,5),(22,2,5),(23,2.1,5),(24,2.2,5),(25,2.3,5),(26,2.4,5),(27,2.5,5),(28,2.6,5),(29,2.7,5),(30,2.8,5),(31,2.9,5),(32,3,5),(33,3.1,5),(34,3.2,5),(35,3.3,5),(36,3.4,5),(37,3.5,5),(38,3.6,5),(39,3.7,5),(40,3.8,5),(41,3.9,5),(42,4,6),(43,4.1,5),(44,4.2,5),(45,4.3,5),(46,4.4,5),(47,4.5,5),(48,4.6,5),(49,4.7,5),(50,4.8,5),(51,4.9,5),(52,5,5),(53,5,5),(54,5.2,5),(55,5.3,5),(56,5.4,5),(57,5.5,5),(58,5.6,5),(59,5.7,5),(60,5.8,5),(61,5.9,5),(62,6,5),(63,6.1,5),(64,6.2,5),(65,6.3,5),(66,6.4,5),(67,6.5,5),(68,6.6,5),(69,6.7,5),(70,6.8,5),(71,6.9,5),(72,7,5),(73,7.1,5),(74,7.2,5),(75,7.3,5),(76,7.4,5),(77,7.5,5),(78,7.6,5),(79,7.7,5),(80,7.8,5),(81,7.9,5),(82,8,5),(83,8.1,5),(84,8.2,5),(85,8.3,5),(86,8.4,5),(87,8.5,5),(88,8.6,5),(89,8.7,5),(90,8.8,5),(91,8.9,5),(92,9,5),(93,9.1,5),(94,9.2,5),(95,9.3,5),(96,9.4,5),(97,9.5,5),(98,9.6,5),(99,9.7,5),(100,9.8,5),(101,9.9,5),(102,10,5),(103,0,6),(104,0.1,6),(105,0.2,6),(106,0.30000000000000004,6),(107,0.4,6),(108,0.5,6),(109,0.6,6),(110,0.7,6),(111,0.7999999999999999,6),(112,0.8999999999999999,6),(113,0.9999999999999999,6),(114,1.0999999999999999,6),(115,1.2,6),(116,1.3,6),(117,1.4000000000000001,6),(118,1.5000000000000002,6),(119,1.6000000000000003,6),(120,1.7000000000000004,6),(121,1.8000000000000005,6),(122,1.9000000000000006,6),(123,2.0000000000000004,6),(124,2.1000000000000005,6),(125,2.2000000000000006,6),(126,2.3000000000000007,6),(127,2.400000000000001,6),(128,2.500000000000001,6),(129,2.600000000000001,6),(130,2.700000000000001,6),(131,2.800000000000001,6),(132,2.9000000000000012,6),(133,3.0000000000000013,6),(134,3.1000000000000014,6),(135,3.2000000000000015,6),(136,3.3000000000000016,6),(137,3.4000000000000017,6),(138,3.5000000000000018,6),(139,3.600000000000002,6),(140,3.700000000000002,6),(141,3.800000000000002,6),(142,3.900000000000002,6),(143,4.000000000000002,6),(144,4.100000000000001,6),(145,4.200000000000001,6),(146,4.300000000000001,6),(147,4.4,6),(148,4.5,6),(149,4.6,6),(150,4.699999999999999,6),(151,4.799999999999999,6),(152,4.899999999999999,6),(153,4.999999999999998,6),(154,5.099999999999998,6),(155,5.1999999999999975,6),(156,5.299999999999997,6),(157,5.399999999999997,6),(158,5.4999999999999964,6),(159,5.599999999999996,6),(160,5.699999999999996,6),(161,5.799999999999995,6),(162,5.899999999999995,6),(163,5.999999999999995,6),(164,6.099999999999994,6),(165,6.199999999999994,6),(166,6.299999999999994,6),(167,6.399999999999993,6),(168,6.499999999999993,6),(169,6.5999999999999925,6),(170,6.699999999999992,6),(171,6.799999999999992,6),(172,6.8999999999999915,6),(173,6.999999999999991,6),(174,7.099999999999991,6),(175,7.19999999999999,6),(176,7.29999999999999,6),(177,7.39999999999999,6),(178,7.499999999999989,6),(179,7.599999999999989,6),(180,7.699999999999989,6),(181,7.799999999999988,6),(182,7.899999999999988,6),(183,7.999999999999988,6),(184,8.099999999999987,6),(185,8.199999999999987,6),(186,8.299999999999986,6),(187,8.399999999999986,6),(188,8.499999999999986,6),(189,8.599999999999985,6),(190,8.699999999999985,6),(191,8.799999999999985,6),(192,8.899999999999984,6),(193,8.999999999999984,6),(194,9.099999999999984,6),(195,9.199999999999983,6),(196,9.299999999999983,6),(197,9.399999999999983,6),(198,9.499999999999982,6),(199,9.599999999999982,6),(200,9.699999999999982,6),(201,9.799999999999981,6),(202,9.89999999999998,6),(203,9.99999999999998,6),(204,10.09999999999998,6),(205,10.19999999999998,6),(206,10.29999999999998,6),(207,10.399999999999979,6),(208,10.499999999999979,6),(209,10.599999999999978,6),(210,10.699999999999978,6),(211,10.799999999999978,6),(212,10.899999999999977,6),(213,10.999999999999977,6),(214,11.099999999999977,6),(215,11.199999999999976,6),(216,11.299999999999976,6),(217,11.399999999999975,6),(218,11.499999999999975,6),(219,11.599999999999975,6),(220,11.699999999999974,6),(221,11.799999999999974,6),(222,11.899999999999974,6),(223,11.999999999999973,6),(224,12.099999999999973,6),(225,12.199999999999973,6),(226,12.299999999999972,6),(227,12.399999999999972,6),(228,12.499999999999972,6),(229,12.599999999999971,6),(230,12.69999999999997,6),(231,12.79999999999997,6),(232,12.89999999999997,6),(233,12.99999999999997,6),(234,13.09999999999997,6),(235,13.199999999999969,6),(236,13.299999999999969,6),(237,13.399999999999968,6),(238,13.499999999999968,6),(239,13.599999999999968,6),(240,13.699999999999967,6),(241,13.799999999999967,6),(242,13.899999999999967,6),(243,13.999999999999966,6),(244,14.099999999999966,6),(245,14.199999999999966,6),(246,14.299999999999965,6),(247,14.399999999999965,6),(248,14.499999999999964,6),(249,14.599999999999964,6),(250,14.699999999999964,6),(251,14.799999999999963,6),(252,14.899999999999963,6),(253,14.999999999999963,6),(254,15.099999999999962,6),(255,15.199999999999962,6),(256,15.299999999999962,6),(257,15.399999999999961,6),(258,15.499999999999961,6),(259,15.59999999999996,6),(260,15.69999999999996,6),(261,15.79999999999996,6),(262,15.89999999999996,6),(263,15.99999999999996,6),(264,16.09999999999996,6),(265,16.19999999999996,6),(266,16.29999999999996,6),(267,16.399999999999963,6),(268,16.499999999999964,6),(269,16.599999999999966,6),(270,16.699999999999967,6),(271,16.79999999999997,6),(272,16.89999999999997,6),(273,16.99999999999997,6),(274,17.099999999999973,6),(275,17.199999999999974,6),(276,17.299999999999976,6),(277,17.399999999999977,6),(278,17.49999999999998,6),(279,17.59999999999998,6),(280,17.69999999999998,6),(281,17.799999999999983,6),(282,17.899999999999984,6),(283,17.999999999999986,6),(284,18.099999999999987,6),(285,18.19999999999999,6),(286,18.29999999999999,6),(287,18.39999999999999,6),(288,18.499999999999993,6),(289,18.599999999999994,6),(290,18.699999999999996,6),(291,18.799999999999997,6),(292,18.9,6),(293,19,6),(294,19.1,6),(295,19.200000000000003,6),(296,19.300000000000004,6),(297,19.400000000000006,6),(298,19.500000000000007,6),(299,19.60000000000001,6),(300,19.70000000000001,6),(301,19.80000000000001,6),(302,19.900000000000013,6);
/*!40000 ALTER TABLE `calificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `Cod_campus` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_campus`),
  KEY `campusestado` (`fkcodestado`),
  CONSTRAINT `campusestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (1,'Centro Multisectorial de Leticia',1),(2,'Centro agropecuario la Salada',1),(3,'Sede el bagre',2),(4,'Centro del Calzado y Manufactura de Cuero',2),(5,'Centro de formacion de diseño, confeccion y moda',2),(6,'Centro de la construccion',1),(7,'Centro de tecnologia de la manufactura avanzada',1),(8,'Centro Tecnologico del mobiliario',1),(9,'Centro Tecnologico del gestion industrial',1),(10,'Subsede santa rosa de osos',1),(11,'Centro de comercio',2),(12,'Sede vicenta',1),(13,'Sede Laureles',1),(14,'Sede pomar',1),(15,'Centro de servicios y gestion empresarial',1),(16,'Complejo tecnologico para la gestion agroempresari',1),(17,'Subsede el bagre',1),(18,'Centro multiseotiral de oriente',1),(19,'Centro multiseotiral de oriente',1),(20,'Sede comercio',1),(21,'Sede industria',1),(22,'Sede municipio de Sonson',1),(23,'Sede municipio guarne',1),(24,'Sede municipio la ceja',1),(25,'Sede municipio el carmen',1),(26,'Sede municipio de girardota',1),(27,'Centro multisectorial de uraba',1),(28,'Sede arauca',2),(29,'sede tame',2),(30,'sede saravena',2),(31,'Centro de atencion sector y agropecuario',2),(32,'Centro nacional Colombo aleman',2),(33,'Sede malambo',2),(34,'Centro nacional industrial de aviacion',1),(35,'Centro de comercio y servicios',1),(36,'Centro de desarrollo agropecuario y Agroindustrial',1),(37,'Centro nacional minero',1),(38,'UNAD',1),(39,'Colegio alejandro de humboldt',1),(40,'Sede recreo',1),(41,'Escuela gastronomica y posada turistica',1),(42,'Oficina empleo',1),(43,'Monoquira',1),(44,'Centro multisectorial de sogamoso',1),(45,'Ciudadela',1),(46,'Chiquinquira',1),(47,'Centro de automatizacion industrial',1),(48,'Agencia publica de empleo',1),(49,'Servicio medico asistencial',1),(50,'Centro multisectorial de florencia',1),(51,'Centro multisectorial de yopal',1),(52,'Centro de teleinformatica y produccion industrial',1),(53,'Centro biotecnologico del caribe',1),(54,'Centro agroempresarial',1),(55,'Centro de operacion y mantenimiento minero',1),(56,'Ambientes del CBC',1),(57,'Ambientes del CDV',1),(58,'Subsede la jagua de ibirico',1),(59,'Tecnoparque nodo valledupar',1),(60,'Sede principal',1),(61,'Sede minercol',2),(62,'Sede iefem',2),(63,'Sede condoto',2),(64,'Sede istima',2),(65,'Centro agropecuario del porvenir',2),(66,'Centro de comercio e industria y turismo de cordob',1),(67,'Centro de la tecnologia de diseño y la productivid',1),(68,'Subsede el colegio',1),(69,'Centro industrial y desarrollo empresarial de soac',1),(70,'Centro multisectorial fusagasuga',1),(71,'Centro de construccion e industria de la madera',1),(72,'Centro de tecnologias para la construccion y la ma',1),(73,'Emprendimiento y diseño',1),(74,'Sede meissen',1),(75,'Sede usme',1),(76,'Sede usme',1),(77,'Escuela colombiana de carreras industriales',1),(78,'Centro nacional de insutria grafica y afines',1),(79,'Antiguo publicaciones',1),(80,'Centro de gestion comercial y mercadeo',1),(81,'SIES sede fontibon',1),(82,'Inesco',1),(83,'Tecnoparque nodo bogota',1),(84,'Calle 45',1),(85,'Calle 34',1),(86,'Centro de servicios administrativos',1),(87,'Fundacion colombo germana',1),(88,'CORUNIVERSITEC',1),(89,'Centro nacional de hoteleria',1),(90,'Cocina hoteleria',1),(91,'Centro de gestion y fortalecimiento socio empresar',1),(92,'Centro multisectorial de rioacha',1),(93,'Sede maica',1),(94,'Centro multisectorial guaviare',1),(95,'Sede la castellana',1),(96,'La union',1),(97,'Centro de atencion sector agropecuario',1),(98,'Colegio aldemar rojas plazas',1),(99,'Colegio julio flores',1),(100,'Aguascalientes',1),(101,'CampusAguascalientes',1),(102,'CampusCiudaddeM?xico',1),(103,'CampusCiudadJu?rez',1),(104,'CampusEstadodeM?xico',1),(105,'PrepaZonaEsmeralda',1),(106,'CampusChihuahua',1),(107,'SedeCasasGrandes',1),(108,'SedeCuauhtemoc',1),(109,'SedeDelicias',1),(110,'SedeParral',1),(111,'CampusChiapas',1),(112,'CampusMorelia',1),(113,'SedeL?zaroC?rdenas',1),(114,'SedeZamora',1),(115,'CampusCiudadObregon',1),(116,'SedeNavojoa',1),(117,'CampusColima',1),(118,'PrepaSantaCatarina',1),(119,'CampusSantaFe',1),(120,'PrepaCumbres',1),(121,'PrepaEugenioGarzaLag?era',1),(122,'PrepaEugenioGarzaSada',1),(123,'CampusGuadalajara',1),(124,'PrepaSantaAnita',1),(125,'CampusGuaymas',1),(126,'CampusSonoraNorte',1),(127,'SedeCaborca',1),(128,'SedeMexicali',1),(129,'SedeNogales',1),(130,'SedeLaPaz',1),(131,'SedeTijuana',1),(132,'CampusHidalgo',1),(133,'SedeCanc?n',1),(134,'SedeHuejutla',1),(135,'SedeYucat?n',1),(136,'SedeTulancingo',1),(137,'SedeTlaxcala',1),(138,'SedeTula',1),(139,'SedeZacualtipan',1),(140,'CampusIrapuato',1),(141,'CampusLaguna',1),(142,'SedeDurango',1),(143,'CampusLe?n',1),(144,'CampusMazatl?n',1),(145,'CampusMorelos',1),(146,'CampusMonterrey',1),(147,'EscueladeGraduadosenAdministraci?nyDirecci?ndeEmpr',1),(148,'EscueladeGraduadosenAdministraci?nP?blica(EGAP)',1),(149,'EscueladeMedicina(EMIS)',1),(150,'CampusPuebla',1),(151,'PrepaValleAlto',1),(152,'CampusQuer?taro',1),(153,'SedeCelaya',1),(154,'UniversidadVirtual',1),(155,'CampusSaltillo',1),(156,'CampusSinaloa',1),(157,'SedeLosMochis',1),(158,'CampusSanLuisPotos?',1),(159,'CampusTampico',1),(160,'SedeMatamoros',1),(161,'CampusToluca',1),(162,'SedeAtlacomulco',1),(163,'SedeEcuador',1),(164,'SedeMetepec',1),(165,'CampusCentraldeVeracruz',1),(166,'SecundariaZacatecas',1),(167,'CampuslifeSl.',1),(168,'CampusTietarSl',1),(169,'CampusfreeS.L.',1),(170,'CampusnewebS.L.',1),(171,'CampusStoresSl',1),(172,'CampusportSl',1),(173,'Campus25l',1),(174,'Campus2010Sl',1),(175,'CampusAbabolSl',1),(176,'CampusOcioSl',1),(177,'CampusprintSl.',1),(178,'CampusSolarSl.',1),(179,'CampusHugoS.L.',1),(180,'CampusLiberSl',1),(181,'Campus384Sl.',1),(182,'CampuscipSl',1),(183,'CampusTalenSl.',1),(184,'CampusFerranSl',1),(185,'CampusSpainSl.',1),(186,'CampusCerdanyaSl',1),(187,'CampusJobs2002Sl',1),(188,'Campus&NaturaS.L.',1),(189,'CampusLp2004Sl.',1),(190,'CampusCateringS.L.',1),(191,'CampusQualitatS.L.',1),(192,'CampusElSabioS.L.',1),(193,'CampusAlfaCdS.L.',1),(194,'CampusAcademicoSl.',1),(195,'CampusSoftwareSl',1),(196,'CampusCanalisSl',1),(197,'CampusAguascalientes',1),(198,'CampusCiudaddeM?xico',1),(199,'CampusCiudadJu?rez',1),(200,'CampusEstadodeM?xico',1),(201,'PrepaZonaEsmeralda',1),(202,'CampusChihuahua',1),(203,'SedeCasasGrandes',1),(204,'SedeCuauhtemoc',1),(205,'SedeDelicias',1),(206,'SedeParral',1),(207,'CampusChiapas',1),(208,'CampusMorelia',1),(209,'SedeL?zaroC?rdenas',1),(210,'SedeZamora',1),(211,'CampusCiudadObregon',1),(212,'SedeNavojoa',1),(213,'CampusColima',1),(214,'PrepaSantaCatarina',1),(215,'CampusSantaFe',1),(216,'PrepaCumbres',1),(217,'PrepaEugenioGarzaLag?era',1),(218,'PrepaEugenioGarzaSada',1),(219,'CampusGuadalajara',1),(220,'PrepaSantaAnita',1),(221,'CampusGuaymas',1),(222,'CampusSonoraNorte',1),(223,'SedeCaborca',1),(224,'SedeMexicali',1),(225,'SedeNogales',1),(226,'SedeLaPaz',1),(227,'SedeTijuana',1),(228,'CampusHidalgo',1),(229,'SedeCanc?n',1),(230,'SedeHuejutla',1),(231,'SedeYucat?n',1),(232,'SedeTulancingo',1),(233,'SedeTlaxcala',1),(234,'SedeTula',1),(235,'SedeZacualtipan',1),(236,'CampusIrapuato',1),(237,'CampusLaguna',1),(238,'SedeDurango',1),(239,'CampusLe?n',1),(240,'CampusMazatl?n',1),(241,'CampusMorelos',1),(242,'CampusMonterrey',1),(243,'EscueladeGraduadosenAdministraci?nyDirecci?ndeEmpr',1),(244,'EscueladeGraduadosenAdministraci?nP?blica(EGAP)',1),(245,'EscueladeMedicina(EMIS)',1),(246,'CampusPuebla',1),(247,'PrepaValleAlto',1),(248,'CampusQuer?taro',1),(249,'SedeCelaya',1),(250,'UniversidadVirtual',1),(251,'CampusSaltillo',1),(252,'CampusSinaloa',1),(253,'SedeLosMochis',1),(254,'CampusSanLuisPotos?',1),(255,'CampusTampico',1),(256,'SedeMatamoros',1),(257,'CampusToluca',1),(258,'SedeAtlacomulco',1),(259,'SedeEcuador',1),(260,'SedeMetepec',1),(261,'CampusCentraldeVeracruz',1),(262,'SecundariaZacatecas',1),(263,'CampuslifeSl.',1),(264,'CampusTietarSl',1),(265,'CampusfreeS.L.',1),(266,'CampusnewebS.L.',1),(267,'CampusStoresSl',1),(268,'CampusportSl',1),(269,'Campus25l',1),(270,'Campus2010Sl',1),(271,'CampusAbabolSl',1),(272,'CampusOcioSl',1),(273,'CampusprintSl.',1),(274,'CampusSolarSl.',1),(275,'CampusHugoS.L.',1),(276,'CampusLiberSl',1),(277,'Campus384Sl.',1),(278,'CampuscipSl',1),(279,'CampusTalenSl.',1),(280,'CampusFerranSl',1),(281,'CampusSpainSl.',1),(282,'CampusCerdanyaSl',1),(283,'CampusJobs2002Sl',1),(284,'Campus&NaturaS.L.',1),(285,'CampusLp2004Sl.',1),(286,'CampusCateringS.L.',1),(287,'CampusQualitatS.L.',1),(288,'CampusElSabioS.L.',1),(289,'CampusAlfaCdS.L.',1),(290,'CampusAcademicoSl.',1),(291,'CampusSoftwareSl',1),(292,'CampusCanalisSl',1),(293,'CampusAguascalientes',1),(294,'CampusCiudaddeM?xico',1),(295,'CampusCiudadJu?rez',1),(296,'CampusEstadodeM?xico',1),(297,'PrepaZonaEsmeralda',1),(298,'CampusChihuahua',1),(299,'SedeCasasGrandes',1),(300,'SedeCuauhtemoc',1);
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `Cod_cargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_cargo`),
  KEY `cargoestado` (`fkcodestado`),
  CONSTRAINT `cargoestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'Profesor',1),(2,'Rector',1),(3,'Coordinador',1),(4,'Orientador',2),(5,'Psicologo',1),(6,'Director',1),(7,'Decano',2),(8,'Vicerrector',1),(9,'Vicedecano',1),(10,'Subdirector',2),(11,'Secretario',1),(12,'Seminario',1),(13,'Bibliotecario',1),(14,'Comision de doctorado',1),(15,'Comision de docencia',1),(16,'Comision de contratacion',1);
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `cod_ciudad` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `fkcoddepartamento` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`cod_ciudad`),
  KEY `ciudaddepartamento` (`fkcoddepartamento`),
  KEY `ciudadestado` (`fkcodestado`),
  CONSTRAINT `ciudaddepartamento` FOREIGN KEY (`fkcoddepartamento`) REFERENCES `departamento` (`cod_departamento`),
  CONSTRAINT `ciudadestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'Kabul',1,1),(2,'Tirana ',2,1),(3,'Berlin',3,1),(4,'Andorra la Vieja',4,1),(5,'Luanda',5,1),(6,'Riad',6,1),(7,'Argel',7,1),(8,'Buenos Aires',8,1),(9,'Ereván',9,1),(10,'Canberra ',10,1),(11,'Viena',11,1),(12,'Baku',12,1),(13,'Nasáu',13,1),(14,'Daca',14,1),(15,'Bridgetown',15,1),(16,'Manama',16,1),(17,'Bruselas',17,1),(18,'Belmopan',18,1),(19,'Porto Novo',19,1),(20,'Minsk',20,1),(21,'Naipyidó',21,1),(22,'sucre',22,1),(23,'Sarajevo',23,1),(24,'Gaborone',24,1),(25,'Rio de janeiro',25,1),(26,'Bandar Seri Begawan',26,1),(27,'Sofía ',27,1),(28,'Uagadugú',28,1),(29,'Bujumbura',29,1),(30,'Praia',30,1),(31,'Nom Pen',31,1),(32,'Yaundé',32,1),(33,'Ottawa ',33,1),(34,'Doha',34,1),(35,'Yamena',35,1),(36,' Santiago de chile',36,1),(37,'Pekin',37,1),(38,'Nicosia ',38,1),(39,'ciudad del vaticano',39,1),(40,'Leticia',40,1),(41,'Medellín',41,1),(42,' Arauca',42,1),(43,'Barranquilla',43,1),(44,'Cartagena',44,1),(45,'Tunja',45,1),(46,'Manizales',46,1),(47,'Florencia',47,1),(48,'Yopal',48,1),(49,'Popayán',49,1),(50,'Valledupar',50,1),(51,'Quibdó',51,1),(52,'Montería',52,1),(53,'Bogotá',53,1),(54,'Puerto Inírida',54,1),(55,'San José del Guaviare',55,1),(56,'Neiva',56,1),(57,'Riohacha',57,1),(58,'Santa Marta',58,1),(59,'Villavicencio',59,1),(60,'Pasto',60,1),(61,'Cúcuta',61,1),(62,'Mocoa',62,1),(63,'Armenia',63,1),(64,'Pereira',64,1),(65,'San Andres',65,1),(66,'Bucaramanga',66,1),(67,'Sincelejo',67,1),(68,'Ibagué',68,1),(69,'Cali',69,1),(70,'Mitú',70,1),(71,'Puerto Carreño',71,1),(72,'Conakri',72,1),(73,'Puerto Príncipe',73,1),(74,'Tegucigalpa',74,1),(75,'Budapest',75,1),(76,'Nueva Delhi',76,1),(77,'Yakarta',77,1),(78,'Bagdad',78,1),(79,'Dublín',79,1),(80,'Reikiavik',80,1),(81,'Majuro',81,1),(82,'Honiara',82,1),(83,'Jesuralen',83,1),(84,'Roma',84,1),(85,'Kingston',85,1),(86,'Tokio',86,1),(87,'Amán',87,1),(88,'Astaná',88,1),(89,'Nairobi',89,1),(90,'Tarawa Sur',90,1),(91,'kuwait',91,1),(92,'Vientián',92,1),(93,'Maseru',93,1),(94,'Riga',94,1),(95,'Beirut',95,1),(96,'Monrovia',96,1),(97,'Vaduz',97,1),(98,'Vilna ',98,1),(99,'Ciudad de Luxemburgo',99,1),(100,'ciudad de maxico',100,1),(101,'Tirana',100,1),(102,'Durr?s',101,2),(103,'Vlor?',102,1),(104,'Elbasan',103,2),(105,'Shkod?r',104,1),(106,'Kam?z',105,2),(107,'Fier',106,1),(108,'Kor??',107,2),(109,'Kashar',108,1),(110,'Paskuqan',109,2),(111,'Berat',110,1),(112,'Lushnj?',111,2),(113,'Rrashbull',112,1),(114,'Fark?',113,2),(115,'Rrethinat',114,1),(116,'Dajt',115,2),(117,'Pogradec',116,1),(118,'Kavaj?',117,2),(119,'Gjirokast?r',118,1),(120,'Fush?-Kruj?',119,2),(121,'Baden-Wurtemberg',120,1),(122,'BajaSajonia',121,2),(123,'Baviera',122,1),(124,'Berl?n',123,2),(125,'Brandeburgo',124,1),(126,'Brema',125,2),(127,'Hamburgo',126,1),(128,'Hesse',127,2),(129,'Mecklemburgo-Pomerania',128,1),(130,'Occidental',129,2),(131,'Renania-Westfalia',130,1),(132,'Renania-Palatinado',131,2),(133,'El',132,1),(134,'Sarre',133,2),(135,'Sajonia',134,1),(136,'Sajonia-Anhalt',135,2),(137,'Schleswig-Holstein',136,1),(138,'Turingia',137,2),(139,'Andorra',138,1),(140,'la',139,2),(141,'Vieja',140,1),(142,'Canillo',141,2),(143,'Encamp',142,1),(144,'Escaldes-Engordany',143,2),(145,'La',144,1),(146,'Massana',145,2),(147,'Ordino',146,1),(148,'Sant',147,2),(149,'Juli?',148,1),(150,'de',149,2),(151,'L?ria',150,1),(152,'Adrar',151,2),(153,'A?n',152,1),(154,'Defla',153,2),(155,'A?n',154,1),(156,'T?mouchent',155,2),(157,'Argel',156,1),(158,'Annaba',157,2),(159,'Batna',158,1),(160,'B?char',159,2),(161,'Buj?a',160,1),(162,'Biskra',161,2),(163,'Blida',162,1),(164,'Bordj',163,2),(165,'Bou',164,1),(166,'Arr?ridj',165,2),(167,'Bouira',166,1),(168,'Boumerd?s',167,2),(169,'Chlef',168,1),(170,'Constantina',169,2),(171,'Djelfa',170,1),(172,'El',171,2),(173,'Bayadh',172,1),(174,'El',173,2),(175,'Oued',174,1),(176,'El',175,2),(177,'Tarf',176,1),(178,'Gharda?a',177,2),(179,'Guelma',178,1),(180,'Illizi',179,2),(181,'Jijel',180,1),(182,'Khenchela',181,2),(183,'Laghouat',182,1),(184,'M?d?a',185,2),(185,'Mila',186,1),(186,'Mostaganem',187,2),(187,'Naama',188,1),(188,'Or?n',189,2),(189,'Ouargla',190,1),(190,'Oum',191,2),(191,'el-Bouaghi',192,1),(192,'Reliz?n',193,2),(193,'Saida',194,1),(194,'S?tif',195,2),(195,'Sidi',196,1),(196,'Bel',197,2),(197,'Abbes',198,1),(198,'Skikda',199,2),(199,'Souk',200,1),(200,'Ahras',201,2),(201,'Tamanghasset',202,1),(202,'T?bessa',203,2),(203,'Tiaret',204,1),(204,'Tinduf',205,2),(205,'Tipasa',206,1),(206,'Tissemsilt',207,2),(207,'Tizi',208,1),(208,'Ouzou',209,2),(209,'Tlemec?n',210,1),(210,'Australia',211,2),(211,'Meridional',212,1),(212,'Australia',213,2),(213,'Occidental',214,1),(214,'Nueva',215,2),(215,'Gales',216,1),(216,'del',217,2),(217,'Sur',218,1),(218,'Queensland',219,2),(219,'Tasmania',220,1),(220,'Territorio',221,2),(221,'de',222,1),(222,'la',223,2),(223,'Capital',224,1),(224,'Australiana',225,2),(225,'Territorio',226,1),(226,'del',227,2),(227,'Norte',228,1),(228,'Victoria',229,2),(229,'Viena',230,1),(230,'Gratz',231,2),(231,'Linz',232,1),(232,'Salzsburgo',233,2),(233,'Insbruck',234,1),(234,'Klagenfurt',235,2),(235,'Villach',236,1),(236,'Wels',237,2),(237,'Sankt',238,1),(238,'P?lten',239,2),(239,'Dornbirn',240,1),(240,'Wiener',241,2),(241,'Neustadt',242,1),(242,'Steyr',243,2),(243,'Feldkirch',244,1),(244,'Bregenz',245,2),(245,'Klosterneuburg',246,1),(246,'Wolfsberg',247,2),(247,'Baden',248,1),(248,'Leoben',249,2),(249,'Leonding',250,1),(250,'Traun',251,2),(251,'Amberes',252,1),(252,'Gante',253,2),(253,'Charleroi',254,1),(254,'Lieja',255,2),(255,'Bruselas',256,1),(256,'Brujas',257,2),(257,'Bruselas',258,1),(258,'Namur',259,2),(259,'Anderlecht',260,1),(260,'Lovaina',261,2),(261,'Mons',262,1),(262,'Molenbeek-Saint-Jean',263,2),(263,'Ixelles',264,1),(264,'Malinas',265,2),(265,'Aalst',266,1),(266,'La',267,2),(267,'Louvi?re',268,1),(268,'Uccle',269,2),(269,'Courtrai',270,1),(270,'Hasselt',271,2),(271,'San',272,1),(272,'Nicol?s',273,2),(273,'Coton?',274,1),(274,'Abomey',275,2),(275,'Calavi',276,1),(276,'Porto-Novo',277,2),(277,'Djougou',278,1),(278,'Banikoara',279,2),(279,'Parakou',280,1),(280,'Aplahou?',281,2),(281,'S?m?-Kpodji',282,1),(282,'Bohicon',283,2),(283,'Tchaourou',284,1),(284,'Savalou',285,2),(285,'Malanville',286,1),(286,'K?tou',287,2),(287,'Kalal?',288,1),(288,'Nikki',289,2),(289,'Ou?ss?',290,1),(290,'Djakotomey',291,2),(291,'Kandi',292,1),(292,'Bemb?r?k?',293,2),(293,'Dassa-Zoum?',294,1),(294,'Santa',295,2),(295,'CruzdelaSierra',296,1),(296,'ElAlto',297,2),(297,'LaPaz',298,1),(298,'Argel',156,1),(299,'Annaba',157,2),(300,'Batna',158,1);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `contadorestadoacta`
--

DROP TABLE IF EXISTS `contadorestadoacta`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoacta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoacta` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoactaac`
--

DROP TABLE IF EXISTS `contadorestadoactaac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoactaac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoactafinaly`
--

DROP TABLE IF EXISTS `contadorestadoactafinaly`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactafinaly`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoactafinaly` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoactain`
--

DROP TABLE IF EXISTS `contadorestadoactain`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoactain` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoactanul`
--

DROP TABLE IF EXISTS `contadorestadoactanul`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactanul`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoactanul` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoactaproce`
--

DROP TABLE IF EXISTS `contadorestadoactaproce`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactaproce`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoactaproce` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoactasuspen`
--

DROP TABLE IF EXISTS `contadorestadoactasuspen`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactasuspen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoactasuspen` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoac`
--

DROP TABLE IF EXISTS `contadorestadoalumnoac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoas`
--

DROP TABLE IF EXISTS `contadorestadoalumnoas`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoas` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoenespera`
--

DROP TABLE IF EXISTS `contadorestadoalumnoenespera`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoenespera`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoenespera` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoenformacion`
--

DROP TABLE IF EXISTS `contadorestadoalumnoenformacion`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoenformacion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoenformacion` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoin`
--

DROP TABLE IF EXISTS `contadorestadoalumnoin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoma`
--

DROP TABLE IF EXISTS `contadorestadoalumnoma`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoma`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoma` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnoretirado`
--

DROP TABLE IF EXISTS `contadorestadoalumnoretirado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoretirado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnoretirado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoalumnosuspen`
--

DROP TABLE IF EXISTS `contadorestadoalumnosuspen`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnosuspen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoalumnosuspen` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoasignaturaac`
--

DROP TABLE IF EXISTS `contadorestadoasignaturaac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoasignaturaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoasignaturaac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoasignaturain`
--

DROP TABLE IF EXISTS `contadorestadoasignaturain`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoasignaturain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoasignaturain` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocalificacionanul`
--

DROP TABLE IF EXISTS `contadorestadocalificacionanul`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocalificacionanul`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocalificacionanul` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocalificacionvigente`
--

DROP TABLE IF EXISTS `contadorestadocalificacionvigente`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocalificacionvigente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocalificacionvigente` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocampusac`
--

DROP TABLE IF EXISTS `contadorestadocampusac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocampusac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocampusac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocampusin`
--

DROP TABLE IF EXISTS `contadorestadocampusin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocampusin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocampusin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocargoac`
--

DROP TABLE IF EXISTS `contadorestadocargoac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocargoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocargoac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocargoin`
--

DROP TABLE IF EXISTS `contadorestadocargoin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocargoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocargoin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoconvocatoriaac`
--

DROP TABLE IF EXISTS `contadorestadoconvocatoriaac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoconvocatoriaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoconvocatoriaac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoconvocatoriain`
--

DROP TABLE IF EXISTS `contadorestadoconvocatoriain`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoconvocatoriain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoconvocatoriain` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocursoac`
--

DROP TABLE IF EXISTS `contadorestadocursoac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocursoac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocursoaplazado`
--

DROP TABLE IF EXISTS `contadorestadocursoaplazado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoaplazado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocursoaplazado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocursoenformacion`
--

DROP TABLE IF EXISTS `contadorestadocursoenformacion`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoenformacion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocursoenformacion` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocursoin`
--

DROP TABLE IF EXISTS `contadorestadocursoin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocursoin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocursosuspendido`
--

DROP TABLE IF EXISTS `contadorestadocursosuspendido`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursosuspendido`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocursosuspendido` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadocursoterminado`
--

DROP TABLE IF EXISTS `contadorestadocursoterminado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoterminado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadocursoterminado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadodepartamentoac`
--

DROP TABLE IF EXISTS `contadorestadodepartamentoac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadodepartamentoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadodepartamentoac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadodepartamentoin`
--

DROP TABLE IF EXISTS `contadorestadodepartamentoin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadodepartamentoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadodepartamentoin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoempleado`
--

DROP TABLE IF EXISTS `contadorestadoempleado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoempleado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoempleadocontratado`
--

DROP TABLE IF EXISTS `contadorestadoempleadocontratado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadocontratado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoempleadocontratado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoempleadodespedido`
--

DROP TABLE IF EXISTS `contadorestadoempleadodespedido`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadodespedido`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoempleadodespedido` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoempleadoin`
--

DROP TABLE IF EXISTS `contadorestadoempleadoin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoempleadoin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoempleadoincapacitado`
--

DROP TABLE IF EXISTS `contadorestadoempleadoincapacitado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadoincapacitado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoempleadoincapacitado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoempleadovacaciones`
--

DROP TABLE IF EXISTS `contadorestadoempleadovacaciones`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadovacaciones`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoempleadovacaciones` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoepsac`
--

DROP TABLE IF EXISTS `contadorestadoepsac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoepsac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoepsac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadoepsin`
--

DROP TABLE IF EXISTS `contadorestadoepsin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadoepsin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadoepsin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadogrupoac`
--

DROP TABLE IF EXISTS `contadorestadogrupoac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadogrupoac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadogrupoaplazado`
--

DROP TABLE IF EXISTS `contadorestadogrupoaplazado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoaplazado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadogrupoaplazado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadogrupocancelado`
--

DROP TABLE IF EXISTS `contadorestadogrupocancelado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupocancelado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadogrupocancelado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadogrupoenformacion`
--

DROP TABLE IF EXISTS `contadorestadogrupoenformacion`;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoenformacion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadogrupoenformacion` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadogrupoin`
--

DROP TABLE IF EXISTS `contadorestadogrupoin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadogrupoin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadogrupoterminado`
--

DROP TABLE IF EXISTS `contadorestadogrupoterminado`;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoterminado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadogrupoterminado` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadohistorialac`
--

DROP TABLE IF EXISTS `contadorestadohistorialac`;
/*!50001 DROP VIEW IF EXISTS `contadorestadohistorialac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadohistorialac` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadohistorialin`
--

DROP TABLE IF EXISTS `contadorestadohistorialin`;
/*!50001 DROP VIEW IF EXISTS `contadorestadohistorialin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadohistorialin` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `contadorestadomatriculain`
--

DROP TABLE IF EXISTS `contadorestadomatriculain`;
/*!50001 DROP VIEW IF EXISTS `contadorestadomatriculain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `contadorestadomatriculain` (
  `count(*)` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `convocatoria`
--

DROP TABLE IF EXISTS `convocatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convocatoria` (
  `Cod_convocatoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  `fkcod_estado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_convocatoria`),
  KEY `convocatoriaestado` (`fkcod_estado`),
  CONSTRAINT `convocatoriaestado` FOREIGN KEY (`fkcod_estado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convocatoria`
--

LOCK TABLES `convocatoria` WRITE;
/*!40000 ALTER TABLE `convocatoria` DISABLE KEYS */;
INSERT INTO `convocatoria` VALUES (1,'Abierta',1),(2,'Cerrada',1),(3,'Anticipada',1),(4,'Extraoficial',2),(5,'Nacional',2),(6,'Internacional',1),(7,'Oficiales',1);
/*!40000 ALTER TABLE `convocatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `Cod_curso` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_curso` varchar(50) DEFAULT NULL,
  `Maximo_alumnos` int(11) DEFAULT NULL,
  `Minimo_creditos_troncales` int(11) DEFAULT NULL,
  `Minimo_creditos_optativos` int(11) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkcodestadocurso` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_curso`),
  KEY `cursoestado` (`fkcodestado`),
  KEY `cursoestadocurso` (`fkcodestadocurso`),
  CONSTRAINT `cursoestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `cursoestadocurso` FOREIGN KEY (`fkcodestadocurso`) REFERENCES `estadocurso` (`Cod_estado_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'01',20,15,20,1,1),(2,'02',30,25,10,1,2),(3,'03',40,10,10,1,1),(4,'04',20,18,30,1,3),(5,'05',30,15,15,1,4),(6,'06',10,15,5,2,1),(7,'07',40,25,5,2,1),(8,'08',40,25,5,2,1),(9,'09',20,25,5,2,1),(10,'10',20,30,10,2,1),(11,'11',20,20,10,2,1),(12,'11',21,24,10,2,1),(13,'11',21,10,10,2,1),(14,'12',21,16,10,2,1),(15,'13',25,20,10,2,1),(16,'14',25,20,10,1,1),(17,'15',30,30,10,1,2),(18,'16',26,30,10,1,3),(19,'17',26,30,10,1,3),(20,'18',26,30,10,1,3),(21,'19',26,30,10,1,3),(22,'20',14,30,10,1,4),(23,'21',14,30,10,1,4),(24,'22',14,30,10,1,4),(25,'23',14,30,10,1,4),(26,'24',14,30,10,1,4),(27,'25',14,30,10,1,4),(28,'26',14,30,10,1,4),(29,'27',14,30,10,1,4),(30,'28',14,30,10,1,4),(31,'29',14,30,10,1,4),(32,'30',14,30,10,1,4),(33,'31',10,25,30,2,3),(34,'32',10,25,30,2,3),(35,'33',10,25,30,2,3),(36,'34',10,25,30,2,3),(37,'35',10,25,30,2,3),(38,'36',10,25,30,2,3),(39,'37',10,25,30,2,3),(40,'38',10,25,30,2,3),(41,'39',10,25,30,2,3),(42,'40',10,25,30,2,3),(43,'41',10,25,30,2,3),(44,'41',5,10,3,1,2),(45,'42',5,10,3,1,2),(46,'43',5,10,3,1,2),(47,'44',5,10,3,1,2),(48,'45',5,10,3,1,2),(49,'46',5,10,3,1,2),(50,'47',5,10,3,1,2),(51,'48',5,10,3,1,2),(52,'49',5,10,3,1,2),(53,'50',5,10,3,1,2),(54,'51',10,20,6,1,1),(55,'2',10,20,6,1,1),(56,'52',10,20,6,1,1),(57,'53',10,20,6,1,1),(58,'54',10,20,6,1,1),(59,'55',10,20,6,1,1),(60,'56',10,20,6,1,1),(61,'57',10,20,6,1,1),(62,'58',10,20,6,1,1),(63,'59',10,20,6,1,1),(64,'60',10,20,6,1,1),(65,'61',20,40,12,2,1),(66,'62',20,40,12,2,1),(67,'63',20,40,12,2,1),(68,'64',20,40,12,2,1),(69,'65',20,40,12,2,1),(70,'66',20,40,12,2,1),(71,'67',20,40,12,2,1),(72,'68',20,40,12,2,1),(73,'69',20,40,12,2,1),(74,'70',20,40,12,2,1),(75,'71',10,20,24,2,2),(76,'72',10,20,24,2,2),(77,'73',10,20,24,2,2),(78,'74',10,20,24,2,2),(79,'75',10,20,24,2,2),(80,'76',10,20,24,2,2),(81,'77',10,20,24,2,2),(82,'78',10,20,24,2,2),(83,'79',10,20,24,2,2),(84,'80',10,20,24,2,2),(85,'81',5,4,5,2,3),(86,'82',5,4,5,2,3),(87,'83',5,4,5,2,3),(88,'84',5,4,5,2,3),(89,'85',5,4,5,2,3),(90,'86',5,4,5,2,3),(91,'87',5,4,5,2,3),(92,'88',5,4,5,2,3),(93,'89',5,4,5,2,3),(94,'90',5,4,5,2,3),(95,'91',15,8,10,2,4),(96,'92',15,8,10,2,4),(97,'93',15,8,10,2,4),(98,'94',15,8,10,2,4),(99,'95',15,8,10,2,4),(100,'96',15,8,10,2,4),(101,'97',15,8,10,2,4),(102,'98',15,8,10,2,4),(103,'99',15,8,10,2,4),(104,'100',15,8,10,2,4),(105,'101',22,13,20,1,3),(106,'102',22,13,20,1,3),(107,'103',22,13,20,1,3),(108,'104',22,13,20,1,3),(109,'105',22,13,20,1,3),(110,'106',22,13,20,1,3),(111,'107',22,13,20,1,3),(112,'108',25,4,2,1,3),(113,'109',25,4,2,1,3),(114,'110',25,4,2,1,3),(115,'curso115',50,30,30,1,1),(116,'curso116',49,29,29,2,2),(117,'curso117',48,28,28,1,3),(118,'curso118',47,27,27,2,4),(119,'curso119',46,26,26,1,1),(120,'curso120',45,25,25,2,2),(121,'curso121',44,24,24,1,3),(122,'curso122',43,23,23,2,4),(123,'curso123',42,22,22,1,1),(124,'curso124',41,21,21,2,2),(125,'curso125',40,20,20,1,3),(126,'curso126',39,19,19,2,4),(127,'curso127',38,18,18,1,1),(128,'curso128',37,17,17,2,2),(129,'curso129',36,16,16,1,3),(130,'curso130',35,15,15,2,4),(131,'curso131',34,14,14,1,1),(132,'curso132',33,13,13,2,2),(133,'curso133',32,12,12,1,3),(134,'curso134',31,11,11,2,4),(135,'curso135',30,10,10,1,1),(136,'curso136',50,40,40,2,2),(137,'curso137',49,39,39,1,3),(138,'curso138',48,38,38,2,4),(139,'curso139',47,37,37,1,1),(140,'curso140',46,36,36,2,2),(141,'curso141',45,35,35,1,3),(142,'curso142',44,34,34,2,4),(143,'curso143',43,33,33,1,1),(144,'curso144',42,32,32,2,2),(145,'curso145',41,31,31,1,3),(146,'curso146',40,30,30,2,4),(147,'curso147',39,29,29,1,1),(148,'curso148',38,28,28,2,2),(149,'curso149',37,27,27,1,3),(150,'curso150',36,26,26,2,4),(151,'curso151',35,25,25,1,1),(152,'curso152',34,24,24,2,2),(153,'curso153',33,23,23,1,3),(154,'curso154',32,22,22,2,4),(155,'curso155',31,21,21,1,1),(156,'curso156',30,20,20,2,2),(157,'curso157',50,19,19,1,3),(158,'curso158',49,18,18,2,4),(159,'curso159',48,17,17,1,1),(160,'curso160',47,16,16,2,2),(161,'curso161',46,15,15,1,3),(162,'curso162',45,14,14,2,4),(163,'curso163',44,13,13,1,1),(164,'curso164',43,12,12,2,2),(165,'curso165',42,11,11,1,3),(166,'curso166',41,10,10,2,4),(167,'curso167',40,40,40,1,1),(168,'curso168',39,39,39,2,2),(169,'curso169',38,38,38,1,3),(170,'curso170',37,37,37,2,4),(171,'curso171',36,36,36,1,1),(172,'curso172',35,35,35,2,2),(173,'curso173',34,34,34,1,3),(174,'curso174',33,33,33,2,4),(175,'curso175',32,32,32,1,1),(176,'curso176',31,31,31,2,2),(177,'curso177',30,30,30,1,3),(178,'curso178',50,29,29,2,4),(179,'curso179',49,28,28,1,1),(180,'curso180',48,27,27,2,2),(181,'curso181',47,26,26,1,3),(182,'curso182',46,25,25,2,4),(183,'curso183',45,24,24,1,1),(184,'curso184',44,23,23,2,2),(185,'curso185',43,22,22,1,3),(186,'curso186',42,21,21,2,4),(187,'curso187',41,20,20,1,1),(188,'curso188',40,19,19,2,2),(189,'curso189',39,18,18,1,3),(190,'curso190',38,17,17,2,4),(191,'curso191',37,16,16,1,1),(192,'curso192',36,15,15,2,2),(193,'curso193',35,14,14,1,3),(194,'curso194',34,13,13,2,4),(195,'curso195',33,12,12,1,1),(196,'curso196',32,11,11,2,2),(197,'curso197',31,10,10,1,3),(198,'curso198',30,40,40,2,4),(199,'curso199',50,39,39,1,1),(200,'curso200',49,38,38,2,2),(201,'curso201',48,37,37,1,3),(202,'curso202',47,36,36,2,4),(203,'curso203',46,35,35,1,1),(204,'curso204',45,34,34,2,2),(205,'curso205',44,33,33,1,3),(206,'curso206',43,32,32,2,4),(207,'curso207',42,31,31,1,1),(208,'curso208',41,30,30,2,2),(209,'curso209',40,29,29,1,3),(210,'curso210',39,28,28,2,4),(211,'curso211',38,27,27,1,1),(212,'curso212',37,26,26,2,2),(213,'curso213',36,25,25,1,3),(214,'curso214',35,24,24,2,4),(215,'curso215',34,23,23,1,1),(216,'curso216',33,22,22,2,2),(217,'curso217',32,21,21,1,3),(218,'curso218',31,20,20,2,4),(219,'curso219',30,19,19,1,1),(220,'curso220',50,18,18,2,2),(221,'curso221',49,17,17,1,3),(222,'curso222',48,16,16,2,4),(223,'curso223',47,15,15,1,1),(224,'curso224',46,14,14,2,2),(225,'curso225',45,13,13,1,3),(226,'curso226',44,12,12,2,4),(227,'curso227',43,11,11,1,1),(228,'curso228',42,10,10,2,2),(229,'curso229',41,40,40,1,3),(230,'curso230',40,39,39,2,4),(231,'curso231',39,38,38,1,1),(232,'curso232',38,37,37,2,2),(233,'curso233',37,36,36,1,3),(234,'curso234',36,35,35,2,4),(235,'curso235',35,34,34,1,1),(236,'curso236',34,33,33,2,2),(237,'curso237',33,32,32,1,3),(238,'curso238',32,31,31,2,4),(239,'curso239',31,30,30,1,1),(240,'curso240',30,29,29,2,2),(241,'curso241',50,28,28,1,3),(242,'curso242',49,27,27,2,4),(243,'curso243',48,26,26,1,1),(244,'curso244',47,25,25,2,2),(245,'curso245',46,24,24,1,3),(246,'curso246',45,23,23,2,4),(247,'curso247',44,22,22,1,1),(248,'curso248',43,21,21,2,2),(249,'curso249',42,20,20,1,3),(250,'curso250',41,19,19,2,4),(251,'curso251',40,18,18,1,1),(252,'curso252',39,17,17,2,2),(253,'curso253',38,16,16,1,3),(254,'curso254',37,15,15,2,4),(255,'curso255',36,14,14,1,1),(256,'curso256',35,13,13,2,2),(257,'curso257',34,12,12,1,3),(258,'curso258',33,11,11,2,4),(259,'curso259',32,10,10,1,1),(260,'curso260',31,40,40,2,2),(261,'curso261',30,39,39,1,3),(262,'curso262',50,38,38,2,4),(263,'curso263',49,37,37,1,1),(264,'curso264',48,36,36,2,2),(265,'curso265',47,35,35,1,3),(266,'curso266',46,34,34,2,4),(267,'curso267',45,33,33,1,1),(268,'curso268',44,32,32,2,2),(269,'curso269',43,31,31,1,3),(270,'curso270',42,30,30,2,4),(271,'curso271',41,29,29,1,1),(272,'curso272',40,28,28,2,2),(273,'curso273',39,27,27,1,3),(274,'curso274',38,26,26,2,4),(275,'curso275',37,25,25,1,1),(276,'curso276',36,24,24,2,2),(277,'curso277',35,23,23,1,3),(278,'curso278',34,22,22,2,4),(279,'curso279',33,21,21,1,1),(280,'curso280',32,20,20,2,2),(281,'curso281',31,19,19,1,3),(282,'curso282',30,18,18,2,4),(283,'curso283',50,17,17,1,1),(284,'curso284',49,16,16,2,2),(285,'curso285',48,15,15,1,3),(286,'curso286',47,14,14,2,4),(287,'curso287',46,13,13,1,1),(288,'curso288',45,12,12,2,2),(289,'curso289',44,11,11,1,3),(290,'curso290',43,10,10,2,4),(291,'curso291',42,40,40,1,1),(292,'curso292',41,39,39,2,2),(293,'curso293',40,38,38,1,3),(294,'curso294',39,37,37,2,4),(295,'curso295',38,36,36,1,1),(296,'curso296',37,35,35,2,2),(297,'curso297',36,34,34,1,3),(298,'curso298',35,33,33,2,4),(299,'curso299',34,32,32,1,1),(300,'curso300',33,31,31,2,2);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `cod_departamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `fkcodpais` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`cod_departamento`),
  KEY `departamentopais` (`fkcodpais`),
  KEY `departamentoestado` (`fkcodestado`),
  CONSTRAINT `departamentoestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `departamentopais` FOREIGN KEY (`fkcodpais`) REFERENCES `pais` (`Cod_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES (1,'Provincia de Kabul',1,1),(2,'Distrito de berat',2,1),(3,'Bremen',3,1),(4,'Andorra',4,1),(5,'Bengo',5,1),(6,'Olaya',6,1),(7,'Adrar',7,1),(8,'Buenos aires',8,1),(9,'Armenia',9,1),(10,'Nueva gales del sur',10,1),(11,'Burgenland',11,1),(12,'Azerbaiyan',12,1),(13,'Nueva providencia',13,1),(14,'Division de daca',14,1),(15,'Antillas menores',15,1),(16,'Golfo persico',16,1),(17,'Golfo persico',17,1),(18,'Cayo',18,1),(19,'Oueme',19,1),(20,'Bielorrusia',20,1),(21,'Kyatpyae',21,1),(22,'Kyatpyae',2,1),(23,'Bosnia',23,1),(24,'Distrito sudeste',24,1),(25,'Distrito sudeste',25,1),(26,'Brunei y Muara',25,1),(27,'Brunei y Muara',27,1),(28,'Kadiogo',28,1),(29,'Kadiogo',29,1),(30,'Santiago',30,1),(31,'Santiago',31,1),(32,'Camerun',32,1),(33,'alberta',33,1),(34,'catar',34,1),(35,'region de yamena',35,1),(36,'metropolitana de santiago',36,1),(37,'china',37,1),(38,'nicosia',38,1),(39,'ciudad del vaticano',39,1),(40,' Amazonas',40,1),(41,'Antioquia',40,1),(42,'Arauca',40,1),(43,'Atlantico',40,1),(44,'Bolivar',40,1),(45,'Boyacá',40,1),(46,'Caldas',40,1),(47,'Caquetá',40,1),(48,'Casanare',40,1),(49,'Cauca',40,1),(50,'Cesar',40,1),(51,'Chocó',40,1),(52,'Córdoba',40,1),(53,'Cundinamarca',40,1),(54,'Guainía',40,1),(55,'Guaviare',40,1),(56,'Huila',40,1),(57,'La Guajira',40,1),(58,'Magdalena',40,1),(59,'Meta',40,1),(60,'Nariño',40,1),(61,'Norte de Santander',40,1),(62,'Putumayo',40,1),(63,'Quindio',40,1),(64,'Risaralda',40,1),(65,'San Andres y Providencia',40,1),(66,'Santander',40,1),(67,'Sucre',40,1),(68,'Tolima',40,1),(69,'Valle del Cauca',40,1),(70,'Vaupés',40,1),(71,'Vichada',40,1),(72,'conakri',73,1),(73,'oeste',74,1),(74,'Franciso Morazan',75,1),(75,'pest',76,1),(76,'Territorio Capital Nacional de Delhi',77,1),(77,'Región especial de Yakarta',78,1),(78,'Provincia de Bagdad',79,1),(79,'Irlanda',80,1),(80,'Islandia',81,1),(81,'Islas marshal',82,1),(82,'Islas salomon ',83,1),(83,'Jerusalen',84,1),(84,'Lacio',85,1),(85,'Jamaica',86,1),(86,'Kanto',87,1),(87,'Jordania',88,1),(88,'Akmola',89,1),(89,'Condado de Nairobi',90,1),(90,'kiribati',91,1),(91,'Kuwait',92,1),(92,'Laos',93,1),(93,'Lesoto',94,1),(94,'Letonia',95,1),(95,'Libano ',96,1),(96,'Liberia',97,1),(97,'Montserrado',98,1),(98,'Oberland',99,1),(99,'Luxemburgo',100,1),(100,'Mexico',110,1),(101,'camboya',100,1),(102,'Alabama',100,1),(103,'Missouri',101,2),(104,'Alaska',102,1),(105,'Montana',103,2),(106,'Arkansas',104,1),(107,'Nebraska',105,2),(108,'Arizona',106,1),(109,'Nevada',107,2),(110,'California',108,1),(111,'NewHampshire',109,2),(112,'Colorado',110,1),(113,'New',111,2),(114,'Jersey',112,1),(115,'Connecticut',113,2),(116,'New',114,1),(117,'Mexico',115,2),(118,'Dakota',116,1),(119,'del',117,2),(120,'Sur',118,1),(121,'Albany',119,2),(122,'Dover',120,1),(123,'North',121,2),(124,'Carolina',122,1),(125,'Florida',123,2),(126,'North',124,1),(127,'Dakota',125,2),(128,'Georgia',126,1),(129,'Atlanta',127,2),(130,'Ohio',128,1),(131,'Columbus',129,2),(132,'Hawaii',130,1),(133,'Honolulu',131,2),(134,'Oklahoma',132,1),(135,'Oklahoma',133,2),(136,'Idaho',134,1),(137,'Boise',135,2),(138,'Oregon',136,1),(139,'Salem',137,2),(140,'Illinois',138,1),(141,'Springfield',139,2),(142,'Pennsylvania',140,1),(143,'Harrisburg',141,2),(144,'Indiana',142,1),(145,'Indianapolis',143,2),(146,'Rhode',144,1),(147,'Island',145,2),(148,'Providence',146,1),(149,'Iowa',147,2),(150,'DesMoines',148,1),(151,'SouthCarolina',149,2),(152,'Columbia',150,1),(153,'Kansas',151,2),(154,'Topeka',152,1),(155,'Tennessee',153,2),(156,'Nashville',154,1),(157,'Kentucky',155,2),(158,'Frankfort',156,1),(159,'Texas',157,2),(160,'Austin',158,1),(161,'Louisiana',159,2),(162,'Baton',160,1),(163,'Rouge',161,2),(164,'Utah',162,1),(165,'Salt',163,2),(166,'Lake',164,1),(167,'City',165,2),(168,'Maine',166,1),(169,'Augusta',167,2),(170,'Vermont',168,1),(171,'Montpelier',169,2),(172,'Maryland',170,1),(173,'Annapolis',171,2),(174,'Virginia',172,1),(175,'Richmond',173,2),(176,'Massachussetts',174,1),(177,'Boston',175,2),(178,'Washington',176,1),(179,'Olympia',177,2),(180,'Michigan',178,1),(181,'Lansing',179,2),(182,'WestVirginia',180,1),(183,'Charleston',181,2),(184,'Minnesota',182,1),(185,'Saint',183,2),(186,'Paul',184,1),(187,'Wisconsin',185,2),(188,'Madison',186,1),(189,'Mississippi',187,2),(190,'Jackson',188,1),(191,'Wyoming',189,2),(192,'Baden-Wurtemberg',190,1),(193,'Baviera',191,2),(194,'Berl?n',192,1),(195,'Brandeburgo',193,2),(196,'Bremen',194,1),(197,'Hamburgo',195,2),(198,'Hesse',196,1),(199,'Mecklemburgo-Pomerania',197,2),(200,'Occidental',198,1),(201,'Baja',199,2),(202,'Sajonia',200,1),(203,'Renania',201,2),(204,'del',202,1),(205,'Norte-Westfalia',203,2),(206,'Renania-Palatinad',204,1),(207,'Sarre',205,2),(208,'Sajonia',206,1),(209,'Sajonia-Anhalt',207,2),(210,'Schleswig-Holstein',208,1),(211,'Turingia',209,2),(212,'Buenos',210,1),(213,'Aires',211,2),(214,'La',212,1),(215,'Plata',213,2),(216,'Catamarca',214,1),(217,'San',215,2),(218,'Fernando',216,1),(219,'del',217,2),(220,'Valle',218,1),(221,'de',219,2),(222,'Catamarca',220,1),(223,'C?rdoba',221,2),(224,'C?rdoba',222,1),(225,'Corrientes',223,2),(226,'Corrientes',224,1),(227,'Chaco',225,2),(228,'Resistencia',226,1),(229,'Chubut',227,2),(230,'Rawson',228,1),(231,'Entre',229,2),(232,'R?os',230,1),(233,'Paran?',231,2),(234,'Formosa',232,1),(235,'Formosa',233,2),(236,'Jujuy',234,1),(237,'San',235,2),(238,'Salvador',236,1),(239,'de',237,2),(240,'Jujuy',238,1),(241,'La',239,2),(242,'Pampa',240,1),(243,'Santa',241,2),(244,'Rosa',242,1),(245,'La',243,2),(246,'Rioja',244,1),(247,'La',245,2),(248,'Rioja',246,1),(249,'Mendoza',247,2),(250,'Mendoza',248,1),(251,'Misiones',249,2),(252,'Posadas',250,1),(253,'Neuqu?n',251,2),(254,'Neuqu?n',252,1),(255,'R?o',253,2),(256,'Negro',254,1),(257,'Viedma',255,2),(258,'Salta',256,1),(259,'Salta',257,2),(260,'San',258,1),(261,'Juan',259,2),(262,'San',260,1),(263,'Juan',261,2),(264,'San',262,1),(265,'Luis',263,2),(266,'San',264,1),(267,'Luis',265,2),(268,'Santa',266,1),(269,'Cruz',267,2),(270,'R?o',268,1),(271,'Gallegos',269,2),(272,'Santa',270,1),(273,'Fe',271,2),(274,'Santa',272,1),(275,'Fe',273,2),(276,'Santiago',274,1),(277,'del',275,2),(278,'Estero',276,1),(279,'Santiago',277,2),(280,'del',278,1),(281,'Estero',279,2),(282,'Tierra',280,1),(283,'del',281,2),(284,'Fuego,',282,1),(285,'Ant?rtida',283,2),(286,'e',284,1),(287,'Islas',285,2),(288,'del',286,1),(289,'Atl?ntico',287,2),(290,'Sur',288,1),(291,'Ushuaia',289,2),(292,'Tucum?n',290,1),(293,'Andaluc?a',291,2),(294,'Arag?n',292,1),(295,'Asturias',293,2),(296,'Cantabria',294,1),(297,'Castilla',295,2),(298,'CastillaMancha',296,1),(299,'Catalu?a',297,2),(300,'ComunidadValenciana',298,1);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecargo`
--

DROP TABLE IF EXISTS `detallecargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecargo` (
  `cod_detalle` bigint(20) NOT NULL AUTO_INCREMENT,
  `Fecha_inicio` date DEFAULT NULL,
  `fkcodcargo` bigint(20) DEFAULT NULL,
  `fkcodempleado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`cod_detalle`),
  KEY `detallecargocargo` (`fkcodcargo`),
  KEY `detallecargoempleado` (`fkcodempleado`),
  CONSTRAINT `detallecargocargo` FOREIGN KEY (`fkcodcargo`) REFERENCES `cargo` (`Cod_cargo`),
  CONSTRAINT `detallecargoempleado` FOREIGN KEY (`fkcodempleado`) REFERENCES `empleado` (`Cod_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecargo`
--

LOCK TABLES `detallecargo` WRITE;
/*!40000 ALTER TABLE `detallecargo` DISABLE KEYS */;
INSERT INTO `detallecargo` VALUES (1,'1900-01-01',1,1),(2,'1901-02-02',2,2),(3,'1902-03-03',3,3),(4,'1903-04-04',4,4),(5,'1904-05-05',5,5),(6,'1905-06-06',6,6),(7,'1906-07-07',7,7),(8,'1907-08-08',8,8),(9,'1908-09-09',9,9),(10,'1909-10-10',10,10),(11,'1910-11-11',11,11),(12,'1911-12-12',12,12),(13,'1912-01-13',13,13),(14,'1913-02-14',14,14),(15,'1914-03-15',15,15),(16,'1915-04-16',16,16),(17,'1916-05-17',1,17),(18,'1917-06-18',2,18),(19,'1918-07-19',3,19),(20,'1919-08-20',4,20),(21,'1920-09-21',5,21),(22,'1921-10-22',6,22),(23,'1922-11-23',7,23),(24,'1923-12-24',8,24),(25,'1924-01-25',9,25),(26,'1925-02-26',10,26),(27,'1926-03-27',11,27),(28,'1927-04-28',12,28),(29,'1928-05-29',13,29),(30,'1929-06-01',14,30),(31,'1930-07-02',15,31),(32,'1931-08-03',16,32),(33,'1932-09-04',1,33),(34,'1933-10-05',2,34),(35,'1934-11-06',3,35),(36,'1935-12-07',4,36),(37,'1936-01-08',5,37),(38,'1937-02-09',6,38),(39,'1938-03-10',7,39),(40,'1939-04-11',8,40),(41,'1940-05-12',9,41),(42,'1941-06-13',10,42),(43,'1942-07-14',11,43),(44,'1943-08-15',12,44),(45,'1944-09-16',13,45),(46,'1945-10-17',14,46),(47,'1946-11-18',15,47),(48,'1947-12-19',16,48),(49,'1948-01-20',1,49),(50,'1949-02-21',2,50),(51,'1950-03-22',3,51),(52,'1951-04-23',4,52),(53,'1952-05-24',5,53),(54,'1953-06-25',6,54),(55,'1954-07-26',7,55),(56,'1955-08-27',8,56),(57,'1956-09-28',9,57),(58,'1957-10-29',10,58),(59,'1958-11-01',11,59),(60,'1959-12-02',12,60),(61,'1960-01-03',13,61),(62,'1961-02-04',14,62),(63,'1962-03-05',15,63),(64,'1963-04-06',16,64),(65,'1964-05-07',1,65),(66,'1965-06-08',2,66),(67,'1966-07-09',3,67),(68,'1967-08-10',4,68),(69,'1968-09-11',5,69),(70,'1969-10-12',6,70),(71,'1970-11-13',7,71),(72,'1971-12-14',8,72),(73,'1972-01-15',9,73),(74,'1973-02-16',10,74),(75,'1974-03-17',11,75),(76,'1975-04-18',12,76),(77,'1976-05-19',13,77),(78,'1977-06-20',14,78),(79,'1978-07-21',15,79),(80,'1979-08-22',16,80),(81,'1980-09-23',1,81),(82,'1981-10-24',2,82),(83,'1982-11-25',3,83),(84,'1983-12-26',4,84),(85,'1984-01-27',5,85),(86,'1985-02-28',6,86),(87,'1986-03-29',7,87),(88,'1987-04-01',8,88),(89,'1988-05-02',9,89),(90,'1989-06-03',10,90),(91,'1990-07-04',11,91),(92,'1991-08-05',12,92),(93,'1992-09-06',13,93),(94,'1993-10-07',14,94),(95,'1994-11-08',15,95),(96,'1995-12-09',16,96),(97,'1996-01-10',1,97),(98,'1997-02-11',2,98),(99,'1998-03-12',3,99),(100,'1999-04-13',4,100),(101,'2000-05-14',5,1),(102,'2001-06-15',6,2),(103,'2002-07-16',7,3),(104,'2003-08-17',8,4),(105,'2004-09-18',9,5),(106,'2005-10-19',10,6),(107,'2006-11-20',11,7),(108,'2007-12-21',12,8),(109,'2008-01-22',13,9),(110,'2009-02-23',14,10),(111,'2010-03-24',15,11),(112,'2011-04-25',16,12),(113,'2012-05-26',1,13),(114,'2013-06-27',2,14),(115,'2014-07-28',3,15),(116,'2015-08-29',4,16),(117,'2016-09-01',5,17),(118,'2000-10-02',6,18),(119,'2001-11-03',7,19),(120,'2002-12-04',8,20),(121,'2003-01-05',9,21),(122,'2004-02-06',10,22),(123,'2005-03-07',11,23),(124,'2006-04-08',12,24),(125,'2007-05-09',13,25),(126,'2008-06-10',14,26),(127,'2009-07-11',15,27),(128,'2010-08-12',16,28),(129,'2011-09-13',1,29),(130,'2012-10-14',2,30),(131,'2013-11-15',3,31),(132,'2014-12-16',4,32),(133,'2015-01-17',5,33),(134,'2016-02-18',6,34),(135,'2000-03-19',7,35),(136,'2001-04-20',8,36),(137,'2002-05-21',9,37),(138,'2003-06-22',10,38),(139,'2004-07-23',11,39),(140,'2005-08-24',12,40),(141,'2006-09-25',13,41),(142,'2007-10-26',14,42),(143,'2008-11-27',15,43),(144,'2009-12-28',16,44),(145,'2010-01-29',1,45),(146,'2011-02-01',2,46),(147,'2012-03-02',3,47),(148,'2013-04-03',4,48),(149,'2014-05-04',5,49),(150,'2015-06-05',6,50),(151,'2016-07-06',7,51),(152,'2000-08-07',8,52),(153,'2001-09-08',9,53),(154,'2002-10-09',10,54),(155,'2003-11-10',11,55),(156,'2004-12-11',12,56),(157,'2005-01-12',13,57),(158,'2006-02-13',14,58),(159,'2007-03-14',15,59),(160,'2008-04-15',16,60),(161,'2009-05-16',1,61),(162,'2010-06-17',2,62),(163,'2011-07-18',3,63),(164,'2012-08-19',4,64),(165,'2013-09-20',5,65),(166,'2014-10-21',6,66),(167,'2015-11-22',7,67),(168,'2016-12-23',8,68),(169,'2000-01-24',9,69),(170,'2001-02-25',10,70),(171,'2002-03-26',11,71),(172,'2003-04-27',12,72),(173,'2004-05-28',13,73),(174,'2005-06-29',14,74),(175,'2006-07-01',15,75),(176,'2007-08-02',16,76),(177,'2008-09-03',1,77),(178,'2009-10-04',2,78),(179,'2010-11-05',3,79),(180,'2011-12-06',4,80),(181,'2012-01-07',5,81),(182,'2013-02-08',6,82),(183,'2014-03-09',7,83),(184,'2015-04-10',8,84),(185,'2016-05-11',9,85),(186,'2000-06-12',10,86),(187,'2001-07-13',11,87),(188,'2002-08-14',12,88),(189,'2003-09-15',13,89),(190,'2004-10-16',14,90),(191,'2005-11-17',15,91),(192,'2006-12-18',16,92),(193,'2007-01-19',1,93),(194,'2008-02-20',2,94),(195,'2009-03-21',3,95),(196,'2010-04-22',4,96),(197,'2011-05-23',5,97),(198,'2012-06-24',6,98),(199,'2013-07-25',7,99),(200,'2014-08-26',8,100),(201,'2015-09-27',9,1),(202,'2016-10-28',10,2),(203,'2000-11-29',11,3),(204,'2001-12-01',12,4),(205,'2002-01-02',13,5),(206,'2003-02-03',14,6),(207,'2004-03-04',15,7),(208,'2005-04-05',16,8),(209,'2006-05-06',1,9),(210,'2007-06-07',2,10),(211,'2008-07-08',3,11),(212,'2009-08-09',4,12),(213,'2010-09-10',5,13),(214,'2011-10-11',6,14),(215,'2012-11-12',7,15),(216,'2013-12-13',8,16),(217,'2014-01-14',9,17),(218,'2015-02-15',10,18),(219,'2016-03-16',11,19),(220,'2000-04-17',12,20),(221,'2001-05-18',13,21),(222,'2002-06-19',14,22),(223,'2003-07-20',15,23),(224,'2004-08-21',16,24),(225,'2005-09-22',1,25),(226,'2006-10-23',2,26),(227,'2007-11-24',3,27),(228,'2008-12-25',4,28),(229,'2009-01-26',5,29),(230,'2010-02-27',6,30),(231,'2011-03-28',7,31),(232,'2012-04-29',8,32),(233,'2013-05-01',9,33),(234,'2014-06-02',10,34),(235,'2015-07-03',11,35),(236,'2016-08-04',12,36),(237,'2000-09-05',13,37),(238,'2001-10-06',14,38),(239,'2002-11-07',15,39),(240,'2003-12-08',16,40),(241,'2004-01-09',1,41),(242,'2005-02-10',2,42),(243,'2006-03-11',3,43),(244,'2007-04-12',4,44),(245,'2008-05-13',5,45),(246,'2009-06-14',6,46),(247,'2010-07-15',7,47),(248,'2011-08-16',8,48),(249,'2012-09-17',9,49),(250,'2013-10-18',10,50),(251,'2014-11-19',11,51),(252,'2015-12-20',12,52),(253,'2016-01-21',13,53),(254,'2000-02-22',14,54),(255,'2001-03-23',15,55),(256,'2002-04-24',16,56),(257,'2003-05-25',1,57),(258,'2004-06-26',2,58),(259,'2005-07-27',3,59),(260,'2006-08-28',4,60),(261,'2007-09-29',5,61),(262,'2008-10-01',6,62),(263,'2009-11-02',7,63),(264,'2010-12-03',8,64),(265,'2011-01-04',9,65),(266,'2012-02-05',10,66),(267,'2013-03-06',11,67),(268,'2014-04-07',12,68),(269,'2015-05-08',13,69),(270,'2016-06-09',14,70),(271,'2000-07-10',15,71),(272,'2001-08-11',16,72),(273,'2002-09-12',1,73),(274,'2003-10-13',2,74),(275,'2004-11-14',3,75),(276,'2005-12-15',4,76),(277,'2006-01-16',5,77),(278,'2007-02-17',6,78),(279,'2008-03-18',7,79),(280,'2009-04-19',8,80),(281,'2010-05-20',9,81),(282,'2011-06-21',10,82),(283,'2012-07-22',11,83),(284,'2013-08-23',12,84),(285,'2014-09-24',13,85),(286,'2015-10-25',14,86),(287,'2016-11-26',15,87),(288,'2000-12-27',16,88),(289,'2001-01-28',1,89),(290,'0000-00-00',2,90),(291,'2003-03-01',3,91),(292,'2004-04-02',4,92),(293,'2005-05-03',5,93),(294,'2006-06-04',6,94),(295,'2007-07-05',7,95),(296,'2008-08-06',8,96),(297,'2009-09-07',9,97),(298,'2010-10-08',10,98),(299,'2011-11-09',11,99),(300,'2012-12-10',12,100);
/*!40000 ALTER TABLE `detallecargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `Cod_empleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Documento_empleado` bigint(20) DEFAULT NULL,
  `Nombre_empleado` varchar(50) DEFAULT NULL,
  `Apellido_empleado` varchar(50) DEFAULT NULL,
  `Telefono_empleado` bigint(20) DEFAULT NULL,
  `Correo_empleado` varchar(70) DEFAULT NULL,
  `fkcodciudad` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkcodestadoemp` bigint(20) DEFAULT NULL,
  `fkcodtipodoc` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_empleado`),
  KEY `empleadociudad` (`fkcodciudad`),
  KEY `empleadoestado` (`fkcodestado`),
  KEY `empleadoestadoempleado` (`fkcodestadoemp`),
  KEY `empleadotipodoc` (`fkcodtipodoc`),
  CONSTRAINT `empleadociudad` FOREIGN KEY (`fkcodciudad`) REFERENCES `ciudad` (`cod_ciudad`),
  CONSTRAINT `empleadoestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `empleadoestadoempleado` FOREIGN KEY (`fkcodestadoemp`) REFERENCES `estadoempleado` (`Cod_estado_emp`),
  CONSTRAINT `empleadotipodoc` FOREIGN KEY (`fkcodtipodoc`) REFERENCES `tipodocumento` (`Cod_tipo_doc`)
) ENGINE=InnoDB AUTO_INCREMENT=400 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,1033456878,'Horacio',' Escudero',3867068,'isntitutocalif@hotmail.com',1,1,1,1),(2,1033456877,'Óscar','Návar',3867067,'isntitutocalif@hotmail.com',2,1,1,1),(3,1033456876,'Enrique','Escobedo',3867066,'isntitutocalif@hotmail.com',3,1,1,1),(4,1033456875,'Francisco','Escobar Beltrán',3867065,'isntitutocalif@hotmail.com',4,1,1,1),(5,1033456874,'Rogelio Samuel','Escartín Morales',3867064,'isntitutocalif@hotmail.com',5,1,1,1),(6,1033456873,'César','Vásquez ',3867063,'isntitutocalif@hotmail.com',6,1,1,1),(7,1033456872,'Luis','Escamilla',3867062,'isntitutocalif@hotmail.com',7,1,1,1),(8,1033456871,'Carlos','Dzul',3867061,'isntitutocalif@hotmail.com',8,1,1,1),(9,1033456870,'Erazo','Bernal',3867060,'isntitutocalif@hotmail.com',9,1,1,1),(10,1033456869,'Alejandro','Flores',3867059,'isntitutocalif@hotmail.com',10,1,1,1),(11,1033456868,'Armando','Enríquez',3867058,'isntitutocalif@hotmail.com',11,1,1,1),(12,1033456867,'Jorge','Herrera',3867057,'isntitutocalif@hotmail.com',12,1,1,1),(13,1033456866,'Isaí','Elizalde',3867056,'isntitutocalif@hotmail.com',13,1,1,1),(14,1033456865,'César','Chi ',3867055,'isntitutocalif@hotmail.com',14,1,1,1),(15,1033456864,'Pedro','Suárez',3867054,'institutocalif@hotmail.com',15,1,1,1),(16,1033456863,'Rafael','Durán',3867053,'institutocalif@hotmail.com',16,1,1,1),(17,1033456862,'Suárez','Carlos',3867052,'institutocalif@hotmail.com',17,1,1,1),(18,1033456861,'Edmundo','Durán',3867051,'institutocalif@hotmail.com',18,1,1,1),(19,1033456860,'Raúl','San Vicente',3867050,'institutocalif@hotmail.com',19,1,1,1),(20,1033456859,'Julián','Durán ',3867049,'institutocalif@hotmail.com',20,1,1,1),(21,1033456858,'Alfredo','Durán de Jesús',3867048,'institutocalif@hotmail.com',21,1,1,1),(22,1033456857,'Briz','Jesús',3867047,'institutocalif@hotmail.com',22,1,1,1),(23,1033456856,'Ángel','Duarte',3867046,'institutocalif@hotmail.com',23,1,1,1),(24,1033456855,'Miguel','Velasco',3867045,'institutocalif@hotmail.com',24,1,1,1),(25,1033456854,'Gerardo','Domínguez ',3867044,'institutocalif@hotmail.com',25,1,1,1),(26,1033456853,' Romo','Domínguez',3867043,'institutocalif@hotmail.com',26,1,1,1),(27,1033456852,' Gabriel',' Barrios',3867042,'institutocalif@hotmail.com',27,1,1,1),(28,1033456851,'Vicente','Domínguez',3867041,'institutocalif@hotmail.com',28,1,1,1),(29,1033456850,'Vivaldo Jose','De León',3867040,'institutocalif@hotmail.com',29,1,1,1),(30,1033456849,'Julio Eduardo','Díaz ',3867039,'institutocalif@hotmail.com',30,1,1,1),(31,1033456848,'Juan Manuel','Díaz Sánchez',3867038,'institutocalif@hotmail.com',31,1,1,1),(32,1033456847,'Julio César','Díaz Núñez',3867037,'institutocalif@hotmail.com',32,1,1,1),(33,1033456846,'Morfín','Cruz Díaz',3867036,'institutocalif@hotmail.com',33,1,1,1),(34,1033456845,'Clemente','Díaz',3867035,'institutocalif@hotmail.com',34,1,1,1),(35,1033456844,'Carlos','Salgado',3867034,'institutocalif@hotmail.com',35,1,1,1),(36,1033456843,'Federico','Delgado',3867033,'institutocalif@hotmail.com',36,1,1,1),(37,1033456842,'Héctor','Guajardo',3867032,'institutocalif@hotmail.com',37,1,1,1),(38,1033456841,'Bugarín','Delgado',3867031,'institutocalif@hotmail.com',38,1,1,1),(39,1033456840,'Norma','Arreola',3867030,'institutocalif@hotmail.com',39,1,1,1),(40,1033456839,'David','Delgado',3867029,'institutocalif@hotmail.com',40,1,1,1),(41,1033456838,' ArmandoDel Toro','Ruiz',3867028,'institutocalif@hotmail.com',41,1,1,1),(42,1033456837,'AdánDel Razo','Ávalos',3867027,'institutocalif@hotmail.com',42,1,1,1),(43,1033456836,'Rodolfo','Sánchez',3867026,'institutocalif@hotmail.com',43,1,1,1),(44,1033456835,'Juan de Dios','Sánchez',3867025,'institutocalif@hotmail.com',44,1,1,1),(45,1033456834,'Guadalupe','VictorianaDe León',3867024,'institutocalif@hotmail.com',45,1,1,1),(46,1033456833,'Mario De León ','Martínez',3867023,'institutocalif@hotmail.com',46,1,1,1),(47,1033456832,'RobertoDe la Medina','Soto',3867022,'institutocalif@hotmail.com',47,1,1,1),(48,1033456831,'ValeriaDe La Fuente','Guerra',3867021,'institutocalif@hotmail.com',48,1,1,1),(49,1033456830,'Claudia','Montero',3867020,'institutocalif@hotmail.com',49,1,1,1),(50,1033456829,'Luis','Dávila ',3867019,'institutocalif@hotmail.com',50,1,1,1),(51,1033456828,'José ','Cuevas Nolasco',3867019,'institutocalif@hotmail.com',51,1,1,1),(52,1033456827,'Gilberto','López  ',3867018,'institutocalif@hotmail.com',52,1,1,1),(53,1033456826,'Arturo ','Cueto ',3867017,'institutocalif@hotmail.com',53,1,1,1),(54,1033456825,'Jesús ','Cuéllar Díaz',3867016,'institutocalif@hotmail.com',54,1,1,1),(55,1033456824,'Patricia','Avendaño',3867015,'institutocalif@hotmail.com',55,1,1,1),(56,1033456823,'Luis Abel','Cuéllar ',3867014,'institutocalif@hotmail.com',56,1,1,1),(57,1033456822,'Alejandro','Cruz Martínez',3867013,'institutocalif@hotmail.com',57,1,1,1),(58,1033456821,'Héctor','Cruz González',3867012,'institutocalif@hotmail.com',58,1,1,1),(59,1033456820,'José Luis','García',3867011,'institutocalif@hotmail.com',59,1,1,1),(60,1033456819,'Sergio','Ignacio Cruz',3867010,'institutocalif@hotmail.com',60,1,1,1),(61,1033456818,'José Luis','Cruz Carmona ',3867009,'institutocalif@hotmail.com',61,1,1,1),(62,1033456817,'Baltazar','Cruz Álvarez ',3867008,'institutocalif@hotmail.com',62,1,1,1),(63,1033456816,'Esperanza','Cortez Arias ',3867007,'institutocalif@hotmail.com',63,1,1,1),(64,1033456815,'Natalia ','Cortés Trujillo ',3867006,'institutocalif@hotmail.com',64,1,1,1),(65,1033456814,'José Eduardo ','Santos',3867005,'institutocalif@hotmail.com',65,1,1,1),(66,1033456813,'Saúl','Cortés',3867004,'institutocalif@hotmail.com',66,1,1,1),(67,1033456812,'Óscar','Cortés Ortiz ',3867003,'institutocalif@hotmail.com',67,1,1,1),(68,1033456811,'Aristeo ','García ',3867002,'institutocalif@hotmail.com',68,1,1,1),(69,1033456810,'Anuar','Sigfrido Cortés',3867001,'institutocalif@hotmail.com',69,1,1,1),(70,1033456809,'Claudia','Corro Ortiz ',3867000,'institutocalif@hotmail.com',70,1,1,1),(71,1033456808,'Andrade ','Rogelio Corrales ',3866001,'institutocalif@hotmail.com',71,1,1,1),(72,1033456878,'Horacio',' Escudero',3867068,'isntitutocalif@hotmail.com',1,1,1,1),(73,1033456877,'Óscar','Návar',3867067,'isntitutocalif@hotmail.com',2,1,1,1),(74,1033456876,'Enrique','Escobedo',3867066,'isntitutocalif@hotmail.com',3,1,1,1),(75,1033456875,'Francisco','Escobar Beltrán',3867065,'isntitutocalif@hotmail.com',4,1,1,1),(76,1033456874,'Rogelio Samuel','Escartín Morales',3867064,'isntitutocalif@hotmail.com',5,1,1,1),(77,1033456873,'César','Vásquez ',3867063,'isntitutocalif@hotmail.com',6,1,1,1),(78,1033456872,'Luis','Escamilla',3867062,'isntitutocalif@hotmail.com',7,1,1,1),(79,1033456871,'Carlos','Dzul',3867061,'isntitutocalif@hotmail.com',8,1,1,1),(80,1033456870,'Erazo','Bernal',3867060,'isntitutocalif@hotmail.com',9,1,1,1),(81,1033456869,'Alejandro','Flores',3867059,'isntitutocalif@hotmail.com',10,1,1,1),(82,1033456868,'Armando','Enríquez',3867058,'isntitutocalif@hotmail.com',11,1,1,1),(83,1033456867,'Jorge','Herrera',3867057,'isntitutocalif@hotmail.com',12,1,1,1),(84,1033456866,'Isaí','Elizalde',3867056,'isntitutocalif@hotmail.com',13,1,1,1),(85,1033456865,'César','Chi ',3867055,'isntitutocalif@hotmail.com',14,1,1,1),(86,1033456864,'Pedro','Suárez',3867054,'institutocalif@hotmail.com',15,1,1,1),(87,1033456863,'Rafael','Durán',3867053,'institutocalif@hotmail.com',16,1,1,1),(88,1033456862,'Suárez','Carlos',3867052,'institutocalif@hotmail.com',17,1,1,1),(89,1033456861,'Edmundo','Durán',3867051,'institutocalif@hotmail.com',18,1,1,1),(90,1033456860,'Raúl','San Vicente',3867050,'institutocalif@hotmail.com',19,1,1,1),(91,1033456859,'Julián','Durán ',3867049,'institutocalif@hotmail.com',20,1,1,1),(92,1033456858,'Alfredo','Durán de Jesús',3867048,'institutocalif@hotmail.com',21,1,1,1),(93,1033456857,'Briz','Jesús',3867047,'institutocalif@hotmail.com',22,1,1,1),(94,1033456856,'Ángel','Duarte',3867046,'institutocalif@hotmail.com',23,1,1,1),(95,1033456855,'Miguel','Velasco',3867045,'institutocalif@hotmail.com',24,1,1,1),(96,1033456854,'Gerardo','Domínguez ',3867044,'institutocalif@hotmail.com',25,1,1,1),(97,1033456853,' Romo','Domínguez',3867043,'institutocalif@hotmail.com',26,1,1,1),(98,1033456852,' Gabriel',' Barrios',3867042,'institutocalif@hotmail.com',27,1,1,1),(99,1033456851,'Vicente','Domínguez',3867041,'institutocalif@hotmail.com',28,1,1,1),(100,1033456780,'Luis','Castro Sánchez',3866171,'institutocalif@hotmail.com',100,1,1,1),(101,667781,'Mateo','Iv?n',56741,'empleado1@instituto.com',1,1,1,1),(102,667782,'Sebasti?n','Josu?',56742,'empleado2@instituto.com',2,2,2,2),(103,667783,'Alejandro','Crist?bal',56743,'empleado3@instituto.com',3,1,3,3),(104,667784,'Mat?as','Ciro',56744,'empleado4@instituto.com',4,2,4,1),(105,667785,'Diego','JuanDavid',56745,'empleado5@instituto.com',5,1,1,2),(106,667786,'Samuel','GARCIA',56746,'empleado6@instituto.com',6,2,2,3),(107,667787,'Nicol?s','GONZALEZ',56747,'empleado7@instituto.com',7,1,3,1),(108,667788,'Daniel','RODRIGUEZ',56748,'empleado8@instituto.com',8,2,4,2),(109,667789,'Mart?n','FERNANDEZ',56749,'empleado9@instituto.com',9,1,1,3),(110,6677810,'Benjam?n','LOPEZ',567410,'empleado10@instituto.com',10,2,2,1),(111,6677811,'Emiliano','MARTINEZ',567411,'empleado11@instituto.com',11,1,3,2),(112,6677812,'Leonardo','SANCHEZ',567412,'empleado12@instituto.com',12,2,4,3),(113,6677813,'Joaqu?n','EREZ',567413,'empleado13@instituto.com',13,1,1,1),(114,6677814,'Lucas','GOMEZ',567414,'empleado14@instituto.com',14,2,2,2),(115,6677815,'Iker','MARTIN',567415,'empleado15@instituto.com',15,1,3,3),(116,6677816,'Gabriel','JIMENEZ',567416,'empleado16@instituto.com',16,2,4,1),(117,6677817,'Thiago','RUIZ',567417,'empleado17@instituto.com',17,1,1,2),(118,6677818,'Adri?n','HERNANDEZ',567418,'empleado18@instituto.com',18,2,2,3),(119,6677819,'Bruno','DIAZ',567419,'empleado19@instituto.com',19,1,3,1),(120,6677820,'Dylan','MORENO',567420,'empleado20@instituto.com',20,2,4,2),(121,6677821,'Tom?s','ALVAREZ',567421,'empleado21@instituto.com',21,1,1,3),(122,6677822,'David','MU?OZ',567422,'empleado22@instituto.com',22,2,2,1),(123,6677823,'Agust?n','ROMERO',567423,'empleado23@instituto.com',23,1,3,2),(124,6677824,'Ian','ALONSO',567424,'empleado24@instituto.com',24,2,4,3),(125,6677825,'Ethan','GUTIERREZ',567425,'empleado25@instituto.com',25,1,1,1),(126,6677826,'Felipe','NAVARRO',567426,'empleado26@instituto.com',26,2,2,2),(127,6677827,'Maximiliano','TORRES',567427,'empleado27@instituto.com',27,1,3,3),(128,6677828,'Ericnue','DOMINGUEZ',567428,'empleado28@instituto.com',28,2,4,1),(129,6677829,'Hugo','VAZQUEZ',567429,'empleado29@instituto.com',29,1,1,2),(130,6677830,'Pablo','RAMOS',567430,'empleado30@instituto.com',30,2,2,3),(131,6677831,'Luca','GIL',567431,'empleado31@instituto.com',31,1,3,1),(132,6677832,'Rodrigo','RAMIREZ',567432,'empleado32@instituto.com',32,2,4,2),(133,6677833,'Ignacio','SERRANO',567433,'empleado33@instituto.com',33,1,1,3),(134,6677834,'Sim?n','BLANCO',567434,'empleado34@instituto.com',34,2,2,1),(135,6677835,'Carlos','SUAREZ',567435,'empleado35@instituto.com',35,1,3,2),(136,6677836,'Javier','MOLINA',567436,'empleado36@instituto.com',36,2,4,3),(137,6677837,'Juan','MORALES',567437,'empleado37@instituto.com',37,1,1,1),(138,6677838,'Isaac','ORTEGA',567438,'empleado38@instituto.com',38,2,2,2),(139,6677839,'Santino','DELGADO',567439,'empleado39@instituto.com',39,1,3,3),(140,6677840,'Manuel','CASTRO',567440,'empleado40@instituto.com',40,2,4,1),(141,6677841,'Jer?nimo','ORTIZ',567441,'empleado41@instituto.com',41,1,1,2),(142,6677842,'Emmanuel','RUBIO',567442,'empleado42@instituto.com',42,2,2,3),(143,6677843,'Aar?n','MARIN',567443,'empleado43@instituto.com',43,1,3,1),(144,6677844,'?ngel','SANZ',567444,'empleado44@instituto.com',44,2,4,2),(145,6677845,'Dante','NU?EZ',567445,'empleado45@instituto.com',45,1,1,3),(146,6677846,'Gael','IGLESIAS',567446,'empleado46@instituto.com',46,2,2,1),(147,6677847,'Vicente','MEDINA',567447,'empleado47@instituto.com',47,1,3,2),(148,6677848,'Juan','GARRIDO',567448,'empleado48@instituto.com',48,2,4,3),(149,6677849,'Sebasti?n','SANTOS',567449,'empleado49@instituto.com',49,1,1,1),(150,6677850,'Liam','CASTILLO',567450,'empleado50@instituto.com',50,2,2,2),(151,6677851,'Dami?n','CORTES',567451,'empleado51@instituto.com',51,1,3,3),(152,6677852,'Leo','LOZANO',567452,'empleado52@instituto.com',52,2,4,1),(153,6677853,'Francisco','GUERRERO',567453,'empleado53@instituto.com',53,1,1,2),(154,6677854,'Alonso','CANO',567454,'empleado54@instituto.com',54,2,2,3),(155,6677855,'Christopher','PRIETO',567455,'empleado55@instituto.com',55,1,3,1),(156,6677856,'?lvaro','MENDEZ',567456,'empleado56@instituto.com',56,2,4,2),(157,6677857,'Bautista','CALVO',567457,'empleado57@instituto.com',57,1,1,3),(158,6677858,'Miguel?ngel','CRUZ',567458,'empleado58@instituto.com',58,2,2,1),(159,6677859,'Valentino','GALLEGO',567459,'empleado59@instituto.com',59,1,3,2),(160,6677860,'Rafael','VIDAL',567460,'empleado60@instituto.com',60,2,4,3),(161,6677861,'Andr?s','LEON',567461,'empleado61@instituto.com',61,1,1,1),(162,6677862,'Franco','HERRERA',567462,'empleado62@instituto.com',62,2,2,2),(163,6677863,'Fernando','MARQUEZ',567463,'empleado63@instituto.com',63,1,3,3),(164,6677864,'Le?n','PE?A',567464,'empleado64@instituto.com',64,2,4,1),(165,6677865,'Oliver','CABRERA',567465,'empleado65@instituto.com',65,1,1,2),(166,6677866,'Emilio','FLORES',567466,'empleado66@instituto.com',66,2,2,3),(167,6677867,'Marcos','CAMPOS',567467,'empleado67@instituto.com',67,1,3,1),(168,6677868,'Juli?n','VEGA',567468,'empleado68@instituto.com',68,2,4,2),(169,6677869,'JuanJos?','DIEZ',567469,'empleado69@instituto.com',69,1,1,3),(170,6677870,'Pedro','FUENTES',567470,'empleado70@instituto.com',70,2,2,1),(171,6677871,'Alexander','CARRASCO',567471,'empleado71@instituto.com',71,1,3,2),(172,6677872,'Lorenzo','CABALLERO',567472,'empleado72@instituto.com',72,2,4,3),(173,6677873,'Mario','NIETO',567473,'empleado73@instituto.com',73,1,1,1),(174,6677874,'Sergio','REYES',567474,'empleado74@instituto.com',74,2,2,2),(175,6677875,'M?ximo','AGUILAR',567475,'empleado75@instituto.com',75,1,3,3),(176,6677876,'Cristian','PASCUAL',567476,'empleado76@instituto.com',76,2,4,1),(177,6677877,'Esteban','HERRERO',567477,'empleado77@instituto.com',77,1,1,2),(178,6677878,'El?as','SANTANA',567478,'empleado78@instituto.com',78,2,2,3),(179,6677879,'Antonio','LORENZO',567479,'empleado79@instituto.com',79,1,3,1),(180,6677880,'Luciano','HIDALGO',567480,'empleado80@instituto.com',80,2,4,2),(181,6677881,'Noah','MONTERO',567481,'empleado81@instituto.com',81,1,1,3),(182,6677882,'Jorge','IBA?EZ',567482,'empleado82@instituto.com',82,2,2,1),(183,6677883,'Enzo','GIMENEZ',567483,'empleado83@instituto.com',83,1,3,2),(184,6677884,'Axel','FERRER',567484,'empleado84@instituto.com',84,2,4,3),(185,6677885,'Salvador','DURAN',567485,'empleado85@instituto.com',85,1,1,1),(186,6677886,'Marc','VICENTE',567486,'empleado86@instituto.com',86,2,2,2),(187,6677887,'Derek','BENITEZ',567487,'empleado87@instituto.com',87,1,3,3),(188,6677888,'JuanMart?n','MORA',567488,'empleado88@instituto.com',88,2,4,1),(189,6677889,'Joelnuevo','SANTIAGO',567489,'empleado89@instituto.com',89,1,1,2),(190,6677890,'Juan','ARIAS',567490,'empleado90@instituto.com',90,2,2,3),(191,6677891,'Diego','VARGAS',567491,'empleado91@instituto.com',91,1,3,1),(192,6677892,'Gonzalo','CARMONA',567492,'empleado92@instituto.com',92,2,4,2),(193,6677893,'Kevin','CRESPO',567493,'empleado93@instituto.com',93,1,1,3),(194,6677894,'Alan','ROMAN',567494,'empleado94@instituto.com',94,2,2,1),(195,6677895,'Eduardo','PASTOR',567495,'empleado95@instituto.com',95,1,3,2),(196,6677896,'Miguel','SOTO',567496,'empleado96@instituto.com',96,2,4,3),(197,6677897,'Iv?n','SAEZ',567497,'empleado97@instituto.com',97,1,1,1),(198,6677898,'Josu?','VELASCO',567498,'empleado98@instituto.com',98,2,2,2),(199,6677899,'Crist?bal','SOLER',567499,'empleado99@instituto.com',99,1,3,3),(200,66778100,'Ciro','MOYA',5674100,'empleado100@instituto.com',100,2,4,1),(201,66778101,'JuanDavid','ESTEBAN',5674101,'empleado101@instituto.com',1,1,1,2),(202,66778102,'Santiago','PARRA',5674102,'empleado102@instituto.com',2,2,2,3),(203,66778103,'Mateo','BRAVO',5674103,'empleado103@instituto.com',3,1,3,1),(204,66778104,'Sebasti?n','GALLARDO',5674104,'empleado104@instituto.com',4,2,4,2),(205,66778105,'Alejandro','ROJAS',5674105,'empleado105@instituto.com',5,1,1,3),(206,66778106,'Mat?as','GARCIA',5674106,'empleado106@instituto.com',6,2,2,1),(207,66778107,'Diego','GONZALEZ',5674107,'empleado107@instituto.com',7,1,3,2),(208,66778108,'Samuel','RODRIGUEZ',5674108,'empleado108@instituto.com',8,2,4,3),(209,66778109,'Nicol?s','FERNANDEZ',5674109,'empleado109@instituto.com',9,1,1,1),(210,66778110,'Daniel','LOPEZ',5674110,'empleado110@instituto.com',10,2,2,2),(211,66778111,'Mart?n','MARTINEZ',5674111,'empleado111@instituto.com',11,1,3,3),(212,66778112,'Benjam?n','SANCHEZ',5674112,'empleado112@instituto.com',12,2,4,1),(213,66778113,'Emiliano','EREZ',5674113,'empleado113@instituto.com',13,1,1,2),(214,66778114,'Leonardo','GOMEZ',5674114,'empleado114@instituto.com',14,2,2,3),(215,66778115,'Joaqu?n','MARTIN',5674115,'empleado115@instituto.com',15,1,3,1),(216,66778116,'Lucas','JIMENEZ',5674116,'empleado116@instituto.com',16,2,4,2),(217,66778117,'Iker','RUIZ',5674117,'empleado117@instituto.com',17,1,1,3),(218,66778118,'Gabriel','HERNANDEZ',5674118,'empleado118@instituto.com',18,2,2,1),(219,66778119,'Thiago','DIAZ',5674119,'empleado119@instituto.com',19,1,3,2),(220,66778120,'Adri?n','MORENO',5674120,'empleado120@instituto.com',20,2,4,3),(221,66778121,'Bruno','ALVAREZ',5674121,'empleado121@instituto.com',21,1,1,1),(222,66778122,'Dylan','MU?OZ',5674122,'empleado122@instituto.com',22,2,2,2),(223,66778123,'Tom?s','ROMERO',5674123,'empleado123@instituto.com',23,1,3,3),(224,66778124,'David','ALONSO',5674124,'empleado124@instituto.com',24,2,4,1),(225,66778125,'Agust?n','GUTIERREZ',5674125,'empleado125@instituto.com',25,1,1,2),(226,66778126,'Ian','NAVARRO',5674126,'empleado126@instituto.com',26,2,2,3),(227,66778127,'Ethan','TORRES',5674127,'empleado127@instituto.com',27,1,3,1),(228,66778128,'Felipe','DOMINGUEZ',5674128,'empleado128@instituto.com',28,2,4,2),(229,66778129,'Maximiliano','VAZQUEZ',5674129,'empleado129@instituto.com',29,1,1,3),(230,66778130,'Ericnue','RAMOS',5674130,'empleado130@instituto.com',30,2,2,1),(231,66778131,'Hugo','GIL',5674131,'empleado131@instituto.com',31,1,3,2),(232,66778132,'Pablo','RAMIREZ',5674132,'empleado132@instituto.com',32,2,4,3),(233,66778133,'Luca','SERRANO',5674133,'empleado133@instituto.com',33,1,1,1),(234,66778134,'Rodrigo','BLANCO',5674134,'empleado134@instituto.com',34,2,2,2),(235,66778135,'Ignacio','SUAREZ',5674135,'empleado135@instituto.com',35,1,3,3),(236,66778136,'Sim?n','MOLINA',5674136,'empleado136@instituto.com',36,2,4,1),(237,66778137,'Carlos','MORALES',5674137,'empleado137@instituto.com',37,1,1,2),(238,66778138,'Javier','ORTEGA',5674138,'empleado138@instituto.com',38,2,2,3),(239,66778139,'Juan','DELGADO',5674139,'empleado139@instituto.com',39,1,3,1),(240,66778140,'Isaac','CASTRO',5674140,'empleado140@instituto.com',40,2,4,2),(241,66778141,'Santino','ORTIZ',5674141,'empleado141@instituto.com',41,1,1,3),(242,66778142,'Manuel','RUBIO',5674142,'empleado142@instituto.com',42,2,2,1),(243,66778143,'Jer?nimo','MARIN',5674143,'empleado143@instituto.com',43,1,3,2),(244,66778144,'Emmanuel','SANZ',5674144,'empleado144@instituto.com',44,2,4,3),(245,66778145,'Aar?n','NU?EZ',5674145,'empleado145@instituto.com',45,1,1,1),(246,66778146,'?ngel','IGLESIAS',5674146,'empleado146@instituto.com',46,2,2,2),(247,66778147,'Dante','MEDINA',5674147,'empleado147@instituto.com',47,1,3,3),(248,66778148,'Gael','GARRIDO',5674148,'empleado148@instituto.com',48,2,4,1),(249,66778149,'Vicente','SANTOS',5674149,'empleado149@instituto.com',49,1,1,2),(250,66778150,'Juan','CASTILLO',5674150,'empleado150@instituto.com',50,2,2,3),(251,66778151,'Sebasti?n','CORTES',5674151,'empleado151@instituto.com',51,1,3,1),(252,66778152,'Liam','LOZANO',5674152,'empleado152@instituto.com',52,2,4,2),(253,66778153,'Dami?n','GUERRERO',5674153,'empleado153@instituto.com',53,1,1,3),(254,66778154,'Leo','CANO',5674154,'empleado154@instituto.com',54,2,2,1),(255,66778155,'Francisco','PRIETO',5674155,'empleado155@instituto.com',55,1,3,2),(256,66778156,'Alonso','MENDEZ',5674156,'empleado156@instituto.com',56,2,4,3),(257,66778157,'Christopher','CALVO',5674157,'empleado157@instituto.com',57,1,1,1),(258,66778158,'?lvaro','CRUZ',5674158,'empleado158@instituto.com',58,2,2,2),(259,66778159,'Bautista','GALLEGO',5674159,'empleado159@instituto.com',59,1,3,3),(260,66778160,'Miguel?ngel','VIDAL',5674160,'empleado160@instituto.com',60,2,4,1),(261,66778161,'Valentino','LEON',5674161,'empleado161@instituto.com',61,1,1,2),(262,66778162,'Rafael','HERRERA',5674162,'empleado162@instituto.com',62,2,2,3),(263,66778163,'Andr?s','MARQUEZ',5674163,'empleado163@instituto.com',63,1,3,1),(264,66778164,'Franco','PE?A',5674164,'empleado164@instituto.com',64,2,4,2),(265,66778165,'Fernando','CABRERA',5674165,'empleado165@instituto.com',65,1,1,3),(266,66778166,'Le?n','FLORES',5674166,'empleado166@instituto.com',66,2,2,1),(267,66778167,'Oliver','CAMPOS',5674167,'empleado167@instituto.com',67,1,3,2),(268,66778168,'Emilio','VEGA',5674168,'empleado168@instituto.com',68,2,4,3),(269,66778169,'Marcos','DIEZ',5674169,'empleado169@instituto.com',69,1,1,1),(270,66778170,'Juli?n','FUENTES',5674170,'empleado170@instituto.com',70,2,2,2),(271,66778171,'JuanJos?','CARRASCO',5674171,'empleado171@instituto.com',71,1,3,3),(272,66778172,'Pedro','CABALLERO',5674172,'empleado172@instituto.com',72,2,4,1),(273,66778173,'Alexander','NIETO',5674173,'empleado173@instituto.com',73,1,1,2),(274,66778174,'Lorenzo','REYES',5674174,'empleado174@instituto.com',74,2,2,3),(275,66778175,'Mario','AGUILAR',5674175,'empleado175@instituto.com',75,1,3,1),(276,66778176,'Sergio','PASCUAL',5674176,'empleado176@instituto.com',76,2,4,2),(277,66778177,'M?ximo','HERRERO',5674177,'empleado177@instituto.com',77,1,1,3),(278,66778178,'Cristian','SANTANA',5674178,'empleado178@instituto.com',78,2,2,1),(279,66778179,'Esteban','LORENZO',5674179,'empleado179@instituto.com',79,1,3,2),(280,66778180,'El?as','HIDALGO',5674180,'empleado180@instituto.com',80,2,4,3),(281,66778181,'Antonio','MONTERO',5674181,'empleado181@instituto.com',81,1,1,1),(282,66778182,'Luciano','IBA?EZ',5674182,'empleado182@instituto.com',82,2,2,2),(283,66778183,'Noah','GIMENEZ',5674183,'empleado183@instituto.com',83,1,3,3),(284,66778184,'Jorge','FERRER',5674184,'empleado184@instituto.com',84,2,4,1),(285,66778185,'Enzo','DURAN',5674185,'empleado185@instituto.com',85,1,1,2),(286,66778186,'Axel','VICENTE',5674186,'empleado186@instituto.com',86,2,2,3),(287,66778187,'Salvador','BENITEZ',5674187,'empleado187@instituto.com',87,1,3,1),(288,66778188,'Marc','MORA',5674188,'empleado188@instituto.com',88,2,4,2),(289,66778189,'Derek','SANTIAGO',5674189,'empleado189@instituto.com',89,1,1,3),(290,66778190,'JuanMart?n','ARIAS',5674190,'empleado190@instituto.com',90,2,2,1),(291,66778191,'Joelnuevo','VARGAS',5674191,'empleado191@instituto.com',91,1,3,2),(292,66778192,'Juan','CARMONA',5674192,'empleado192@instituto.com',92,2,4,3),(293,66778193,'Diego','CRESPO',5674193,'empleado193@instituto.com',93,1,1,1),(294,66778194,'Gonzalo','ROMAN',5674194,'empleado194@instituto.com',94,2,2,2),(295,66778195,'Kevin','PASTOR',5674195,'empleado195@instituto.com',95,1,3,3),(296,66778196,'Alan','SOTO',5674196,'empleado196@instituto.com',96,2,4,1),(297,66778197,'Eduardo','SAEZ',5674197,'empleado197@instituto.com',97,1,1,2),(298,66778198,'Miguel','VELASCO',5674198,'empleado198@instituto.com',98,2,2,3),(299,66778199,'Iv?n','SOLER',5674199,'empleado199@instituto.com',99,1,3,1),(300,66778200,'Josu?','MOYA',5674200,'empleado200@instituto.com',100,2,4,2),(301,66778201,'Crist?bal','ESTEBAN',5674201,'empleado201@instituto.com',1,1,1,3),(302,66778202,'Ciro','PARRA',5674202,'empleado202@instituto.com',2,2,2,1),(303,66778203,'JuanDavid','BRAVO',5674203,'empleado203@instituto.com',3,1,3,2),(304,66778204,'Santiago','GALLARDO',5674204,'empleado204@instituto.com',4,2,4,3),(305,66778205,'Mateo','ROJAS',5674205,'empleado205@instituto.com',5,1,1,1),(306,66778206,'Sebasti?n','GARCIA',5674206,'empleado206@instituto.com',6,2,2,2),(307,66778207,'Alejandro','GONZALEZ',5674207,'empleado207@instituto.com',7,1,3,3),(308,66778208,'Mat?as','RODRIGUEZ',5674208,'empleado208@instituto.com',8,2,4,1),(309,66778209,'Diego','FERNANDEZ',5674209,'empleado209@instituto.com',9,1,1,2),(310,66778210,'Samuel','LOPEZ',5674210,'empleado210@instituto.com',10,2,2,3),(311,66778211,'Nicol?s','MARTINEZ',5674211,'empleado211@instituto.com',11,1,3,1),(312,66778212,'Daniel','SANCHEZ',5674212,'empleado212@instituto.com',12,2,4,2),(313,66778213,'Mart?n','EREZ',5674213,'empleado213@instituto.com',13,1,1,3),(314,66778214,'Benjam?n','GOMEZ',5674214,'empleado214@instituto.com',14,2,2,1),(315,66778215,'Emiliano','MARTIN',5674215,'empleado215@instituto.com',15,1,3,2),(316,66778216,'Leonardo','JIMENEZ',5674216,'empleado216@instituto.com',16,2,4,3),(317,66778217,'Joaqu?n','RUIZ',5674217,'empleado217@instituto.com',17,1,1,1),(318,66778218,'Lucas','HERNANDEZ',5674218,'empleado218@instituto.com',18,2,2,2),(319,66778219,'Iker','DIAZ',5674219,'empleado219@instituto.com',19,1,3,3),(320,66778220,'Gabriel','MORENO',5674220,'empleado220@instituto.com',20,2,4,1),(321,66778221,'Thiago','ALVAREZ',5674221,'empleado221@instituto.com',21,1,1,2),(322,66778222,'Adri?n','MU?OZ',5674222,'empleado222@instituto.com',22,2,2,3),(323,66778223,'Bruno','ROMERO',5674223,'empleado223@instituto.com',23,1,3,1),(324,66778224,'Dylan','ALONSO',5674224,'empleado224@instituto.com',24,2,4,2),(325,66778225,'Tom?s','GUTIERREZ',5674225,'empleado225@instituto.com',25,1,1,3),(326,66778226,'David','NAVARRO',5674226,'empleado226@instituto.com',26,2,2,1),(327,66778227,'Agust?n','TORRES',5674227,'empleado227@instituto.com',27,1,3,2),(328,66778228,'Ian','DOMINGUEZ',5674228,'empleado228@instituto.com',28,2,4,3),(329,66778229,'Ethan','VAZQUEZ',5674229,'empleado229@instituto.com',29,1,1,1),(330,66778230,'Felipe','RAMOS',5674230,'empleado230@instituto.com',30,2,2,2),(331,66778231,'Maximiliano','GIL',5674231,'empleado231@instituto.com',31,1,3,3),(332,66778232,'Ericnue','RAMIREZ',5674232,'empleado232@instituto.com',32,2,4,1),(333,66778233,'Hugo','SERRANO',5674233,'empleado233@instituto.com',33,1,1,2),(334,66778234,'Pablo','BLANCO',5674234,'empleado234@instituto.com',34,2,2,3),(335,66778235,'Luca','SUAREZ',5674235,'empleado235@instituto.com',35,1,3,1),(336,66778236,'Rodrigo','MOLINA',5674236,'empleado236@instituto.com',36,2,4,2),(337,66778237,'Ignacio','MORALES',5674237,'empleado237@instituto.com',37,1,1,3),(338,66778238,'Sim?n','ORTEGA',5674238,'empleado238@instituto.com',38,2,2,1),(339,66778239,'Carlos','DELGADO',5674239,'empleado239@instituto.com',39,1,3,2),(340,66778240,'Javier','CASTRO',5674240,'empleado240@instituto.com',40,2,4,3),(341,66778241,'Juan','ORTIZ',5674241,'empleado241@instituto.com',41,1,1,1),(342,66778242,'Isaac','RUBIO',5674242,'empleado242@instituto.com',42,2,2,2),(343,66778243,'Santino','MARIN',5674243,'empleado243@instituto.com',43,1,3,3),(344,66778244,'Manuel','SANZ',5674244,'empleado244@instituto.com',44,2,4,1),(345,66778245,'Jer?nimo','NU?EZ',5674245,'empleado245@instituto.com',45,1,1,2),(346,66778246,'Emmanuel','IGLESIAS',5674246,'empleado246@instituto.com',46,2,2,3),(347,66778247,'Aar?n','MEDINA',5674247,'empleado247@instituto.com',47,1,3,1),(348,66778248,'?ngel','GARRIDO',5674248,'empleado248@instituto.com',48,2,4,2),(349,66778249,'Dante','SANTOS',5674249,'empleado249@instituto.com',49,1,1,3),(350,66778250,'Gael','CASTILLO',5674250,'empleado250@instituto.com',50,2,2,1),(351,66778251,'Vicente','CORTES',5674251,'empleado251@instituto.com',51,1,3,2),(352,66778252,'Juan','LOZANO',5674252,'empleado252@instituto.com',52,2,4,3),(353,66778253,'Sebasti?n','GUERRERO',5674253,'empleado253@instituto.com',53,1,1,1),(354,66778254,'Liam','CANO',5674254,'empleado254@instituto.com',54,2,2,2),(355,66778255,'Dami?n','PRIETO',5674255,'empleado255@instituto.com',55,1,3,3),(356,66778256,'Leo','MENDEZ',5674256,'empleado256@instituto.com',56,2,4,1),(357,66778257,'Francisco','CALVO',5674257,'empleado257@instituto.com',57,1,1,2),(358,66778258,'Alonso','CRUZ',5674258,'empleado258@instituto.com',58,2,2,3),(359,66778259,'Christopher','GALLEGO',5674259,'empleado259@instituto.com',59,1,3,1),(360,66778260,'?lvaro','VIDAL',5674260,'empleado260@instituto.com',60,2,4,2),(361,66778261,'Bautista','LEON',5674261,'empleado261@instituto.com',61,1,1,3),(362,66778262,'Miguel?ngel','HERRERA',5674262,'empleado262@instituto.com',62,2,2,1),(363,66778263,'Valentino','MARQUEZ',5674263,'empleado263@instituto.com',63,1,3,2),(364,66778264,'Rafael','PE?A',5674264,'empleado264@instituto.com',64,2,4,3),(365,66778265,'Andr?s','CABRERA',5674265,'empleado265@instituto.com',65,1,1,1),(366,66778266,'Franco','FLORES',5674266,'empleado266@instituto.com',66,2,2,2),(367,66778267,'Fernando','CAMPOS',5674267,'empleado267@instituto.com',67,1,3,3),(368,66778268,'Le?n','VEGA',5674268,'empleado268@instituto.com',68,2,4,1),(369,66778269,'Oliver','DIEZ',5674269,'empleado269@instituto.com',69,1,1,2),(370,66778270,'Emilio','FUENTES',5674270,'empleado270@instituto.com',70,2,2,3),(371,66778271,'Marcos','CARRASCO',5674271,'empleado271@instituto.com',71,1,3,1),(372,66778272,'Juli?n','CABALLERO',5674272,'empleado272@instituto.com',72,2,4,2),(373,66778273,'JuanJos?','NIETO',5674273,'empleado273@instituto.com',73,1,1,3),(374,66778274,'Pedro','REYES',5674274,'empleado274@instituto.com',74,2,2,1),(375,66778275,'Alexander','AGUILAR',5674275,'empleado275@instituto.com',75,1,3,2),(376,66778276,'Lorenzo','PASCUAL',5674276,'empleado276@instituto.com',76,2,4,3),(377,66778277,'Mario','HERRERO',5674277,'empleado277@instituto.com',77,1,1,1),(378,66778278,'Sergio','SANTANA',5674278,'empleado278@instituto.com',78,2,2,2),(379,66778279,'M?ximo','LORENZO',5674279,'empleado279@instituto.com',79,1,3,3),(380,66778280,'Cristian','HIDALGO',5674280,'empleado280@instituto.com',80,2,4,1),(381,66778281,'Esteban','MONTERO',5674281,'empleado281@instituto.com',81,1,1,2),(382,66778282,'El?as','IBA?EZ',5674282,'empleado282@instituto.com',82,2,2,3),(383,66778283,'Antonio','GIMENEZ',5674283,'empleado283@instituto.com',83,1,3,1),(384,66778284,'Luciano','FERRER',5674284,'empleado284@instituto.com',84,2,4,2),(385,66778285,'Noah','DURAN',5674285,'empleado285@instituto.com',85,1,1,3),(386,66778286,'Jorge','VICENTE',5674286,'empleado286@instituto.com',86,2,2,1),(387,66778287,'Enzo','BENITEZ',5674287,'empleado287@instituto.com',87,1,3,2),(388,66778288,'Axel','MORA',5674288,'empleado288@instituto.com',88,2,4,3),(389,66778289,'Salvador','SANTIAGO',5674289,'empleado289@instituto.com',89,1,1,1),(390,66778290,'Marc','ARIAS',5674290,'empleado290@instituto.com',90,2,2,2),(391,66778291,'Derek','VARGAS',5674291,'empleado291@instituto.com',91,1,3,3),(392,66778292,'JuanMart?n','CARMONA',5674292,'empleado292@instituto.com',92,2,4,1),(393,66778293,'Joelnuevo','CRESPO',5674293,'empleado293@instituto.com',93,1,1,2),(394,66778294,'Juan','ROMAN',5674294,'empleado294@instituto.com',94,2,2,3),(395,66778295,'Diego','PASTOR',5674295,'empleado295@instituto.com',95,1,3,1),(396,66778296,'Gonzalo','SOTO',5674296,'empleado296@instituto.com',96,2,4,2),(397,66778297,'Kevin','SAEZ',5674297,'empleado297@instituto.com',97,1,1,3),(398,66778298,'Alan','VELASCO',5674298,'empleado298@instituto.com',98,2,2,1),(399,66778299,'Eduardo','SOLER',5674299,'empleado299@instituto.com',99,1,3,2);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `Cod_eps` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_eps`),
  KEY `epsestado` (`fkcodestado`),
  CONSTRAINT `epsestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
INSERT INTO `eps` VALUES (1,'sura',1),(2,'sanitas',1),(3,'compensar',1),(4,'coomeva',1),(5,'salud total',1),(6,'famisanar',2),(7,'comfenalco',2),(8,'nueva eps',2),(9,'coosalud',2),(10,'mutual ser',2),(11,' aliansalud',1),(12,' Emssanar',1),(13,'Comfarma',1),(14,'Cafe Salud',1),(15,'Saludvida',1),(16,'Capital salud',1),(17,'Salud Coop',1),(18,'Sisben',1);
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `Cod_estado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoacta`
--

DROP TABLE IF EXISTS `estadoacta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoacta` (
  `Cod_estado_acta` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_acta`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoacta`
--

LOCK TABLES `estadoacta` WRITE;
/*!40000 ALTER TABLE `estadoacta` DISABLE KEYS */;
INSERT INTO `estadoacta` VALUES (1,'En proceso'),(2,'Finalizada'),(3,'Anulada'),(4,'Suspendida');
/*!40000 ALTER TABLE `estadoacta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadoactaac`
--

DROP TABLE IF EXISTS `estadoactaac`;
/*!50001 DROP VIEW IF EXISTS `estadoactaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoactaac` (
  `cod_acta` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoactaanul`
--

DROP TABLE IF EXISTS `estadoactaanul`;
/*!50001 DROP VIEW IF EXISTS `estadoactaanul`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoactaanul` (
  `cod_acta` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoactafinaly`
--

DROP TABLE IF EXISTS `estadoactafinaly`;
/*!50001 DROP VIEW IF EXISTS `estadoactafinaly`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoactafinaly` (
  `cod_acta` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoactain`
--

DROP TABLE IF EXISTS `estadoactain`;
/*!50001 DROP VIEW IF EXISTS `estadoactain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoactain` (
  `cod_acta` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoactaproce`
--

DROP TABLE IF EXISTS `estadoactaproce`;
/*!50001 DROP VIEW IF EXISTS `estadoactaproce`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoactaproce` (
  `cod_acta` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoactasuspen`
--

DROP TABLE IF EXISTS `estadoactasuspen`;
/*!50001 DROP VIEW IF EXISTS `estadoactasuspen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoactasuspen` (
  `cod_acta` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadoalumno`
--

DROP TABLE IF EXISTS `estadoalumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoalumno` (
  `Cod_estado_alumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_alumno`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoalumno`
--

LOCK TABLES `estadoalumno` WRITE;
/*!40000 ALTER TABLE `estadoalumno` DISABLE KEYS */;
INSERT INTO `estadoalumno` VALUES (1,'Aspirante'),(2,'Matriculado'),(3,'En formacion'),(4,'Suspendido'),(5,'En espera'),(6,'Retirado');
/*!40000 ALTER TABLE `estadoalumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadoalumnoac`
--

DROP TABLE IF EXISTS `estadoalumnoac`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoac` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnoas`
--

DROP TABLE IF EXISTS `estadoalumnoas`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoas` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnoenespera`
--

DROP TABLE IF EXISTS `estadoalumnoenespera`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoenespera`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoenespera` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnoenformacion`
--

DROP TABLE IF EXISTS `estadoalumnoenformacion`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoenformacion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoenformacion` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnoin`
--

DROP TABLE IF EXISTS `estadoalumnoin`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoin` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnoma`
--

DROP TABLE IF EXISTS `estadoalumnoma`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoma`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoma` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnoretirado`
--

DROP TABLE IF EXISTS `estadoalumnoretirado`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoretirado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnoretirado` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoalumnosuspen`
--

DROP TABLE IF EXISTS `estadoalumnosuspen`;
/*!50001 DROP VIEW IF EXISTS `estadoalumnosuspen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoalumnosuspen` (
  `Doc_alumno` bigint(20),
  `Nombre_alumno` varchar(30),
  `Apellido_alumno` varchar(30),
  `telefono` bigint(20),
  `correo` varchar(70),
  `fkcodestado` bigint(20),
  `fkcodestadoalumno` bigint(20),
  `fkcodmatricula` bigint(20),
  `fkcodciudad` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoasignaturaac`
--

DROP TABLE IF EXISTS `estadoasignaturaac`;
/*!50001 DROP VIEW IF EXISTS `estadoasignaturaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoasignaturaac` (
  `Cod_asignatura` bigint(20),
  `Nombre_asiganturadenominacion` varchar(50),
  `fkdocalumno` bigint(20),
  `fkcodcurso` bigint(20),
  `fkcodcampus` bigint(20),
  `fkcodtitulacion` bigint(20),
  `fkcodconvocatoria` bigint(20),
  `fkcodgrupo` bigint(20),
  `fkcodempleado` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodarea` bigint(20),
  `fkcodcalificacion` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoasignaturain`
--

DROP TABLE IF EXISTS `estadoasignaturain`;
/*!50001 DROP VIEW IF EXISTS `estadoasignaturain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoasignaturain` (
  `Cod_asignatura` bigint(20),
  `Nombre_asiganturadenominacion` varchar(50),
  `fkdocalumno` bigint(20),
  `fkcodcurso` bigint(20),
  `fkcodcampus` bigint(20),
  `fkcodtitulacion` bigint(20),
  `fkcodconvocatoria` bigint(20),
  `fkcodgrupo` bigint(20),
  `fkcodempleado` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodarea` bigint(20),
  `fkcodcalificacion` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadocalificacion`
--

DROP TABLE IF EXISTS `estadocalificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadocalificacion` (
  `Cod_estado_calif` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_calif`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadocalificacion`
--

LOCK TABLES `estadocalificacion` WRITE;
/*!40000 ALTER TABLE `estadocalificacion` DISABLE KEYS */;
INSERT INTO `estadocalificacion` VALUES (5,'Anulada'),(6,'Vigente');
/*!40000 ALTER TABLE `estadocalificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadocalificacionanul`
--

DROP TABLE IF EXISTS `estadocalificacionanul`;
/*!50001 DROP VIEW IF EXISTS `estadocalificacionanul`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocalificacionanul` (
  `Cod_calificacion` bigint(20),
  `Descripcion` double,
  `fkCod_estado_calificacion` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocalificacionvigente`
--

DROP TABLE IF EXISTS `estadocalificacionvigente`;
/*!50001 DROP VIEW IF EXISTS `estadocalificacionvigente`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocalificacionvigente` (
  `Cod_calificacion` bigint(20),
  `Descripcion` double,
  `fkCod_estado_calificacion` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocampusac`
--

DROP TABLE IF EXISTS `estadocampusac`;
/*!50001 DROP VIEW IF EXISTS `estadocampusac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocampusac` (
  `Cod_campus` bigint(20),
  `Nombre` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocampusin`
--

DROP TABLE IF EXISTS `estadocampusin`;
/*!50001 DROP VIEW IF EXISTS `estadocampusin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocampusin` (
  `Cod_campus` bigint(20),
  `Nombre` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocargoac`
--

DROP TABLE IF EXISTS `estadocargoac`;
/*!50001 DROP VIEW IF EXISTS `estadocargoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocargoac` (
  `Cod_cargo` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocargoin`
--

DROP TABLE IF EXISTS `estadocargoin`;
/*!50001 DROP VIEW IF EXISTS `estadocargoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocargoin` (
  `Cod_cargo` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoconvocatoriaac`
--

DROP TABLE IF EXISTS `estadoconvocatoriaac`;
/*!50001 DROP VIEW IF EXISTS `estadoconvocatoriaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoconvocatoriaac` (
  `Cod_convocatoria` bigint(20),
  `Descripcion` varchar(50),
  `fkcod_estado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoconvocatoriain`
--

DROP TABLE IF EXISTS `estadoconvocatoriain`;
/*!50001 DROP VIEW IF EXISTS `estadoconvocatoriain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoconvocatoriain` (
  `Cod_convocatoria` bigint(20),
  `Descripcion` varchar(50),
  `fkcod_estado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadocurso`
--

DROP TABLE IF EXISTS `estadocurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadocurso` (
  `Cod_estado_curso` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadocurso`
--

LOCK TABLES `estadocurso` WRITE;
/*!40000 ALTER TABLE `estadocurso` DISABLE KEYS */;
INSERT INTO `estadocurso` VALUES (1,'En formacion'),(2,'Suspendido'),(3,'Aplazado'),(4,'Terminado');
/*!40000 ALTER TABLE `estadocurso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadocursoac`
--

DROP TABLE IF EXISTS `estadocursoac`;
/*!50001 DROP VIEW IF EXISTS `estadocursoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocursoac` (
  `Cod_curso` bigint(20),
  `Nombre_curso` varchar(50),
  `Maximo_alumnos` int(11),
  `Minimo_creditos_troncales` int(11),
  `Minimo_creditos_optativos` int(11),
  `fkcodestado` bigint(20),
  `fkcodestadocurso` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocursoaplazado`
--

DROP TABLE IF EXISTS `estadocursoaplazado`;
/*!50001 DROP VIEW IF EXISTS `estadocursoaplazado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocursoaplazado` (
  `Cod_curso` bigint(20),
  `Nombre_curso` varchar(50),
  `Maximo_alumnos` int(11),
  `Minimo_creditos_troncales` int(11),
  `Minimo_creditos_optativos` int(11),
  `fkcodestado` bigint(20),
  `fkcodestadocurso` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocursoenformacion`
--

DROP TABLE IF EXISTS `estadocursoenformacion`;
/*!50001 DROP VIEW IF EXISTS `estadocursoenformacion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocursoenformacion` (
  `Cod_curso` bigint(20),
  `Nombre_curso` varchar(50),
  `Maximo_alumnos` int(11),
  `Minimo_creditos_troncales` int(11),
  `Minimo_creditos_optativos` int(11),
  `fkcodestado` bigint(20),
  `fkcodestadocurso` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocursoin`
--

DROP TABLE IF EXISTS `estadocursoin`;
/*!50001 DROP VIEW IF EXISTS `estadocursoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocursoin` (
  `Cod_curso` bigint(20),
  `Nombre_curso` varchar(50),
  `Maximo_alumnos` int(11),
  `Minimo_creditos_troncales` int(11),
  `Minimo_creditos_optativos` int(11),
  `fkcodestado` bigint(20),
  `fkcodestadocurso` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocursosuspendido`
--

DROP TABLE IF EXISTS `estadocursosuspendido`;
/*!50001 DROP VIEW IF EXISTS `estadocursosuspendido`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocursosuspendido` (
  `Cod_curso` bigint(20),
  `Nombre_curso` varchar(50),
  `Maximo_alumnos` int(11),
  `Minimo_creditos_troncales` int(11),
  `Minimo_creditos_optativos` int(11),
  `fkcodestado` bigint(20),
  `fkcodestadocurso` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadocursoterminado`
--

DROP TABLE IF EXISTS `estadocursoterminado`;
/*!50001 DROP VIEW IF EXISTS `estadocursoterminado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadocursoterminado` (
  `Cod_curso` bigint(20),
  `Nombre_curso` varchar(50),
  `Maximo_alumnos` int(11),
  `Minimo_creditos_troncales` int(11),
  `Minimo_creditos_optativos` int(11),
  `fkcodestado` bigint(20),
  `fkcodestadocurso` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadodepartamentoac`
--

DROP TABLE IF EXISTS `estadodepartamentoac`;
/*!50001 DROP VIEW IF EXISTS `estadodepartamentoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadodepartamentoac` (
  `cod_departamento` bigint(20),
  `Nombre` varchar(50),
  `fkcodpais` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadodepartamentoin`
--

DROP TABLE IF EXISTS `estadodepartamentoin`;
/*!50001 DROP VIEW IF EXISTS `estadodepartamentoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadodepartamentoin` (
  `cod_departamento` bigint(20),
  `Nombre` varchar(50),
  `fkcodpais` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadoempleado`
--

DROP TABLE IF EXISTS `estadoempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoempleado` (
  `Cod_estado_emp` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_emp`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoempleado`
--

LOCK TABLES `estadoempleado` WRITE;
/*!40000 ALTER TABLE `estadoempleado` DISABLE KEYS */;
INSERT INTO `estadoempleado` VALUES (1,'Contratado'),(2,'Incapacitado'),(3,'Despedido'),(4,'Vacaciones');
/*!40000 ALTER TABLE `estadoempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadoempleadoac`
--

DROP TABLE IF EXISTS `estadoempleadoac`;
/*!50001 DROP VIEW IF EXISTS `estadoempleadoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoempleadoac` (
  `Cod_empleado` bigint(20),
  `Documento_empleado` bigint(20),
  `Nombre_empleado` varchar(50),
  `Apellido_empleado` varchar(50),
  `Telefono_empleado` bigint(20),
  `Correo_empleado` varchar(70),
  `fkcodciudad` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodestadoemp` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoempleadocontratado`
--

DROP TABLE IF EXISTS `estadoempleadocontratado`;
/*!50001 DROP VIEW IF EXISTS `estadoempleadocontratado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoempleadocontratado` (
  `Cod_empleado` bigint(20),
  `Documento_empleado` bigint(20),
  `Nombre_empleado` varchar(50),
  `Apellido_empleado` varchar(50),
  `Telefono_empleado` bigint(20),
  `Correo_empleado` varchar(70),
  `fkcodciudad` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodestadoemp` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoempleadodespedido`
--

DROP TABLE IF EXISTS `estadoempleadodespedido`;
/*!50001 DROP VIEW IF EXISTS `estadoempleadodespedido`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoempleadodespedido` (
  `Cod_empleado` bigint(20),
  `Documento_empleado` bigint(20),
  `Nombre_empleado` varchar(50),
  `Apellido_empleado` varchar(50),
  `Telefono_empleado` bigint(20),
  `Correo_empleado` varchar(70),
  `fkcodciudad` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodestadoemp` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoempleadoin`
--

DROP TABLE IF EXISTS `estadoempleadoin`;
/*!50001 DROP VIEW IF EXISTS `estadoempleadoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoempleadoin` (
  `Cod_empleado` bigint(20),
  `Documento_empleado` bigint(20),
  `Nombre_empleado` varchar(50),
  `Apellido_empleado` varchar(50),
  `Telefono_empleado` bigint(20),
  `Correo_empleado` varchar(70),
  `fkcodciudad` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodestadoemp` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoempleadoincapacitado`
--

DROP TABLE IF EXISTS `estadoempleadoincapacitado`;
/*!50001 DROP VIEW IF EXISTS `estadoempleadoincapacitado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoempleadoincapacitado` (
  `Cod_empleado` bigint(20),
  `Documento_empleado` bigint(20),
  `Nombre_empleado` varchar(50),
  `Apellido_empleado` varchar(50),
  `Telefono_empleado` bigint(20),
  `Correo_empleado` varchar(70),
  `fkcodciudad` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodestadoemp` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoempleadovacaciones`
--

DROP TABLE IF EXISTS `estadoempleadovacaciones`;
/*!50001 DROP VIEW IF EXISTS `estadoempleadovacaciones`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoempleadovacaciones` (
  `Cod_empleado` bigint(20),
  `Documento_empleado` bigint(20),
  `Nombre_empleado` varchar(50),
  `Apellido_empleado` varchar(50),
  `Telefono_empleado` bigint(20),
  `Correo_empleado` varchar(70),
  `fkcodciudad` bigint(20),
  `fkcodestado` bigint(20),
  `fkcodestadoemp` bigint(20),
  `fkcodtipodoc` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoepsac`
--

DROP TABLE IF EXISTS `estadoepsac`;
/*!50001 DROP VIEW IF EXISTS `estadoepsac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoepsac` (
  `Cod_eps` bigint(20),
  `Nombre` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadoepsin`
--

DROP TABLE IF EXISTS `estadoepsin`;
/*!50001 DROP VIEW IF EXISTS `estadoepsin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadoepsin` (
  `Cod_eps` bigint(20),
  `Nombre` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadogrupo`
--

DROP TABLE IF EXISTS `estadogrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadogrupo` (
  `Cod_estado_grupo` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadogrupo`
--

LOCK TABLES `estadogrupo` WRITE;
/*!40000 ALTER TABLE `estadogrupo` DISABLE KEYS */;
INSERT INTO `estadogrupo` VALUES (1,'En formacion'),(2,'Aplazado'),(3,'Cancelado'),(4,'Terminado');
/*!40000 ALTER TABLE `estadogrupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadogrupoac`
--

DROP TABLE IF EXISTS `estadogrupoac`;
/*!50001 DROP VIEW IF EXISTS `estadogrupoac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadogrupoac` (
  `Cod_grupo` bigint(20),
  `Num_max_alumnos` int(11),
  `fkcodjornada` bigint(20),
  `fkcodestadogrupo` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadogrupoaplazado`
--

DROP TABLE IF EXISTS `estadogrupoaplazado`;
/*!50001 DROP VIEW IF EXISTS `estadogrupoaplazado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadogrupoaplazado` (
  `Cod_grupo` bigint(20),
  `Num_max_alumnos` int(11),
  `fkcodjornada` bigint(20),
  `fkcodestadogrupo` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadogrupocancelado`
--

DROP TABLE IF EXISTS `estadogrupocancelado`;
/*!50001 DROP VIEW IF EXISTS `estadogrupocancelado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadogrupocancelado` (
  `Cod_grupo` bigint(20),
  `Num_max_alumnos` int(11),
  `fkcodjornada` bigint(20),
  `fkcodestadogrupo` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadogrupoenformacion`
--

DROP TABLE IF EXISTS `estadogrupoenformacion`;
/*!50001 DROP VIEW IF EXISTS `estadogrupoenformacion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadogrupoenformacion` (
  `Cod_grupo` bigint(20),
  `Num_max_alumnos` int(11),
  `fkcodjornada` bigint(20),
  `fkcodestadogrupo` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadogrupoin`
--

DROP TABLE IF EXISTS `estadogrupoin`;
/*!50001 DROP VIEW IF EXISTS `estadogrupoin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadogrupoin` (
  `Cod_grupo` bigint(20),
  `Num_max_alumnos` int(11),
  `fkcodjornada` bigint(20),
  `fkcodestadogrupo` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadogrupoterminado`
--

DROP TABLE IF EXISTS `estadogrupoterminado`;
/*!50001 DROP VIEW IF EXISTS `estadogrupoterminado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadogrupoterminado` (
  `Cod_grupo` bigint(20),
  `Num_max_alumnos` int(11),
  `fkcodjornada` bigint(20),
  `fkcodestadogrupo` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadohistorialac`
--

DROP TABLE IF EXISTS `estadohistorialac`;
/*!50001 DROP VIEW IF EXISTS `estadohistorialac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadohistorialac` (
  `Cod_historial` bigint(20),
  `Fecha_historial` date,
  `fkcodestado` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadohistorialin`
--

DROP TABLE IF EXISTS `estadohistorialin`;
/*!50001 DROP VIEW IF EXISTS `estadohistorialin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadohistorialin` (
  `Cod_historial` bigint(20),
  `Fecha_historial` date,
  `fkcodestado` bigint(20),
  `fkdocalumno` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadomatricula`
--

DROP TABLE IF EXISTS `estadomatricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadomatricula` (
  `Cod_estado_matricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadomatricula`
--

LOCK TABLES `estadomatricula` WRITE;
/*!40000 ALTER TABLE `estadomatricula` DISABLE KEYS */;
INSERT INTO `estadomatricula` VALUES (1,'En proceso'),(2,'Matriculado'),(3,'Denegado'),(4,'Cancelada');
/*!40000 ALTER TABLE `estadomatricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadomatriculaac`
--

DROP TABLE IF EXISTS `estadomatriculaac`;
/*!50001 DROP VIEW IF EXISTS `estadomatriculaac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadomatriculaac` (
  `Cod_matricula` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20),
  `fkcodestadomatricula` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadomatriculacancelada`
--

DROP TABLE IF EXISTS `estadomatriculacancelada`;
/*!50001 DROP VIEW IF EXISTS `estadomatriculacancelada`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadomatriculacancelada` (
  `Cod_matricula` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20),
  `fkcodestadomatricula` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadomatriculadenegado`
--

DROP TABLE IF EXISTS `estadomatriculadenegado`;
/*!50001 DROP VIEW IF EXISTS `estadomatriculadenegado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadomatriculadenegado` (
  `Cod_matricula` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20),
  `fkcodestadomatricula` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadomatriculaenproceso`
--

DROP TABLE IF EXISTS `estadomatriculaenproceso`;
/*!50001 DROP VIEW IF EXISTS `estadomatriculaenproceso`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadomatriculaenproceso` (
  `Cod_matricula` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20),
  `fkcodestadomatricula` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadomatriculain`
--

DROP TABLE IF EXISTS `estadomatriculain`;
/*!50001 DROP VIEW IF EXISTS `estadomatriculain`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadomatriculain` (
  `Cod_matricula` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20),
  `fkcodestadomatricula` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadomatriculamatriculado`
--

DROP TABLE IF EXISTS `estadomatriculamatriculado`;
/*!50001 DROP VIEW IF EXISTS `estadomatriculamatriculado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadomatriculamatriculado` (
  `Cod_matricula` bigint(20),
  `Descripcion` varchar(50),
  `fkcodestado` bigint(20),
  `fkcodestadomatricula` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadopaisac`
--

DROP TABLE IF EXISTS `estadopaisac`;
/*!50001 DROP VIEW IF EXISTS `estadopaisac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadopaisac` (
  `Cod_pais` bigint(20),
  `Nombre` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadopaisin`
--

DROP TABLE IF EXISTS `estadopaisin`;
/*!50001 DROP VIEW IF EXISTS `estadopaisin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadopaisin` (
  `Cod_pais` bigint(20),
  `Nombre` varchar(50),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estadotitulacion`
--

DROP TABLE IF EXISTS `estadotitulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadotitulacion` (
  `Cod_estado_titulacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_estado_titulacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadotitulacion`
--

LOCK TABLES `estadotitulacion` WRITE;
/*!40000 ALTER TABLE `estadotitulacion` DISABLE KEYS */;
INSERT INTO `estadotitulacion` VALUES (1,'En proceso'),(2,'Certificado'),(3,'Aplazado'),(4,'Cancelado');
/*!40000 ALTER TABLE `estadotitulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `estadotitulacionac`
--

DROP TABLE IF EXISTS `estadotitulacionac`;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionac`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadotitulacionac` (
  `Cod_titulacion` bigint(20),
  `Numero_creditos` int(11),
  `Carga_lectiva` varchar(70),
  `fkcodcampus` bigint(20),
  `fkcodestadotitulacion` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadotitulacionaplazado`
--

DROP TABLE IF EXISTS `estadotitulacionaplazado`;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionaplazado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadotitulacionaplazado` (
  `Cod_titulacion` bigint(20),
  `Numero_creditos` int(11),
  `Carga_lectiva` varchar(70),
  `fkcodcampus` bigint(20),
  `fkcodestadotitulacion` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadotitulacioncancelado`
--

DROP TABLE IF EXISTS `estadotitulacioncancelado`;
/*!50001 DROP VIEW IF EXISTS `estadotitulacioncancelado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadotitulacioncancelado` (
  `Cod_titulacion` bigint(20),
  `Numero_creditos` int(11),
  `Carga_lectiva` varchar(70),
  `fkcodcampus` bigint(20),
  `fkcodestadotitulacion` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadotitulacioncertificado`
--

DROP TABLE IF EXISTS `estadotitulacioncertificado`;
/*!50001 DROP VIEW IF EXISTS `estadotitulacioncertificado`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadotitulacioncertificado` (
  `Cod_titulacion` bigint(20),
  `Numero_creditos` int(11),
  `Carga_lectiva` varchar(70),
  `fkcodcampus` bigint(20),
  `fkcodestadotitulacion` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadotitulacionenproceso`
--

DROP TABLE IF EXISTS `estadotitulacionenproceso`;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionenproceso`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadotitulacionenproceso` (
  `Cod_titulacion` bigint(20),
  `Numero_creditos` int(11),
  `Carga_lectiva` varchar(70),
  `fkcodcampus` bigint(20),
  `fkcodestadotitulacion` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `estadotitulacionin`
--

DROP TABLE IF EXISTS `estadotitulacionin`;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `estadotitulacionin` (
  `Cod_titulacion` bigint(20),
  `Numero_creditos` int(11),
  `Carga_lectiva` varchar(70),
  `fkcodcampus` bigint(20),
  `fkcodestadotitulacion` bigint(20),
  `fkcodestado` bigint(20)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `Cod_grupo` bigint(20) NOT NULL AUTO_INCREMENT,
  `Num_max_alumnos` int(11) DEFAULT NULL,
  `fkcodjornada` bigint(20) DEFAULT NULL,
  `fkcodestadogrupo` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_grupo`),
  KEY `grupojornada` (`fkcodjornada`),
  KEY `grupoestadogrupo` (`fkcodestadogrupo`),
  KEY `grupoestado` (`fkcodestado`),
  CONSTRAINT `grupoestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `grupoestadogrupo` FOREIGN KEY (`fkcodestadogrupo`) REFERENCES `estadogrupo` (`Cod_estado_grupo`),
  CONSTRAINT `grupojornada` FOREIGN KEY (`fkcodjornada`) REFERENCES `jornada` (`Cod_jornada`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
INSERT INTO `grupo` VALUES (1,60,1,1,1),(2,50,2,2,1),(3,20,2,2,1),(4,30,2,2,1),(5,40,2,2,1),(6,35,2,2,1),(7,25,2,2,1),(8,27,2,2,1),(9,27,3,3,2),(10,28,4,3,2),(11,30,4,3,2),(12,15,4,3,2),(13,15,1,4,1),(14,16,1,4,1),(15,17,1,4,1),(16,20,1,4,1),(17,23,1,4,1),(18,70,2,4,1),(19,50,2,4,1),(20,20,3,2,1),(21,20,3,2,1),(22,25,3,2,1),(23,30,3,2,1),(24,40,3,2,1),(25,50,3,2,1),(26,55,3,2,1),(27,45,3,2,1),(28,25,3,2,1),(29,40,3,2,1),(30,40,1,1,1),(31,30,1,2,2),(32,50,1,3,2),(33,50,1,4,2),(34,70,2,1,1),(35,30,2,2,1),(36,20,2,2,2),(37,40,3,1,1),(38,45,3,2,1),(39,48,3,2,2),(40,23,4,1,1),(41,30,4,2,1),(42,41,4,2,2),(43,47,4,3,1),(44,43,4,3,2),(45,23,1,2,1),(46,24,2,2,1),(47,25,2,2,2),(48,26,3,2,1),(49,27,3,2,2),(50,28,4,2,1),(51,29,4,2,2),(52,30,2,2,1),(53,31,2,2,2),(54,32,3,2,1),(55,33,3,2,2),(56,34,4,2,1),(57,35,4,2,2),(58,36,1,3,1),(59,37,1,3,2),(60,38,1,3,2),(61,39,2,3,1),(62,40,2,3,2),(63,41,3,3,1),(64,43,3,3,2),(65,44,4,3,1),(66,45,4,3,2),(67,46,1,4,1),(68,47,1,4,2),(69,48,2,4,1),(70,49,2,4,2),(71,50,3,4,1),(72,51,3,4,2),(73,52,4,4,1),(74,53,4,4,2),(75,54,1,1,1),(76,55,2,2,1),(77,56,3,3,1),(78,57,4,4,1),(79,58,1,1,2),(80,59,2,2,2),(81,60,3,3,2),(82,61,4,4,2),(83,62,1,4,1),(84,25,1,3,1),(85,30,2,1,2),(86,25,4,3,2),(87,30,2,1,1),(88,35,2,1,1),(89,35,4,2,2),(90,40,3,1,1),(91,41,2,2,2),(92,42,1,3,1),(93,43,2,3,2),(94,44,1,3,1),(95,51,2,1,2),(96,52,3,3,1),(97,53,4,2,2),(98,54,2,4,1),(100,55,3,2,2),(101,56,2,1,2),(102,57,3,2,1),(103,40,1,1,1),(104,41,2,2,2),(105,42,3,3,1),(106,43,4,4,2),(107,44,5,1,1),(108,45,1,2,2),(109,46,2,3,1),(110,47,3,4,2),(111,48,4,1,1),(112,49,5,2,2),(113,50,1,3,1),(114,30,2,4,2),(115,31,3,1,1),(116,32,4,2,2),(117,33,5,3,1),(118,34,1,4,2),(119,35,2,1,1),(120,36,3,2,2),(121,37,4,3,1),(122,38,5,4,2),(123,39,1,1,1),(124,40,2,2,2),(125,41,3,3,1),(126,42,4,4,2),(127,43,5,1,1),(128,44,1,2,2),(129,45,2,3,1),(130,46,3,4,2),(131,47,4,1,1),(132,48,5,2,2),(133,49,1,3,1),(134,50,2,4,2),(135,30,3,1,1),(136,31,4,2,2),(137,32,5,3,1),(138,33,1,4,2),(139,34,2,1,1),(140,35,3,2,2),(141,36,4,3,1),(142,37,5,4,2),(143,38,1,1,1),(144,39,2,2,2),(145,40,3,3,1),(146,41,4,4,2),(147,42,5,1,1),(148,43,1,2,2),(149,44,2,3,1),(150,45,3,4,2),(151,46,4,1,1),(152,47,5,2,2),(153,48,1,3,1),(154,49,2,4,2),(155,50,3,1,1),(156,30,4,2,2),(157,31,5,3,1),(158,32,1,4,2),(159,33,2,1,1),(160,34,3,2,2),(161,35,4,3,1),(162,36,5,4,2),(163,37,1,1,1),(164,38,2,2,2),(165,39,3,3,1),(166,40,4,4,2),(167,41,5,1,1),(168,42,1,2,2),(169,43,2,3,1),(170,44,3,4,2),(171,45,4,1,1),(172,46,5,2,2),(173,47,1,3,1),(174,48,2,4,2),(175,49,3,1,1),(176,50,4,2,2),(177,30,5,3,1),(178,31,1,4,2),(179,32,2,1,1),(180,33,3,2,2),(181,34,4,3,1),(182,35,5,4,2),(183,36,1,1,1),(184,37,2,2,2),(185,38,3,3,1),(186,39,4,4,2),(187,40,5,1,1),(188,41,1,2,2),(189,42,2,3,1),(190,43,3,4,2),(191,44,4,1,1),(192,45,5,2,2),(193,46,1,3,1),(194,47,2,4,2),(195,48,3,1,1),(196,49,4,2,2),(197,50,5,3,1),(198,30,1,4,2),(199,31,2,1,1),(200,32,3,2,2),(201,33,4,3,1),(202,34,5,4,2),(203,35,1,1,1),(204,36,2,2,2),(205,37,3,3,1),(206,38,4,4,2),(207,39,5,1,1),(208,40,1,2,2),(209,41,2,3,1),(210,42,3,4,2),(211,43,4,1,1),(212,44,5,2,2),(213,45,1,3,1),(214,46,2,4,2),(215,47,3,1,1),(216,48,4,2,2),(217,49,5,3,1),(218,50,1,4,2),(219,30,2,1,1),(220,31,3,2,2),(221,32,4,3,1),(222,33,5,4,2),(223,34,1,1,1),(224,35,2,2,2),(225,36,3,3,1),(226,37,4,4,2),(227,38,5,1,1),(228,39,1,2,2),(229,40,2,3,1),(230,41,3,4,2),(231,42,4,1,1),(232,43,5,2,2),(233,44,1,3,1),(234,45,2,4,2),(235,46,3,1,1),(236,47,4,2,2),(237,48,5,3,1),(238,49,1,4,2),(239,50,2,1,1),(240,30,3,2,2),(241,31,4,3,1),(242,32,5,4,2),(243,33,1,1,1),(244,34,2,2,2),(245,35,3,3,1),(246,36,4,4,2),(247,37,5,1,1),(248,38,1,2,2),(249,39,2,3,1),(250,40,3,4,2),(251,41,4,1,1),(252,42,5,2,2),(253,43,1,3,1),(254,44,2,4,2),(255,45,3,1,1),(256,46,4,2,2),(257,47,5,3,1),(258,48,1,4,2),(259,49,2,1,1),(260,50,3,2,2),(261,30,4,3,1),(262,31,5,4,2),(263,32,1,1,1),(264,33,2,2,2),(265,34,3,3,1),(266,35,4,4,2),(267,36,5,1,1),(268,37,1,2,2),(269,38,2,3,1),(270,39,3,4,2),(271,40,4,1,1),(272,41,5,2,2),(273,42,1,3,1),(274,43,2,4,2),(275,44,3,1,1),(276,45,4,2,2),(277,46,5,3,1),(278,47,1,4,2),(279,48,2,1,1),(280,49,3,2,2),(281,50,4,3,1),(282,30,5,4,2),(283,31,1,1,1),(284,32,2,2,2),(285,33,3,3,1),(286,34,4,4,2),(287,35,5,1,1),(288,36,1,2,2),(289,37,2,3,1),(290,38,3,4,2),(291,39,4,1,1),(292,40,5,2,2),(293,41,1,3,1),(294,42,2,4,2),(295,43,3,1,1),(296,44,4,2,2),(297,45,5,3,1),(298,46,1,4,2),(299,47,2,1,1),(300,48,3,2,2),(301,49,4,3,1),(302,50,5,4,2);
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial`
--

DROP TABLE IF EXISTS `historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial` (
  `Cod_historial` bigint(20) NOT NULL AUTO_INCREMENT,
  `Fecha_historial` date DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkdocalumno` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_historial`),
  KEY `historialestado` (`fkcodestado`),
  KEY `historialalumno` (`fkdocalumno`),
  CONSTRAINT `historialalumno` FOREIGN KEY (`fkdocalumno`) REFERENCES `alumno` (`Doc_alumno`),
  CONSTRAINT `historialestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=604 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial`
--

LOCK TABLES `historial` WRITE;
/*!40000 ALTER TABLE `historial` DISABLE KEYS */;
INSERT INTO `historial` VALUES (302,'1901-02-02',2,102542),(303,'1902-03-03',1,102543),(304,'1903-04-04',2,102544),(305,'1904-05-05',1,102545),(306,'1905-06-06',2,102546),(307,'1906-07-07',1,102547),(308,'1907-08-08',2,102548),(309,'1908-09-09',1,102549),(310,'1909-10-10',2,1025410),(311,'1910-11-11',1,1025411),(312,'1911-12-12',2,1025412),(313,'1912-01-13',1,1025413),(314,'1913-02-14',2,1025414),(315,'1914-03-15',1,1025415),(316,'1915-04-16',2,1025416),(317,'1916-05-17',1,1025417),(318,'1917-06-18',2,1025418),(319,'1918-07-19',1,1025419),(320,'1919-08-20',2,1025420),(321,'1920-09-21',1,1025421),(322,'1921-10-22',2,1025422),(323,'1922-11-23',1,1025423),(324,'1923-12-24',2,1025424),(325,'1924-01-25',1,1025425),(326,'1925-02-26',2,1025426),(327,'1926-03-27',1,1025427),(328,'1927-04-28',2,1025428),(329,'1928-05-01',1,1025429),(330,'1929-06-02',2,1025430),(331,'1930-07-03',1,1025431),(332,'1931-08-04',2,1025432),(333,'1932-09-05',1,1025433),(334,'1933-10-06',2,1025434),(335,'1934-11-07',1,1025435),(336,'1935-12-08',2,1025436),(337,'1936-01-09',1,1025437),(338,'1937-02-10',2,1025438),(339,'1938-03-11',1,1025439),(340,'1939-04-12',2,1025440),(341,'1940-05-13',1,1025441),(342,'1941-06-14',2,1025442),(343,'1942-07-15',1,1025443),(344,'1943-08-16',2,1025444),(345,'1944-09-17',1,1025445),(346,'1945-10-18',2,1025446),(347,'1946-11-19',1,1025447),(348,'1947-12-20',2,1025448),(349,'1948-01-21',1,1025449),(350,'1949-02-22',2,1025450),(351,'1950-03-23',1,1025451),(352,'1951-04-24',2,1025452),(353,'1952-05-25',1,1025453),(354,'1953-06-26',2,1025454),(355,'1954-07-27',1,1025455),(356,'1955-08-28',2,1025456),(357,'1956-09-01',1,1025457),(358,'1957-10-02',2,1025458),(359,'1958-11-03',1,1025459),(360,'1959-12-04',2,1025460),(361,'1960-01-05',1,1025461),(362,'1961-02-06',2,1025462),(363,'1962-03-07',1,1025463),(364,'1963-04-08',2,1025464),(365,'1964-05-09',1,1025465),(366,'1965-06-10',2,1025466),(367,'1966-07-11',1,1025467),(368,'1967-08-12',2,1025468),(369,'1968-09-13',1,1025469),(370,'1969-10-14',2,1025470),(371,'1970-11-15',1,1025471),(372,'1971-12-16',2,1025472),(373,'1972-01-17',1,1025473),(374,'1973-02-18',2,1025474),(375,'1974-03-19',1,1025475),(376,'1975-04-20',2,1025476),(377,'1976-05-21',1,1025477),(378,'1977-06-22',2,1025478),(379,'1978-07-23',1,1025479),(380,'1979-08-24',2,1025480),(381,'1980-09-25',1,1025481),(382,'1981-10-26',2,1025482),(383,'1982-11-27',1,1025483),(384,'1983-12-28',2,1025484),(385,'1984-01-01',1,1025485),(386,'1985-02-02',2,1025486),(387,'1986-03-03',1,1025487),(388,'1987-04-04',2,1025488),(389,'1988-05-05',1,1025489),(390,'1989-06-06',2,1025490),(391,'1990-07-07',1,1025491),(392,'1991-08-08',2,1025492),(393,'1992-09-09',1,1025493),(394,'1993-10-10',2,1025494),(395,'1994-11-11',1,1025495),(396,'1995-12-12',2,1025496),(397,'1996-01-13',1,1025497),(398,'1997-02-14',2,1025498),(399,'1998-03-15',1,1025499),(400,'1999-04-16',2,10254100),(401,'2000-05-17',1,10254101),(402,'2001-06-18',2,10254102),(403,'2002-07-19',1,10254103),(404,'2003-08-20',2,10254104),(405,'2004-09-21',1,10254105),(406,'2005-10-22',2,10254106),(407,'2006-11-23',1,10254107),(408,'2007-12-24',2,10254108),(409,'2008-01-25',1,10254109),(410,'2009-02-26',2,10254110),(411,'2010-03-27',1,10254111),(412,'2011-04-28',2,10254112),(413,'2012-05-01',1,10254113),(414,'2013-06-02',2,10254114),(415,'2014-07-03',1,10254115),(416,'2015-08-04',2,10254116),(417,'2016-09-05',1,10254117),(418,'1999-10-06',2,10254118),(419,'2000-11-07',1,10254119),(420,'2001-12-08',2,10254120),(421,'2002-01-09',1,10254121),(422,'2003-02-10',2,10254122),(423,'2004-03-11',1,10254123),(424,'2005-04-12',2,10254124),(425,'2006-05-13',1,10254125),(426,'2007-06-14',2,10254126),(427,'2008-07-15',1,10254127),(428,'2009-08-16',2,10254128),(429,'2010-09-17',1,10254129),(430,'2011-10-18',2,10254130),(431,'2012-11-19',1,10254131),(432,'2013-12-20',2,10254132),(433,'2014-01-21',1,10254133),(434,'2015-02-22',2,10254134),(435,'2016-03-23',1,10254135),(436,'1999-04-24',2,10254136),(437,'2000-05-25',1,10254137),(438,'2001-06-26',2,10254138),(439,'2002-07-27',1,10254139),(440,'2003-08-28',2,10254140),(441,'2004-09-01',1,10254141),(442,'2005-10-02',2,10254142),(443,'2006-11-03',1,10254143),(444,'2007-12-04',2,10254144),(445,'2008-01-05',1,10254145),(446,'2009-02-06',2,10254146),(447,'2010-03-07',1,10254147),(448,'2011-04-08',2,10254148),(449,'2012-05-09',1,10254149),(450,'2013-06-10',2,10254150),(451,'2014-07-11',1,10254151),(452,'2015-08-12',2,10254152),(453,'2016-09-13',1,10254153),(454,'1999-10-14',2,10254154),(455,'2000-11-15',1,10254155),(456,'2001-12-16',2,10254156),(457,'2002-01-17',1,10254157),(458,'2003-02-18',2,10254158),(459,'2004-03-19',1,10254159),(460,'2005-04-20',2,10254160),(461,'2006-05-21',1,10254161),(462,'2007-06-22',2,10254162),(463,'2008-07-23',1,10254163),(464,'2009-08-24',2,10254164),(465,'2010-09-25',1,10254165),(466,'2011-10-26',2,10254166),(467,'2012-11-27',1,10254167),(468,'2013-12-28',2,10254168),(469,'2014-01-01',1,10254169),(470,'2015-02-02',2,10254170),(471,'2016-03-03',1,10254171),(472,'1999-04-04',2,10254172),(473,'2000-05-05',1,10254173),(474,'2001-06-06',2,10254174),(475,'2002-07-07',1,10254175),(476,'2003-08-08',2,10254176),(477,'2004-09-09',1,10254177),(478,'2005-10-10',2,10254178),(479,'2006-11-11',1,10254179),(480,'2007-12-12',2,10254180),(481,'2008-01-13',1,10254181),(482,'2009-02-14',2,10254182),(483,'2010-03-15',1,10254183),(484,'2011-04-16',2,10254184),(485,'2012-05-17',1,10254185),(486,'2013-06-18',2,10254186),(487,'2014-07-19',1,10254187),(488,'2015-08-20',2,10254188),(489,'2016-09-21',1,10254189),(490,'1999-10-22',2,10254190),(491,'2000-11-23',1,10254191),(492,'2001-12-24',2,10254192),(493,'2002-01-25',1,10254193),(494,'2003-02-26',2,10254194),(495,'2004-03-27',1,10254195),(496,'2005-04-28',2,10254196),(497,'2006-05-01',1,10254197),(498,'2007-06-02',2,10254198),(499,'2008-07-03',1,10254199),(500,'2009-08-04',2,10254200),(501,'2010-09-05',1,10254201),(502,'2011-10-06',2,10254202),(503,'2012-11-07',1,10254203),(504,'2013-12-08',2,10254204),(505,'2014-01-09',1,10254205),(506,'2015-02-10',2,10254206),(507,'2016-03-11',1,10254207),(508,'1999-04-12',2,10254208),(509,'2000-05-13',1,10254209),(510,'2001-06-14',2,10254210),(511,'2002-07-15',1,10254211),(512,'2003-08-16',2,10254212),(513,'2004-09-17',1,10254213),(514,'2005-10-18',2,10254214),(515,'2006-11-19',1,10254215),(516,'2007-12-20',2,10254216),(517,'2008-01-21',1,10254217),(518,'2009-02-22',2,10254218),(519,'2010-03-23',1,10254219),(520,'2011-04-24',2,10254220),(521,'2012-05-25',1,10254221),(522,'2013-06-26',2,10254222),(523,'2014-07-27',1,10254223),(524,'2015-08-28',2,10254224),(525,'2016-09-01',1,10254225),(526,'1999-10-02',2,10254226),(527,'2000-11-03',1,10254227),(528,'2001-12-04',2,10254228),(529,'2002-01-05',1,10254229),(530,'2003-02-06',2,10254230),(531,'2004-03-07',1,10254231),(532,'2005-04-08',2,10254232),(533,'2006-05-09',1,10254233),(534,'2007-06-10',2,10254234),(535,'2008-07-11',1,10254235),(536,'2009-08-12',2,10254236),(537,'2010-09-13',1,10254237),(538,'2011-10-14',2,10254238),(539,'2012-11-15',1,10254239),(540,'2013-12-16',2,10254240),(541,'2014-01-17',1,10254241),(542,'2015-02-18',2,10254242),(543,'2016-03-19',1,10254243),(544,'1999-04-20',2,10254244),(545,'2000-05-21',1,10254245),(546,'2001-06-22',2,10254246),(547,'2002-07-23',1,10254247),(548,'2003-08-24',2,10254248),(549,'2004-09-25',1,10254249),(550,'2005-10-26',2,10254250),(551,'2006-11-27',1,10254251),(552,'2007-12-28',2,10254252),(553,'2008-01-01',1,10254253),(554,'2009-02-02',2,10254254),(555,'2010-03-03',1,10254255),(556,'2011-04-04',2,10254256),(557,'2012-05-05',1,10254257),(558,'2013-06-06',2,10254258),(561,'2016-09-09',1,10254261),(562,'1999-10-10',2,10254262),(563,'2000-11-11',1,10254263),(564,'2001-12-12',2,10254264),(565,'2002-01-13',1,10254265),(566,'2003-02-14',2,10254266),(567,'2004-03-15',1,10254267),(568,'2005-04-16',2,10254268),(569,'2006-05-17',1,10254269),(570,'2007-06-18',2,10254270),(571,'2008-07-19',1,10254271),(572,'2009-08-20',2,10254272),(573,'2010-09-21',1,10254273),(574,'2011-10-22',2,10254274),(575,'2012-11-23',1,10254275),(576,'2013-12-24',2,10254276),(577,'2014-01-25',1,10254277),(578,'2015-02-26',2,10254278),(579,'2016-03-27',1,10254279),(580,'1999-04-28',2,10254280),(581,'2000-05-01',1,10254281),(582,'2001-06-02',2,10254282),(583,'2002-07-03',1,10254283),(584,'2003-08-04',2,10254284),(585,'2004-09-05',1,10254285),(586,'2005-10-06',2,10254286),(587,'2006-11-07',1,10254287),(588,'2007-12-08',2,10254288),(589,'2008-01-09',1,10254289),(590,'2009-02-10',2,10254290),(591,'2010-03-11',1,10254291),(592,'2011-04-12',2,10254292),(593,'2012-05-13',1,10254293),(594,'2013-06-14',2,10254294),(595,'2014-07-15',1,10254295),(596,'2015-08-16',2,10254296),(597,'2016-09-17',1,10254297),(598,'1999-10-18',2,10254298),(599,'2000-11-19',1,10254299),(600,'2001-12-20',2,10254300),(601,'1906-07-07',1,102547),(602,'1907-08-08',2,102548),(603,'1908-09-09',1,102549);
/*!40000 ALTER TABLE `historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jornada`
--

DROP TABLE IF EXISTS `jornada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jornada` (
  `Cod_jornada` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  `fkcodtipojornada` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_jornada`),
  KEY `jornadatipojornada` (`fkcodtipojornada`),
  CONSTRAINT `jornadatipojornada` FOREIGN KEY (`fkcodtipojornada`) REFERENCES `tipojornada` (`Cod_tipo_jornada`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jornada`
--

LOCK TABLES `jornada` WRITE;
/*!40000 ALTER TABLE `jornada` DISABLE KEYS */;
INSERT INTO `jornada` VALUES (1,'Horario de 6:00 a 12:00',1),(2,'Horario de 8:00 a 3:00',1),(3,'Horario de 6:00P a 10:00P',2),(4,'Horario de 3:00P a 8:00P',3),(5,'Horario de 12:00P a 6:00P',1);
/*!40000 ALTER TABLE `jornada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `Cod_matricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  `fkcodestadomatricula` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_matricula`),
  KEY `matriculaestado` (`fkcodestado`),
  KEY `matriculaestadomatricula` (`fkcodestadomatricula`),
  CONSTRAINT `matriculaestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `matriculaestadomatricula` FOREIGN KEY (`fkcodestadomatricula`) REFERENCES `estadomatricula` (`Cod_estado_matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matricula`
--

LOCK TABLES `matricula` WRITE;
/*!40000 ALTER TABLE `matricula` DISABLE KEYS */;
INSERT INTO `matricula` VALUES (1,'Matricula para el curso HistoriadelaLengua',1,1),(2,'Matricula para el curso Francesa',2,2),(3,'Matricula para el curso Historiadelaliteraturafran',1,3),(4,'Matricula para el curso Historiadelaliteraturafran',2,4),(5,'Matricula para el curso Historiadelaliteraturafran',1,1),(6,'Matricula para el curso Lexicograf?aySem?nticaFran',2,2),(7,'Matricula para el curso LiteraturaAlemana:L?rica',1,3),(8,'Matricula para el curso LiteraturaFrancesayPensami',2,4),(9,'Matricula para el curso Teor?asCr?ticasyAn?lisisde',1,1),(10,'Matricula para el curso Alem?n',2,2),(11,'Matricula para el curso Alemania,AustriaySuiza:ide',1,3),(12,'Matricula para el curso Alemania,AustriaySuiza:ide',2,4),(13,'Matricula para el curso An?lisisdeTextos',1,1),(14,'Matricula para el curso ItalianosLiterarios',2,2),(15,'Matricula para el curso Contemp.',1,3),(16,'Matricula para el curso An?lisisdelDiscursoenLengu',2,4),(17,'Matricula para el curso An?lisisdelDiscursoLiterar',1,1),(18,'Matricula para el curso An?lisisdeldiscursoydiscur',2,2),(19,'Matricula para el curso An?lisisdeldiscursoylengua',1,3),(20,'Matricula para el curso An?lisiseinterpretaci?ndet',2,4),(21,'Matricula para el curso An?lisiseinterpretaci?ndet',1,1),(22,'Matricula para el curso An?lisiseinterpretaci?ndet',2,2),(23,'Matricula para el curso An?lisisFilol?icoDeTextosR',1,3),(24,'Matricula para el curso An?lisisLing??sticaDiacr?n',2,4),(25,'Matricula para el curso An?lisisling??sticodetexto',1,1),(26,'Matricula para el curso ?rabeHablado',2,2),(27,'Matricula para el curso ?rabeablado',1,3),(28,'Matricula para el curso ?rabeI',2,4),(29,'Matricula para el curso ?rabeII',1,1),(30,'Matricula para el curso ?rabeIII',2,2),(31,'Matricula para el curso ?rabeIV',1,3),(32,'Matricula para el curso ?rabeMarroqu?',2,4),(33,'Matricula para el curso ?rabeMarroqu?',1,1),(34,'Matricula para el curso ?rabeV',2,2),(35,'Matricula para el curso ?rabeVI',1,3),(36,'Matricula para el curso ?rabeVII',2,4),(37,'Matricula para el curso ?rabeVIII',1,1),(38,'Matricula para el curso Arameo',2,2),(39,'Matricula para el curso Arameo(Sir?aco)Cl?sicoOrie',1,3),(40,'Matricula para el curso ArameoModerno',2,4),(41,'Matricula para el curso ArameoModernoI',1,1),(42,'Matricula para el curso ArameoModernoII',2,2),(43,'Matricula para el curso Arqueolog?aCl?sica',1,3),(44,'Matricula para el curso ArteIsl?micoeHispanomusulm',2,4),(45,'Matricula para el curso Catal?nI',1,1),(46,'Matricula para el curso Catal?nII',2,2),(47,'Matricula para el curso Cl?sicosdelaliteraturabras',1,3),(48,'Matricula para el curso Cl?sicosdelaliteraturaport',2,4),(49,'Matricula para el curso Cl?sicosdeliteraturaportug',1,1),(50,'Matricula para el curso ComentariodeTextosAlemanes',2,2),(51,'Matricula para el curso ComentariodeTextosdelasLit',1,3),(52,'Matricula para el curso ComentariodeTextosHist?ric',2,4),(53,'Matricula para el curso ComentariodeTextosItaliano',1,1),(54,'Matricula para el curso ComentarioFilol?gicodeText',2,2),(55,'Matricula para el curso ComentarioFilol?gicodeText',1,3),(56,'Matricula para el curso ComentarioFilol?gicodeText',2,4),(57,'Matricula para el curso ComentarioLing??stico',1,1),(58,'Matricula para el curso ComentarioLiterariodeTexto',2,2),(59,'Matricula para el curso ComentarioLiterariodeTexto',1,3),(60,'Matricula para el curso ComentariosdeTextosPo?tico',2,4),(61,'Matricula para el curso HistoriadelaLengua',1,1),(62,'Matricula para el curso Francesa',2,2),(63,'Matricula para el curso Historiadelaliteraturafran',1,3),(64,'Matricula para el curso Historiadelaliteraturafran',2,4),(65,'Matricula para el curso Historiadelaliteraturafran',1,1),(66,'Matricula para el curso Lexicograf?aySem?nticaFran',2,2),(67,'Matricula para el curso LiteraturaAlemana:L?rica',1,3),(68,'Matricula para el curso LiteraturaFrancesayPensami',2,4),(69,'Matricula para el curso Teor?asCr?ticasyAn?lisisde',1,1),(70,'Matricula para el curso Alem?n',2,2),(71,'Matricula para el curso Alemania,AustriaySuiza:ide',1,3),(72,'Matricula para el curso Alemania,AustriaySuiza:ide',2,4),(73,'Matricula para el curso An?lisisdeTextos',1,1),(74,'Matricula para el curso ItalianosLiterarios',2,2),(75,'Matricula para el curso Contemp.',1,3),(76,'Matricula para el curso An?lisisdelDiscursoenLengu',2,4),(77,'Matricula para el curso An?lisisdelDiscursoLiterar',1,1),(78,'Matricula para el curso An?lisisdeldiscursoydiscur',2,2),(79,'Matricula para el curso An?lisisdeldiscursoylengua',1,3),(80,'Matricula para el curso An?lisiseinterpretaci?ndet',2,4),(81,'Matricula para el curso An?lisiseinterpretaci?ndet',1,1),(82,'Matricula para el curso An?lisiseinterpretaci?ndet',2,2),(83,'Matricula para el curso An?lisisFilol?icoDeTextosR',1,3),(84,'Matricula para el curso An?lisisLing??sticaDiacr?n',2,4),(85,'Matricula para el curso An?lisisling??sticodetexto',1,1),(86,'Matricula para el curso ?rabeHablado',2,2),(87,'Matricula para el curso ?rabeablado',1,3),(88,'Matricula para el curso ?rabeI',2,4),(89,'Matricula para el curso ?rabeII',1,1),(90,'Matricula para el curso ?rabeIII',2,2),(91,'Matricula para el curso ?rabeIV',1,3),(92,'Matricula para el curso ?rabeMarroqu?',2,4),(93,'Matricula para el curso ?rabeMarroqu?',1,1),(94,'Matricula para el curso ?rabeV',2,2),(95,'Matricula para el curso ?rabeVI',1,3),(96,'Matricula para el curso ?rabeVII',2,4),(97,'Matricula para el curso ?rabeVIII',1,1),(98,'Matricula para el curso Arameo',2,2),(99,'Matricula para el curso Arameo(Sir?aco)Cl?sicoOrie',1,3),(100,'Matricula para el curso ArameoModerno',2,4),(101,'Matricula para el curso ArameoModernoI',1,1),(102,'Matricula para el curso ArameoModernoII',2,2),(103,'Matricula para el curso Arqueolog?aCl?sica',1,3),(104,'Matricula para el curso ArteIsl?micoeHispanomusulm',2,4),(105,'Matricula para el curso Catal?nI',1,1),(106,'Matricula para el curso Catal?nII',2,2),(107,'Matricula para el curso Cl?sicosdelaliteraturabras',1,3),(108,'Matricula para el curso Cl?sicosdelaliteraturaport',2,4),(109,'Matricula para el curso Cl?sicosdeliteraturaportug',1,1),(110,'Matricula para el curso ComentariodeTextosAlemanes',2,2),(111,'Matricula para el curso ComentariodeTextosdelasLit',1,3),(112,'Matricula para el curso ComentariodeTextosHist?ric',2,4),(113,'Matricula para el curso ComentariodeTextosItaliano',1,1),(114,'Matricula para el curso ComentarioFilol?gicodeText',2,2),(115,'Matricula para el curso ComentarioFilol?gicodeText',1,3),(116,'Matricula para el curso ComentarioFilol?gicodeText',2,4),(117,'Matricula para el curso ComentarioLing??stico',1,1),(118,'Matricula para el curso ComentarioLiterariodeTexto',2,2),(119,'Matricula para el curso ComentarioLiterariodeTexto',1,3),(120,'Matricula para el curso ComentariosdeTextosPo?tico',2,4),(121,'Matricula para el curso HistoriadelaLengua',1,1),(122,'Matricula para el curso Francesa',2,2),(123,'Matricula para el curso Historiadelaliteraturafran',1,3),(124,'Matricula para el curso Historiadelaliteraturafran',2,4),(125,'Matricula para el curso Historiadelaliteraturafran',1,1),(126,'Matricula para el curso Lexicograf?aySem?nticaFran',2,2),(127,'Matricula para el curso LiteraturaAlemana:L?rica',1,3),(128,'Matricula para el curso LiteraturaFrancesayPensami',2,4),(129,'Matricula para el curso Teor?asCr?ticasyAn?lisisde',1,1),(130,'Matricula para el curso Alem?n',2,2),(131,'Matricula para el curso Alemania,AustriaySuiza:ide',1,3),(132,'Matricula para el curso Alemania,AustriaySuiza:ide',2,4),(133,'Matricula para el curso An?lisisdeTextos',1,1),(134,'Matricula para el curso ItalianosLiterarios',2,2),(135,'Matricula para el curso Contemp.',1,3),(136,'Matricula para el curso An?lisisdelDiscursoenLengu',2,4),(137,'Matricula para el curso An?lisisdelDiscursoLiterar',1,1),(138,'Matricula para el curso An?lisisdeldiscursoydiscur',2,2),(139,'Matricula para el curso An?lisisdeldiscursoylengua',1,3),(140,'Matricula para el curso An?lisiseinterpretaci?ndet',2,4),(141,'Matricula para el curso An?lisiseinterpretaci?ndet',1,1),(142,'Matricula para el curso An?lisiseinterpretaci?ndet',2,2),(143,'Matricula para el curso An?lisisFilol?icoDeTextosR',1,3),(144,'Matricula para el curso An?lisisLing??sticaDiacr?n',2,4),(145,'Matricula para el curso An?lisisling??sticodetexto',1,1),(146,'Matricula para el curso ?rabeHablado',2,2),(147,'Matricula para el curso ?rabeablado',1,3),(148,'Matricula para el curso ?rabeI',2,4),(149,'Matricula para el curso ?rabeII',1,1),(150,'Matricula para el curso ?rabeIII',2,2),(151,'Matricula para el curso ?rabeIV',1,3),(152,'Matricula para el curso ?rabeMarroqu?',2,4),(153,'Matricula para el curso ?rabeMarroqu?',1,1),(154,'Matricula para el curso ?rabeV',2,2),(155,'Matricula para el curso ?rabeVI',1,3),(156,'Matricula para el curso ?rabeVII',2,4),(157,'Matricula para el curso ?rabeVIII',1,1),(158,'Matricula para el curso Arameo',2,2),(159,'Matricula para el curso Arameo(Sir?aco)Cl?sicoOrie',1,3),(160,'Matricula para el curso ArameoModerno',2,4),(161,'Matricula para el curso ArameoModernoI',1,1),(162,'Matricula para el curso ArameoModernoII',2,2),(163,'Matricula para el curso Arqueolog?aCl?sica',1,3),(164,'Matricula para el curso ArteIsl?micoeHispanomusulm',2,4),(165,'Matricula para el curso Catal?nI',1,1),(166,'Matricula para el curso Catal?nII',2,2),(167,'Matricula para el curso Cl?sicosdelaliteraturabras',1,3),(168,'Matricula para el curso Cl?sicosdelaliteraturaport',2,4),(169,'Matricula para el curso Cl?sicosdeliteraturaportug',1,1),(170,'Matricula para el curso ComentariodeTextosAlemanes',2,2),(171,'Matricula para el curso ComentariodeTextosdelasLit',1,3),(172,'Matricula para el curso ComentariodeTextosHist?ric',2,4),(173,'Matricula para el curso ComentariodeTextosItaliano',1,1),(174,'Matricula para el curso ComentarioFilol?gicodeText',2,2),(175,'Matricula para el curso ComentarioFilol?gicodeText',1,3),(176,'Matricula para el curso ComentarioFilol?gicodeText',2,4),(177,'Matricula para el curso ComentarioLing??stico',1,1),(178,'Matricula para el curso ComentarioLiterariodeTexto',2,2),(179,'Matricula para el curso ComentarioLiterariodeTexto',1,3),(180,'Matricula para el curso ComentariosdeTextosPo?tico',2,4),(181,'Matricula para el curso HistoriadelaLengua',1,1),(182,'Matricula para el curso Francesa',2,2),(183,'Matricula para el curso Historiadelaliteraturafran',1,3),(184,'Matricula para el curso Historiadelaliteraturafran',2,4),(185,'Matricula para el curso Historiadelaliteraturafran',1,1),(186,'Matricula para el curso Lexicograf?aySem?nticaFran',2,2),(187,'Matricula para el curso LiteraturaAlemana:L?rica',1,3),(188,'Matricula para el curso LiteraturaFrancesayPensami',2,4),(189,'Matricula para el curso Teor?asCr?ticasyAn?lisisde',1,1),(190,'Matricula para el curso Alem?n',2,2),(191,'Matricula para el curso Alemania,AustriaySuiza:ide',1,3),(192,'Matricula para el curso Alemania,AustriaySuiza:ide',2,4),(193,'Matricula para el curso An?lisisdeTextos',1,1),(194,'Matricula para el curso ItalianosLiterarios',2,2),(195,'Matricula para el curso Contemp.',1,3),(196,'Matricula para el curso An?lisisdelDiscursoenLengu',2,4),(197,'Matricula para el curso An?lisisdelDiscursoLiterar',1,1),(198,'Matricula para el curso An?lisisdeldiscursoydiscur',2,2),(199,'Matricula para el curso An?lisisdeldiscursoylengua',1,3),(200,'Matricula para el curso An?lisiseinterpretaci?ndet',2,4),(201,'Matricula para el curso An?lisiseinterpretaci?ndet',1,1),(202,'Matricula para el curso An?lisiseinterpretaci?ndet',2,2),(203,'Matricula para el curso An?lisisFilol?icoDeTextosR',1,3),(204,'Matricula para el curso An?lisisLing??sticaDiacr?n',2,4),(205,'Matricula para el curso An?lisisling??sticodetexto',1,1),(206,'Matricula para el curso ?rabeHablado',2,2),(207,'Matricula para el curso ?rabeablado',1,3),(208,'Matricula para el curso ?rabeI',2,4),(209,'Matricula para el curso ?rabeII',1,1),(210,'Matricula para el curso ?rabeIII',2,2),(211,'Matricula para el curso ?rabeIV',1,3),(212,'Matricula para el curso ?rabeMarroqu?',2,4),(213,'Matricula para el curso ?rabeMarroqu?',1,1),(214,'Matricula para el curso ?rabeV',2,2),(215,'Matricula para el curso ?rabeVI',1,3),(216,'Matricula para el curso ?rabeVII',2,4),(217,'Matricula para el curso ?rabeVIII',1,1),(218,'Matricula para el curso Arameo',2,2),(219,'Matricula para el curso Arameo(Sir?aco)Cl?sicoOrie',1,3),(220,'Matricula para el curso ArameoModerno',2,4),(221,'Matricula para el curso ArameoModernoI',1,1),(222,'Matricula para el curso ArameoModernoII',2,2),(223,'Matricula para el curso Arqueolog?aCl?sica',1,3),(224,'Matricula para el curso ArteIsl?micoeHispanomusulm',2,4),(225,'Matricula para el curso Catal?nI',1,1),(226,'Matricula para el curso Catal?nII',2,2),(227,'Matricula para el curso Cl?sicosdelaliteraturabras',1,3),(228,'Matricula para el curso Cl?sicosdelaliteraturaport',2,4),(229,'Matricula para el curso Cl?sicosdeliteraturaportug',1,1),(230,'Matricula para el curso ComentariodeTextosAlemanes',2,2),(231,'Matricula para el curso ComentariodeTextosdelasLit',1,3),(232,'Matricula para el curso ComentariodeTextosHist?ric',2,4),(233,'Matricula para el curso ComentariodeTextosItaliano',1,1),(234,'Matricula para el curso ComentarioFilol?gicodeText',2,2),(235,'Matricula para el curso ComentarioFilol?gicodeText',1,3),(236,'Matricula para el curso ComentarioFilol?gicodeText',2,4),(237,'Matricula para el curso ComentarioLing??stico',1,1),(238,'Matricula para el curso ComentarioLiterariodeTexto',2,2),(239,'Matricula para el curso ComentarioLiterariodeTexto',1,3),(240,'Matricula para el curso ComentariosdeTextosPo?tico',2,4),(241,'Matricula para el curso HistoriadelaLengua',1,1),(242,'Matricula para el curso Francesa',2,2),(243,'Matricula para el curso Historiadelaliteraturafran',1,3),(244,'Matricula para el curso Historiadelaliteraturafran',2,4),(245,'Matricula para el curso Historiadelaliteraturafran',1,1),(246,'Matricula para el curso Lexicograf?aySem?nticaFran',2,2),(247,'Matricula para el curso LiteraturaAlemana:L?rica',1,3),(248,'Matricula para el curso LiteraturaFrancesayPensami',2,4),(249,'Matricula para el curso Teor?asCr?ticasyAn?lisisde',1,1),(250,'Matricula para el curso Alem?n',2,2),(251,'Matricula para el curso Alemania,AustriaySuiza:ide',1,3),(252,'Matricula para el curso Alemania,AustriaySuiza:ide',2,4),(253,'Matricula para el curso An?lisisdeTextos',1,1),(254,'Matricula para el curso ItalianosLiterarios',2,2),(255,'Matricula para el curso Contemp.',1,3),(256,'Matricula para el curso An?lisisdelDiscursoenLengu',2,4),(257,'Matricula para el curso An?lisisdelDiscursoLiterar',1,1),(258,'Matricula para el curso An?lisisdeldiscursoydiscur',2,2),(259,'Matricula para el curso An?lisisdeldiscursoylengua',1,3),(260,'Matricula para el curso An?lisiseinterpretaci?ndet',2,4),(261,'Matricula para el curso An?lisiseinterpretaci?ndet',1,1),(262,'Matricula para el curso An?lisiseinterpretaci?ndet',2,2),(263,'Matricula para el curso An?lisisFilol?icoDeTextosR',1,3),(264,'Matricula para el curso An?lisisLing??sticaDiacr?n',2,4),(265,'Matricula para el curso An?lisisling??sticodetexto',1,1),(266,'Matricula para el curso ?rabeHablado',2,2),(267,'Matricula para el curso ?rabeablado',1,3),(268,'Matricula para el curso ?rabeI',2,4),(269,'Matricula para el curso ?rabeII',1,1),(270,'Matricula para el curso ?rabeIII',2,2),(271,'Matricula para el curso ?rabeIV',1,3),(272,'Matricula para el curso ?rabeMarroqu?',2,4),(273,'Matricula para el curso ?rabeMarroqu?',1,1),(274,'Matricula para el curso ?rabeV',2,2),(275,'Matricula para el curso ?rabeVI',1,3),(276,'Matricula para el curso ?rabeVII',2,4),(277,'Matricula para el curso ?rabeVIII',1,1),(278,'Matricula para el curso Arameo',2,2),(279,'Matricula para el curso Arameo(Sir?aco)Cl?sicoOrie',1,3),(280,'Matricula para el curso ArameoModerno',2,4),(281,'Matricula para el curso ArameoModernoI',1,1),(282,'Matricula para el curso ArameoModernoII',2,2),(283,'Matricula para el curso Arqueolog?aCl?sica',1,3),(284,'Matricula para el curso ArteIsl?micoeHispanomusulm',2,4),(285,'Matricula para el curso Catal?nI',1,1),(286,'Matricula para el curso Catal?nII',2,2),(287,'Matricula para el curso Cl?sicosdelaliteraturabras',1,3),(288,'Matricula para el curso Cl?sicosdelaliteraturaport',2,4),(289,'Matricula para el curso Cl?sicosdeliteraturaportug',1,1),(290,'Matricula para el curso ComentariodeTextosAlemanes',2,2),(291,'Matricula para el curso ComentariodeTextosdelasLit',1,3),(292,'Matricula para el curso ComentariodeTextosHist?ric',2,4),(293,'Matricula para el curso ComentariodeTextosItaliano',1,1),(294,'Matricula para el curso ComentarioFilol?gicodeText',2,2),(295,'Matricula para el curso ComentarioFilol?gicodeText',1,3),(296,'Matricula para el curso ComentarioFilol?gicodeText',2,4),(297,'Matricula para el curso ComentarioLing??stico',1,1),(298,'Matricula para el curso ComentarioLiterariodeTexto',2,2),(299,'Matricula para el curso ComentarioLiterariodeTexto',1,3),(300,'Matricula para el curso ComentariosdeTextosPo?tico',2,4);
/*!40000 ALTER TABLE `matricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `Cod_pais` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_pais`),
  KEY `paisestado` (`fkcodestado`),
  CONSTRAINT `paisestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Afganistan',1),(2,'Albania',1),(3,'Alemania',1),(4,'Andorra',1),(5,'Aangola',1),(6,'Arabia saudi',1),(7,'Argelia',1),(8,'Argentina',1),(9,'Aarmenia',1),(10,'Australia',1),(11,'Austria',1),(12,'Azerbaiyan',1),(13,'Bahamas',1),(14,'Banglades',1),(15,'Barbados',1),(16,'Barein',1),(17,'Belgica',1),(18,'Belice',1),(19,'Benin',1),(20,'Bielorrusia',1),(21,'Birmania',1),(22,'Bolivia',1),(23,'Bosnia',1),(24,'Boutsuana',1),(25,'Brasil',1),(26,'Brunei',1),(27,'Bulgaria',1),(28,'Burkina faso',1),(29,'Burundi',1),(30,'Cabo verde',1),(31,'Camboya',1),(32,'Camerun',1),(33,'Canada',1),(34,'Catar',1),(35,'Chad',1),(36,'Chile',1),(37,'China',1),(38,'Chipre',1),(39,'Ciudad del vaticano',1),(40,'Colombia',1),(41,'Comoras',1),(42,'Corea del norte',1),(43,'Corea del sur',1),(44,'Costa de marfil',1),(45,'Costa rica',1),(46,'Croacia',1),(47,'Cuba',1),(48,'Dinamarca',1),(49,'Dominica',1),(50,'Ecuador',1),(51,'Egipto',1),(52,'El salvador',1),(53,'Emiratos arabes unidos',1),(54,'Eritrea',1),(55,'Eslovaquia',1),(56,'Eslovenia',1),(57,'España',1),(58,'Estados unidos',1),(59,'Estonia',1),(60,'Etiopia',1),(61,'Filipinas',1),(62,'Finlandia',1),(63,'Fiyi',1),(64,'Francia',1),(65,'Gabon',1),(66,'Gambia',1),(67,'Georgia',1),(68,'Ghana',1),(69,'Granada',1),(70,'Grecia',1),(71,'Guatemala',1),(72,'Guyana',1),(73,'Guinea',1),(74,'Haiti',1),(75,'Honduras',1),(76,'Hungria',1),(77,'India',1),(78,'Indonesia',1),(79,'Irak',1),(80,'Irlanda',1),(81,'Islandia',1),(82,'Islas marshall',1),(83,'Islas salomon',1),(84,'Israel',1),(85,'Italia',1),(86,'Jamaica',1),(87,'Japon',1),(88,'Jordania',1),(89,'Kazajistan',1),(90,'Kenia',1),(91,'Kiribati',1),(92,'Kuwait',1),(93,'Laos',1),(94,'Lesoto',1),(95,'Letonia',1),(96,'Libano',1),(97,'Liberia',1),(98,'Liechtenstein',1),(99,'Lituania',1),(100,'Luxemburgo',1),(101,'Madagascar',1),(102,'Malasia',1),(103,'Malaui',1),(104,'Maldivas',1),(105,'Mali',1),(106,'Malta',1),(107,'Marruecos',1),(108,'Mauricio',1),(109,'Mauritania',1),(110,'Mexico',1),(111,'CaboVerde',1),(112,'Camboya',2),(113,'Camer?n',1),(114,'Canad?',2),(115,'Catar',1),(116,'Chad',2),(117,'Chile',1),(118,'China',2),(119,'Chipre',1),(120,'Colombia',2),(121,'Comoras',1),(122,'Congo',2),(123,'CoreadelNorte',1),(124,'CoreadelSur',2),(125,'CostadeMarfil',1),(126,'CostaRica',2),(127,'Croacia',1),(128,'Cuba',2),(129,'Dinamarca',1),(130,'Dominica',2),(131,'Dubai',1),(132,'Ecuador',2),(133,'Egipto',1),(134,'ElSalvador',2),(135,'Emiratos?rabesUnidos',1),(136,'Eritrea',2),(137,'Eslovaquia',1),(138,'Eslovenia',2),(139,'Espa?a',1),(140,'EstadosUnidosdeAm?rica',2),(141,'Estonia',1),(142,'Etiop?a',2),(143,'Fiyi',1),(144,'Filipinas',2),(145,'Finlandia',1),(146,'Francia',2),(147,'Gab?n',1),(148,'Gambia',2),(149,'Georgia',1),(150,'Ghana',2),(151,'Grecia',1),(152,'Guam',2),(153,'Guatemala',1),(154,'GuayanaFrancesa',2),(155,'Guinea-Bissau',1),(156,'GuineaEcuatorial',2),(157,'Guinea',1),(158,'Guyana',2),(159,'Granada',1),(160,'Hait?',2),(161,'Honduras',1),(162,'Hong',2),(163,'Kong',1),(164,'Hungr?a',2),(165,'Holanda',1),(166,'India',2),(167,'Indonesia',1),(168,'Irak',2),(169,'Ir?n',1),(170,'Irlanda',2),(171,'Islandia',1),(172,'IslasCaim?n',2),(173,'IslasMarshall',1),(174,'IslasPitcairn',2),(175,'IslasSalom?n',1),(176,'Israel',2),(177,'Italia',1),(178,'Jamaica',2),(179,'Jap?n',1),(180,'Jordania',2),(181,'Kazajst?n',1),(182,'Kenia',2),(183,'Kirguist?n',1),(184,'Kiribati',2),(185,'K?sovo',1),(186,'Kuwait',2),(187,'Laos',1),(188,'Lesotho',2),(189,'Letonia',1),(190,'L?bano',2),(191,'Liberia',1),(192,'Libia',2),(193,'Liechtenstein',1),(194,'Lituania',2),(195,'Luxemburgo',1),(196,'Macedonia',2),(197,'Madagascar',1),(198,'Malasia',2),(199,'Malawi',1),(200,'Maldivas',2),(201,'Mal?',1),(202,'Malta',2),(203,'MarianasdelNorte',1),(204,'Marruecos',2),(205,'Mauricio',1),(206,'Mauritania',2),(207,'M?xico',1),(208,'Micronesia',2),(209,'M?naco',1),(210,'Moldavia',2),(211,'Mongolia',1),(212,'Montenegro',2),(213,'Mozambique',1),(214,'Myanmar',2),(215,'Namibia',1),(216,'Nauru',2),(217,'Nepal',1),(218,'Nicaragua',2),(219,'N?ger',1),(220,'Nigeria',2),(221,'Noruega',1),(222,'NuevaZelanda',2),(223,'Om?n',1),(224,'OrdendeMalta',2),(225,'Pa?sesBajos',1),(226,'Pakist?n',2),(227,'Palestina',1),(228,'Palau',2),(229,'Panam?',1),(230,'Pap?aNuevaGuinea',2),(231,'Paraguay',1),(232,'Per?',2),(233,'Polonia',1),(234,'Portugal',2),(235,'PuertoRico',1),(236,'ReinoUnido',2),(237,'Rep?blicaCentroafricana',1),(238,'Rep?blicaCheca',2),(239,'Rep?blicadelCongo',1),(240,'Rep?blicaDemocr?ticadelCongo',2),(241,'Rep?blicaDominicana',1),(242,'Ruanda',2),(243,'Rumania',1),(244,'Rusia',2),(245,'S?haraOccidental',1),(246,'SamoaAmericana',2),(247,'Samoa',1),(248,'SanCrist?balyNieves',2),(249,'SanMarino',1),(250,'SantaLuc?a',2),(251,'SantoTom?yPr?ncipe',1),(252,'SanVicenteylasGranadinas',2),(253,'Senegal',1),(254,'Serbia',2),(255,'Seychelles',1),(256,'Sierra',2),(257,'Leona',1),(258,'Singapur',2),(259,'Siria',1),(260,'Somalia',2),(261,'SriLanka',1),(262,'Sud?frica',2),(263,'Sud?n',1),(264,'Suecia',2),(265,'Suiza',1),(266,'Suazilandia',2),(267,'Tailandia',1),(268,'Taiw?n',2),(269,'Tanzania',1),(270,'Tayikist?n',2),(271,'T?bet',1),(272,'Timor',2),(273,'Togo',1),(274,'Tonga',2),(275,'Trinidad',1),(276,'y',2),(277,'Tobago',1),(278,'T?nez',2),(279,'Turkmenist?n',1),(280,'Turqu?a',2),(281,'Tuvalu',1),(282,'Ucrania',2),(283,'Uganda',1),(284,'Uruguay',2),(285,'Uzbequist?n',1),(286,'Vanuatu',2),(287,'Vaticano',1),(288,'Venezuela',2),(289,'Vietnam',1),(290,'Wallis',2),(291,'Yemen',1),(292,'Yibuti',2),(293,'Zambia',1),(294,'Zaire',2),(295,'Zimbabue',1),(296,'Bolivia',2),(297,'Botswana',1),(298,'Bosnia',2),(299,'Brasil',1),(300,'Catar',1);
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `Cod_tipo_doc` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_tipo_doc`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
INSERT INTO `tipodocumento` VALUES (1,'Cedula'),(2,'Cedula Extranjeria'),(3,'Tarjeta Identidad');
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipojornada`
--

DROP TABLE IF EXISTS `tipojornada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipojornada` (
  `Cod_tipo_jornada` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Cod_tipo_jornada`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipojornada`
--

LOCK TABLES `tipojornada` WRITE;
/*!40000 ALTER TABLE `tipojornada` DISABLE KEYS */;
INSERT INTO `tipojornada` VALUES (1,'Diurna'),(2,'Nocturna'),(3,'Mixta');
/*!40000 ALTER TABLE `tipojornada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `titulacion`
--

DROP TABLE IF EXISTS `titulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `titulacion` (
  `Cod_titulacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Numero_creditos` int(11) DEFAULT NULL,
  `Carga_lectiva` varchar(70) DEFAULT NULL,
  `fkcodcampus` bigint(20) DEFAULT NULL,
  `fkcodestadotitulacion` bigint(20) DEFAULT NULL,
  `fkcodestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Cod_titulacion`),
  KEY `titulacioncampus` (`fkcodcampus`),
  KEY `titulacionestadotitulacion` (`fkcodestadotitulacion`),
  KEY `titulacionestado` (`fkcodestado`),
  CONSTRAINT `titulacioncampus` FOREIGN KEY (`fkcodcampus`) REFERENCES `campus` (`Cod_campus`),
  CONSTRAINT `titulacionestado` FOREIGN KEY (`fkcodestado`) REFERENCES `estado` (`Cod_estado`),
  CONSTRAINT `titulacionestadotitulacion` FOREIGN KEY (`fkcodestadotitulacion`) REFERENCES `estadotitulacion` (`Cod_estado_titulacion`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titulacion`
--

LOCK TABLES `titulacion` WRITE;
/*!40000 ALTER TABLE `titulacion` DISABLE KEYS */;
INSERT INTO `titulacion` VALUES (1,10,'Plan numero 1 para la materia ?rabeIII',1,1,1),(2,11,'Plan numero 2 para la materia ?rabeIV',2,2,2),(3,12,'Plan numero 3 para la materia ?rabeMarroqu?',3,3,1),(4,13,'Plan numero 4 para la materia ?rabeMarroqu?',4,4,2),(5,14,'Plan numero 5 para la materia ?rabeV',5,1,1),(6,15,'Plan numero 6 para la materia ?rabeVI',6,2,2),(7,16,'Plan numero 7 para la materia ?rabeVII',7,3,1),(8,17,'Plan numero 8 para la materia ?rabeVIII',8,4,2),(9,18,'Plan numero 9 para la materia Arameo',9,1,1),(10,19,'Plan numero 10 para la materia Arameo(Sir?aco)Cl?sicoOriental',10,2,2),(11,20,'Plan numero 11 para la materia ArameoModerno',11,3,1),(12,21,'Plan numero 12 para la materia ArameoModernoI',12,4,2),(13,22,'Plan numero 13 para la materia ArameoModernoII',13,1,1),(14,23,'Plan numero 14 para la materia Arqueolog?aCl?sica',14,2,2),(15,24,'Plan numero 15 para la materia ArteIsl?micoeHispanomusulm?n',15,3,1),(16,25,'Plan numero 16 para la materia Catal?nI',16,4,2),(17,26,'Plan numero 17 para la materia Catal?nII',17,1,1),(18,27,'Plan numero 18 para la materia Cl?sicosdelaliteraturabrasile?a',18,2,2),(19,28,'Plan numero 19 para la materia Cl?sicosdelaliteraturaportuguesaI',19,3,1),(20,29,'Plan numero 20 para la materia Cl?sicosdeliteraturaportuguesaII',20,4,2),(21,30,'Plan numero 21 para la materia ComentariodeTextosAlemanes',21,1,1),(22,31,'Plan numero 22 para la materia ComentariodeTextosdelasLiteraturasHisp?',22,2,2),(23,32,'Plan numero 23 para la materia ComentariodeTextosHist?ricosenLenguaIng',23,3,1),(24,33,'Plan numero 24 para la materia ComentariodeTextosItalianos',24,4,2),(25,34,'Plan numero 25 para la materia ComentarioFilol?gicodeTextos',25,1,1),(26,35,'Plan numero 26 para la materia ComentarioFilol?gicodeTextos',26,2,2),(27,36,'Plan numero 27 para la materia ComentarioFilol?gicodeTextos',27,3,1),(28,37,'Plan numero 28 para la materia ComentarioLing??stico',28,4,2),(29,38,'Plan numero 29 para la materia ComentarioLiterariodeTextosFranceses',29,1,1),(30,39,'Plan numero 30 para la materia ComentarioLiterariodeTextosFrancesesI',30,2,2),(31,40,'Plan numero 31 para la materia ComentariosdeTextosPo?ticosLatinosdela?',31,3,1),(32,41,'Plan numero 32 para la materia HistoriadelaLengua',32,4,2),(33,42,'Plan numero 33 para la materia Francesa',33,1,1),(34,43,'Plan numero 34 para la materia Historiadelaliteraturafrancesa:EdadMedi',34,2,2),(35,44,'Plan numero 35 para la materia Historiadelaliteraturafrancesa:SiglosXI',35,3,1),(36,45,'Plan numero 36 para la materia Historiadelaliteraturafrancesa:SiglosXV',36,4,2),(37,46,'Plan numero 37 para la materia Lexicograf?aySem?nticaFrancesas',37,1,1),(38,47,'Plan numero 38 para la materia LiteraturaAlemana:L?rica',38,2,2),(39,48,'Plan numero 39 para la materia LiteraturaFrancesayPensamiento:SiglosXV',39,3,1),(40,49,'Plan numero 40 para la materia Teor?asCr?ticasyAn?lisisdeTextosLiterar',40,4,2),(41,50,'Plan numero 41 para la materia Alem?n',41,1,1),(42,10,'Plan numero 42 para la materia Alemania,AustriaySuiza:identidadescultu',42,2,2),(43,11,'Plan numero 43 para la materia Alemania,AustriaySuiza:identidadescultu',43,3,1),(44,12,'Plan numero 44 para la materia An?lisisdeTextos',44,4,2),(45,13,'Plan numero 45 para la materia ItalianosLiterarios',45,1,1),(46,14,'Plan numero 46 para la materia Contemp.',46,2,2),(47,15,'Plan numero 47 para la materia An?lisisdelDiscursoenLenguaInglesa',47,3,1),(48,16,'Plan numero 48 para la materia An?lisisdelDiscursoLiterarioenLenguaIng',48,4,2),(49,17,'Plan numero 49 para la materia An?lisisdeldiscursoydiscursosocial',49,1,1),(50,18,'Plan numero 50 para la materia An?lisisdeldiscursoylenguahablada',50,2,2),(51,19,'Plan numero 51 para la materia An?lisiseinterpretaci?ndetextosliterari',51,3,1),(52,20,'Plan numero 52 para la materia An?lisiseinterpretaci?ndetextosliterari',52,4,2),(53,21,'Plan numero 53 para la materia An?lisiseinterpretaci?ndetextosliterari',53,1,1),(54,22,'Plan numero 54 para la materia An?lisisFilol?icoDeTextosRomancesMediev',54,2,2),(55,23,'Plan numero 55 para la materia An?lisisLing??sticaDiacr?nicodeTextosLi',55,3,1),(56,24,'Plan numero 56 para la materia An?lisisling??sticodetextoscient?fico-t',56,4,2),(57,25,'Plan numero 57 para la materia ?rabeHablado',57,1,1),(58,26,'Plan numero 58 para la materia ?rabeablado',58,2,2),(59,27,'Plan numero 59 para la materia ?rabeI',59,3,1),(60,28,'Plan numero 60 para la materia ?rabeII',60,4,2),(61,29,'Plan numero 61 para la materia ?rabeIII',61,1,1),(62,30,'Plan numero 62 para la materia ?rabeIV',62,2,2),(63,31,'Plan numero 63 para la materia ?rabeMarroqu?',63,3,1),(64,32,'Plan numero 64 para la materia ?rabeMarroqu?',64,4,2),(65,33,'Plan numero 65 para la materia ?rabeV',65,1,1),(66,34,'Plan numero 66 para la materia ?rabeVI',66,2,2),(67,35,'Plan numero 67 para la materia ?rabeVII',67,3,1),(68,36,'Plan numero 68 para la materia ?rabeVIII',68,4,2),(69,37,'Plan numero 69 para la materia Arameo',69,1,1),(70,38,'Plan numero 70 para la materia Arameo(Sir?aco)Cl?sicoOriental',70,2,2),(71,39,'Plan numero 71 para la materia ArameoModerno',71,3,1),(72,40,'Plan numero 72 para la materia ArameoModernoI',72,4,2),(73,41,'Plan numero 73 para la materia ArameoModernoII',73,1,1),(74,42,'Plan numero 74 para la materia Arqueolog?aCl?sica',74,2,2),(75,43,'Plan numero 75 para la materia ArteIsl?micoeHispanomusulm?n',75,3,1),(76,44,'Plan numero 76 para la materia Catal?nI',76,4,2),(77,45,'Plan numero 77 para la materia Catal?nII',77,1,1),(78,46,'Plan numero 78 para la materia Cl?sicosdelaliteraturabrasile?a',78,2,2),(79,47,'Plan numero 79 para la materia Cl?sicosdelaliteraturaportuguesaI',79,3,1),(80,48,'Plan numero 80 para la materia Cl?sicosdeliteraturaportuguesaII',80,4,2),(81,49,'Plan numero 81 para la materia ComentariodeTextosAlemanes',81,1,1),(82,50,'Plan numero 82 para la materia ComentariodeTextosdelasLiteraturasHisp?',82,2,2),(83,10,'Plan numero 83 para la materia ComentariodeTextosHist?ricosenLenguaIng',83,3,1),(84,11,'Plan numero 84 para la materia ComentariodeTextosItalianos',84,4,2),(85,12,'Plan numero 85 para la materia ComentarioFilol?gicodeTextos',85,1,1),(86,13,'Plan numero 86 para la materia ComentarioFilol?gicodeTextos',86,2,2),(87,14,'Plan numero 87 para la materia ComentarioFilol?gicodeTextos',87,3,1),(88,15,'Plan numero 88 para la materia ComentarioLing??stico',88,4,2),(89,16,'Plan numero 89 para la materia ComentarioLiterariodeTextosFranceses',89,1,1),(90,17,'Plan numero 90 para la materia ComentarioLiterariodeTextosFrancesesI',90,2,2),(91,18,'Plan numero 91 para la materia ComentariosdeTextosPo?ticosLatinosdela?',91,3,1),(92,19,'Plan numero 92 para la materia HistoriadelaLengua',92,4,2),(93,20,'Plan numero 93 para la materia Francesa',93,1,1),(94,21,'Plan numero 94 para la materia Historiadelaliteraturafrancesa:EdadMedi',94,2,2),(95,22,'Plan numero 95 para la materia Historiadelaliteraturafrancesa:SiglosXI',95,3,1),(96,23,'Plan numero 96 para la materia Historiadelaliteraturafrancesa:SiglosXV',96,4,2),(97,24,'Plan numero 97 para la materia Lexicograf?aySem?nticaFrancesas',97,1,1),(98,25,'Plan numero 98 para la materia LiteraturaAlemana:L?rica',98,2,2),(99,26,'Plan numero 99 para la materia LiteraturaFrancesayPensamiento:SiglosXV',99,3,1),(100,27,'Plan numero 100 para la materia Teor?asCr?ticasyAn?lisisdeTextosLitera',100,4,2),(101,28,'Plan numero 101 para la materia Alem?n',101,1,1),(102,29,'Plan numero 102 para la materia Alemania,AustriaySuiza:identidadescult',102,2,2),(103,30,'Plan numero 103 para la materia Alemania,AustriaySuiza:identidadescult',103,3,1),(104,31,'Plan numero 104 para la materia An?lisisdeTextos',104,4,2),(105,32,'Plan numero 105 para la materia ItalianosLiterarios',105,1,1),(106,33,'Plan numero 106 para la materia Contemp.',106,2,2),(107,34,'Plan numero 107 para la materia An?lisisdelDiscursoenLenguaInglesa',107,3,1),(108,35,'Plan numero 108 para la materia An?lisisdelDiscursoLiterarioenLenguaIn',108,4,2),(109,36,'Plan numero 109 para la materia An?lisisdeldiscursoydiscursosocial',109,1,1),(110,37,'Plan numero 110 para la materia An?lisisdeldiscursoylenguahablada',110,2,2),(111,38,'Plan numero 111 para la materia An?lisiseinterpretaci?ndetextosliterar',111,3,1),(112,39,'Plan numero 112 para la materia An?lisiseinterpretaci?ndetextosliterar',112,4,2),(113,40,'Plan numero 113 para la materia An?lisiseinterpretaci?ndetextosliterar',113,1,1),(114,41,'Plan numero 114 para la materia An?lisisFilol?icoDeTextosRomancesMedie',114,2,2),(115,42,'Plan numero 115 para la materia An?lisisLing??sticaDiacr?nicodeTextosL',115,3,1),(116,43,'Plan numero 116 para la materia An?lisisling??sticodetextoscient?fico-',116,4,2),(117,44,'Plan numero 117 para la materia ?rabeHablado',117,1,1),(118,45,'Plan numero 118 para la materia ?rabeablado',118,2,2),(119,46,'Plan numero 119 para la materia ?rabeI',119,3,1),(120,47,'Plan numero 120 para la materia ?rabeII',120,4,2),(121,48,'Plan numero 121 para la materia ?rabeIII',121,1,1),(122,49,'Plan numero 122 para la materia ?rabeIV',122,2,2),(123,50,'Plan numero 123 para la materia ?rabeMarroqu?',123,3,1),(124,10,'Plan numero 124 para la materia ?rabeMarroqu?',124,4,2),(125,11,'Plan numero 125 para la materia ?rabeV',125,1,1),(126,12,'Plan numero 126 para la materia ?rabeVI',126,2,2),(127,13,'Plan numero 127 para la materia ?rabeVII',127,3,1),(128,14,'Plan numero 128 para la materia ?rabeVIII',128,4,2),(129,15,'Plan numero 129 para la materia Arameo',129,1,1),(130,16,'Plan numero 130 para la materia Arameo(Sir?aco)Cl?sicoOriental',130,2,2),(131,17,'Plan numero 131 para la materia ArameoModerno',131,3,1),(132,18,'Plan numero 132 para la materia ArameoModernoI',132,4,2),(133,19,'Plan numero 133 para la materia ArameoModernoII',133,1,1),(134,20,'Plan numero 134 para la materia Arqueolog?aCl?sica',134,2,2),(135,21,'Plan numero 135 para la materia ArteIsl?micoeHispanomusulm?n',135,3,1),(136,22,'Plan numero 136 para la materia Catal?nI',136,4,2),(137,23,'Plan numero 137 para la materia Catal?nII',137,1,1),(138,24,'Plan numero 138 para la materia Cl?sicosdelaliteraturabrasile?a',138,2,2),(139,25,'Plan numero 139 para la materia Cl?sicosdelaliteraturaportuguesaI',139,3,1),(140,26,'Plan numero 140 para la materia Cl?sicosdeliteraturaportuguesaII',140,4,2),(141,27,'Plan numero 141 para la materia ComentariodeTextosAlemanes',141,1,1),(142,28,'Plan numero 142 para la materia ComentariodeTextosdelasLiteraturasHisp',142,2,2),(143,29,'Plan numero 143 para la materia ComentariodeTextosHist?ricosenLenguaIn',143,3,1),(144,30,'Plan numero 144 para la materia ComentariodeTextosItalianos',144,4,2),(145,31,'Plan numero 145 para la materia ComentarioFilol?gicodeTextos',145,1,1),(146,32,'Plan numero 146 para la materia ComentarioFilol?gicodeTextos',146,2,2),(147,33,'Plan numero 147 para la materia ComentarioFilol?gicodeTextos',147,3,1),(148,34,'Plan numero 148 para la materia ComentarioLing??stico',148,4,2),(149,35,'Plan numero 149 para la materia ComentarioLiterariodeTextosFranceses',149,1,1),(150,36,'002585150 para la materia HistoriadelaLengua',150,2,2),(151,37,'002585151 para la materia Francesa',151,3,1),(152,38,'002585152 para la materia Historiadelaliteraturafrancesa:EdadMediayien',152,4,2),(153,39,'002585153 para la materia Historiadelaliteraturafrancesa:SiglosXIXyXX',153,1,1),(154,40,'002585154 para la materia Historiadelaliteraturafrancesa:SiglosXVIIyXV',154,2,2),(155,41,'002585155 para la materia Lexicograf?aySem?nticaFrancesas',155,3,1),(156,42,'002585156 para la materia LiteraturaAlemana:L?rica',156,4,2),(157,43,'002585157 para la materia LiteraturaFrancesayPensamiento:SiglosXVIIyXV',157,1,1),(158,44,'002585158 para la materia Teor?asCr?ticasyAn?lisisdeTextosLiterarioses',158,2,2),(159,45,'002585159 para la materia Alem?n',159,3,1),(160,46,'002585160 para la materia Alemania,AustriaySuiza:identidadesculturales',160,4,2),(161,47,'002585161 para la materia Alemania,AustriaySuiza:identidadesculturales',161,1,1),(162,48,'002585162 para la materia An?lisisdeTextos',162,2,2),(163,49,'002585163 para la materia ItalianosLiterarios',163,3,1),(164,50,'002585164 para la materia Contemp.',164,4,2),(165,10,'002585165 para la materia An?lisisdelDiscursoenLenguaInglesa',165,1,1),(166,11,'002585166 para la materia An?lisisdelDiscursoLiterarioenLenguaInglesa',166,2,2),(167,12,'002585167 para la materia An?lisisdeldiscursoydiscursosocial',167,3,1),(168,13,'002585168 para la materia An?lisisdeldiscursoylenguahablada',168,4,2),(169,14,'002585169 para la materia An?lisiseinterpretaci?ndetextosliterariosale',169,1,1),(170,15,'002585170 para la materia An?lisiseinterpretaci?ndetextosliterariosale',170,2,2),(171,16,'002585171 para la materia An?lisiseinterpretaci?ndetextosliterariosale',171,3,1),(172,17,'002585172 para la materia An?lisisFilol?icoDeTextosRomancesMedievales',172,4,2),(173,18,'002585173 para la materia An?lisisLing??sticaDiacr?nicodeTextosLiterar',173,1,1),(174,19,'002585174 para la materia An?lisisling??sticodetextoscient?fico-t?cnic',174,2,2),(175,20,'002585175 para la materia ?rabeHablado',175,3,1),(176,21,'002585176 para la materia ?rabeablado',176,4,2),(177,22,'002585177 para la materia ?rabeI',177,1,1),(178,23,'002585178 para la materia ?rabeII',178,2,2),(179,24,'002585179 para la materia ?rabeIII',179,3,1),(180,25,'002585180 para la materia ?rabeIV',180,4,2),(181,26,'002585181 para la materia ?rabeMarroqu?',181,1,1),(182,27,'002585182 para la materia ?rabeMarroqu?',182,2,2),(183,28,'002585183 para la materia ?rabeV',183,3,1),(184,29,'002585184 para la materia ?rabeVI',184,4,2),(185,30,'002585185 para la materia ?rabeVII',185,1,1),(186,31,'002585186 para la materia ?rabeVIII',186,2,2),(187,32,'002585187 para la materia Arameo',187,3,1),(188,33,'002585188 para la materia Arameo(Sir?aco)Cl?sicoOriental',188,4,2),(189,34,'002585189 para la materia ArameoModerno',189,1,1),(190,35,'002585190 para la materia ArameoModernoI',190,2,2),(191,36,'002585191 para la materia ArameoModernoII',191,3,1),(192,37,'002585192 para la materia Arqueolog?aCl?sica',192,4,2),(193,38,'002585193 para la materia ArteIsl?micoeHispanomusulm?n',193,1,1),(194,39,'002585194 para la materia Catal?nI',194,2,2),(195,40,'002585195 para la materia Catal?nII',195,3,1),(196,41,'002585196 para la materia Cl?sicosdelaliteraturabrasile?a',196,4,2),(197,42,'002585197 para la materia Cl?sicosdelaliteraturaportuguesaI',197,1,1),(198,43,'002585198 para la materia Cl?sicosdeliteraturaportuguesaII',198,2,2),(199,44,'002585199 para la materia ComentariodeTextosAlemanes',199,3,1),(200,45,'002585200 para la materia ComentariodeTextosdelasLiteraturasHisp?nicas',200,4,2),(201,46,'002585201 para la materia ComentariodeTextosHist?ricosenLenguaInglesa',201,1,1),(202,47,'002585202 para la materia ComentariodeTextosItalianos',202,2,2),(203,48,'002585203 para la materia ComentarioFilol?gicodeTextos',203,3,1),(204,49,'002585204 para la materia ComentarioFilol?gicodeTextos',204,4,2),(205,50,'002585205 para la materia ComentarioFilol?gicodeTextos',205,1,1),(206,10,'002585206 para la materia ComentarioLing??stico',206,2,2),(207,11,'002585207 para la materia ComentarioLiterariodeTextosFranceses',207,3,1),(208,12,'002585208 para la materia ComentarioLiterariodeTextosFrancesesI',208,4,2),(209,13,'002585209 para la materia ComentariosdeTextosPo?ticosLatinosdela?pocaC',209,1,1),(210,14,'002585210 para la materia HistoriadelaLengua',210,2,2),(211,15,'002585211 para la materia Francesa',211,3,1),(212,16,'002585212 para la materia Historiadelaliteraturafrancesa:EdadMediayien',212,4,2),(213,17,'002585213 para la materia Historiadelaliteraturafrancesa:SiglosXIXyXX',213,1,1),(214,18,'002585214 para la materia Historiadelaliteraturafrancesa:SiglosXVIIyXV',214,2,2),(215,19,'002585215 para la materia Lexicograf?aySem?nticaFrancesas',215,3,1),(216,20,'002585216 para la materia LiteraturaAlemana:L?rica',216,4,2),(217,21,'002585217 para la materia LiteraturaFrancesayPensamiento:SiglosXVIIyXV',217,1,1),(218,22,'002585218 para la materia Teor?asCr?ticasyAn?lisisdeTextosLiterarioses',218,2,2),(219,23,'002585219 para la materia Alem?n',219,3,1),(220,24,'002585220 para la materia Alemania,AustriaySuiza:identidadesculturales',220,4,2),(221,25,'002585221 para la materia Alemania,AustriaySuiza:identidadesculturales',221,1,1),(222,26,'002585222 para la materia An?lisisdeTextos',222,2,2),(223,27,'002585223 para la materia ItalianosLiterarios',223,3,1),(224,28,'002585224 para la materia Contemp.',224,4,2),(225,29,'002585225 para la materia An?lisisdelDiscursoenLenguaInglesa',225,1,1),(226,30,'002585226 para la materia An?lisisdelDiscursoLiterarioenLenguaInglesa',226,2,2),(227,31,'002585227 para la materia An?lisisdeldiscursoydiscursosocial',227,3,1),(228,32,'002585228 para la materia An?lisisdeldiscursoylenguahablada',228,4,2),(229,33,'002585229 para la materia An?lisiseinterpretaci?ndetextosliterariosale',229,1,1),(230,34,'002585230 para la materia An?lisiseinterpretaci?ndetextosliterariosale',230,2,2),(231,35,'002585231 para la materia An?lisiseinterpretaci?ndetextosliterariosale',231,3,1),(232,36,'002585232 para la materia An?lisisFilol?icoDeTextosRomancesMedievales',232,4,2),(233,37,'002585233 para la materia An?lisisLing??sticaDiacr?nicodeTextosLiterar',233,1,1),(234,38,'002585234 para la materia An?lisisling??sticodetextoscient?fico-t?cnic',234,2,2),(235,39,'002585235 para la materia ?rabeHablado',235,3,1),(236,40,'002585236 para la materia ?rabeablado',236,4,2),(237,41,'002585237 para la materia ?rabeI',237,1,1),(238,42,'002585238 para la materia ?rabeII',238,2,2),(239,43,'002585239 para la materia ?rabeIII',239,3,1),(240,44,'002585240 para la materia ?rabeIV',240,4,2),(241,45,'002585241 para la materia ?rabeMarroqu?',241,1,1),(242,46,'002585242 para la materia ?rabeMarroqu?',242,2,2),(243,47,'002585243 para la materia ?rabeV',243,3,1),(244,48,'002585244 para la materia ?rabeVI',244,4,2),(245,49,'002585245 para la materia ?rabeVII',245,1,1),(246,50,'002585246 para la materia ?rabeVIII',246,2,2),(247,10,'002585247 para la materia Arameo',247,3,1),(248,11,'002585248 para la materia Arameo(Sir?aco)Cl?sicoOriental',248,4,2),(249,12,'002585249 para la materia ArameoModerno',249,1,1),(250,13,'002585250 para la materia ArameoModernoI',250,2,2),(251,14,'002585251 para la materia ArameoModernoII',251,3,1),(252,15,'002585252 para la materia Arqueolog?aCl?sica',252,4,2),(253,16,'002585253 para la materia ArteIsl?micoeHispanomusulm?n',253,1,1),(254,17,'002585254 para la materia Catal?nI',254,2,2),(255,18,'002585255 para la materia Catal?nII',255,3,1),(256,19,'002585256 para la materia Cl?sicosdelaliteraturabrasile?a',256,4,2),(257,20,'002585257 para la materia Cl?sicosdelaliteraturaportuguesaI',257,1,1),(258,21,'002585258 para la materia Cl?sicosdeliteraturaportuguesaII',258,2,2),(259,22,'002585259 para la materia ComentariodeTextosAlemanes',259,3,1),(260,23,'002585260 para la materia ComentariodeTextosdelasLiteraturasHisp?nicas',260,4,2),(261,24,'002585261 para la materia ComentariodeTextosHist?ricosenLenguaInglesa',261,1,1),(262,25,'002585262 para la materia ComentariodeTextosItalianos',262,2,2),(263,26,'002585263 para la materia ComentarioFilol?gicodeTextos',263,3,1),(264,27,'002585264 para la materia ComentarioFilol?gicodeTextos',264,4,2),(265,28,'002585265 para la materia ComentarioFilol?gicodeTextos',265,1,1),(266,29,'002585266 para la materia ComentarioLing??stico',266,2,2),(267,30,'002585267 para la materia ComentarioLiterariodeTextosFranceses',267,3,1),(268,31,'002585268 para la materia ComentarioLiterariodeTextosFrancesesI',268,4,2),(269,32,'002585269 para la materia ComentariosdeTextosPo?ticosLatinosdela?pocaC',269,1,1),(270,33,'002585270 para la materia HistoriadelaLengua',270,2,2),(271,34,'002585271 para la materia Francesa',271,3,1),(272,35,'002585272 para la materia Historiadelaliteraturafrancesa:EdadMediayien',272,4,2),(273,36,'002585273 para la materia Historiadelaliteraturafrancesa:SiglosXIXyXX',273,1,1),(274,37,'002585274 para la materia Historiadelaliteraturafrancesa:SiglosXVIIyXV',274,2,2),(275,38,'002585275 para la materia Lexicograf?aySem?nticaFrancesas',275,3,1),(276,39,'002585276 para la materia LiteraturaAlemana:L?rica',276,4,2),(277,40,'002585277 para la materia LiteraturaFrancesayPensamiento:SiglosXVIIyXV',277,1,1),(278,41,'002585278 para la materia Teor?asCr?ticasyAn?lisisdeTextosLiterarioses',278,2,2),(279,42,'002585279 para la materia Alem?n',279,3,1),(280,43,'002585280 para la materia Alemania,AustriaySuiza:identidadesculturales',280,4,2),(281,44,'002585281 para la materia Alemania,AustriaySuiza:identidadesculturales',281,1,1),(282,45,'002585282 para la materia An?lisisdeTextos',282,2,2),(283,46,'002585283 para la materia ItalianosLiterarios',283,3,1),(284,47,'002585284 para la materia Contemp.',284,4,2),(285,48,'002585285 para la materia An?lisisdelDiscursoenLenguaInglesa',285,1,1),(286,49,'002585286 para la materia An?lisisdelDiscursoLiterarioenLenguaInglesa',286,2,2),(287,50,'002585287 para la materia An?lisisdeldiscursoydiscursosocial',287,3,1),(288,10,'002585288 para la materia An?lisisdeldiscursoylenguahablada',288,4,2),(289,11,'002585289 para la materia An?lisiseinterpretaci?ndetextosliterariosale',289,1,1),(290,12,'002585290 para la materia An?lisiseinterpretaci?ndetextosliterariosale',290,2,2),(291,13,'002585291 para la materia An?lisiseinterpretaci?ndetextosliterariosale',291,3,1),(292,14,'002585292 para la materia An?lisisFilol?icoDeTextosRomancesMedievales',292,4,2),(293,15,'002585293 para la materia An?lisisLing??sticaDiacr?nicodeTextosLiterar',293,1,1),(294,16,'002585294 para la materia An?lisisling??sticodetextoscient?fico-t?cnic',294,2,2),(295,17,'002585295 para la materia ?rabeHablado',295,3,1),(296,18,'002585296 para la materia ?rabeablado',296,4,2),(297,19,'002585297 para la materia ?rabeI',297,1,1),(298,20,'002585298 para la materia ?rabeII',298,2,2),(299,21,'002585299 para la materia ?rabeIII',299,3,1),(300,22,'002585300 para la materia árabeIII',300,4,2);
/*!40000 ALTER TABLE `titulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `contadorestadoacta`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoacta`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoacta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoacta` AS (select count(0) AS `count(*)` from `estadoactaac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoactaac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoactaac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoactaac` AS (select count(0) AS `count(*)` from `estadoactaac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoactafinaly`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoactafinaly`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactafinaly`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoactafinaly` AS (select count(0) AS `count(*)` from `estadoactafinaly`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoactain`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoactain`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoactain` AS (select count(0) AS `count(*)` from `estadoactain`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoactanul`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoactanul`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactanul`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoactanul` AS (select count(0) AS `count(*)` from `estadoactaanul`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoactaproce`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoactaproce`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactaproce`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoactaproce` AS (select count(0) AS `count(*)` from `estadoactaproce`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoactasuspen`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoactasuspen`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoactasuspen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoactasuspen` AS (select count(0) AS `count(*)` from `estadoactasuspen`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoac` AS (select count(0) AS `count(*)` from `estadoalumnoac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoas`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoas`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoas` AS (select count(0) AS `count(*)` from `estadoalumnoas`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoenespera`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoenespera`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoenespera`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoenespera` AS (select count(0) AS `count(*)` from `estadoalumnoenespera`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoenformacion`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoenformacion`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoenformacion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoenformacion` AS (select count(0) AS `count(*)` from `estadoalumnoenformacion`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoin` AS (select count(0) AS `count(*)` from `estadoalumnoin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoma`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoma`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoma`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoma` AS (select count(0) AS `count(*)` from `estadoalumnoma`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnoretirado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnoretirado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnoretirado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnoretirado` AS (select count(0) AS `count(*)` from `estadoalumnoretirado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoalumnosuspen`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoalumnosuspen`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoalumnosuspen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoalumnosuspen` AS (select count(0) AS `count(*)` from `estadoalumnosuspen`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoasignaturaac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoasignaturaac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoasignaturaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoasignaturaac` AS (select count(0) AS `count(*)` from `estadoasignaturaac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoasignaturain`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoasignaturain`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoasignaturain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoasignaturain` AS (select count(0) AS `count(*)` from `estadoasignaturain`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocalificacionanul`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocalificacionanul`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocalificacionanul`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocalificacionanul` AS (select count(0) AS `count(*)` from `estadocalificacionanul`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocalificacionvigente`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocalificacionvigente`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocalificacionvigente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocalificacionvigente` AS (select count(0) AS `count(*)` from `estadocalificacionvigente`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocampusac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocampusac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocampusac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocampusac` AS (select count(0) AS `count(*)` from `estadocampusac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocampusin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocampusin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocampusin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocampusin` AS (select count(0) AS `count(*)` from `estadocampusin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocargoac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocargoac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocargoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocargoac` AS (select count(0) AS `count(*)` from `estadocargoac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocargoin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocargoin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocargoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocargoin` AS (select count(0) AS `count(*)` from `estadocargoin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoconvocatoriaac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoconvocatoriaac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoconvocatoriaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoconvocatoriaac` AS (select count(0) AS `count(*)` from `estadoconvocatoriaac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoconvocatoriain`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoconvocatoriain`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoconvocatoriain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoconvocatoriain` AS (select count(0) AS `count(*)` from `estadoconvocatoriain`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocursoac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocursoac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocursoac` AS (select count(0) AS `count(*)` from `estadocursoac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocursoaplazado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocursoaplazado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoaplazado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocursoaplazado` AS (select count(0) AS `count(*)` from `estadocursoaplazado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocursoenformacion`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocursoenformacion`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoenformacion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocursoenformacion` AS (select count(0) AS `count(*)` from `estadocursoenformacion`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocursoin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocursoin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocursoin` AS (select count(0) AS `count(*)` from `estadocursoin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocursosuspendido`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocursosuspendido`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursosuspendido`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocursosuspendido` AS (select count(0) AS `count(*)` from `estadocursosuspendido`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadocursoterminado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadocursoterminado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadocursoterminado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadocursoterminado` AS (select count(0) AS `count(*)` from `estadocursoterminado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadodepartamentoac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadodepartamentoac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadodepartamentoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadodepartamentoac` AS (select count(0) AS `count(*)` from `estadodepartamentoac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadodepartamentoin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadodepartamentoin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadodepartamentoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadodepartamentoin` AS (select count(0) AS `count(*)` from `estadodepartamentoin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoempleado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoempleado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoempleado` AS (select count(0) AS `count(*)` from `estadoempleadoac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoempleadocontratado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoempleadocontratado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadocontratado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoempleadocontratado` AS (select count(0) AS `count(*)` from `estadoempleadocontratado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoempleadodespedido`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoempleadodespedido`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadodespedido`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoempleadodespedido` AS (select count(0) AS `count(*)` from `estadoempleadodespedido`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoempleadoin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoempleadoin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoempleadoin` AS (select count(0) AS `count(*)` from `estadoempleadoin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoempleadoincapacitado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoempleadoincapacitado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadoincapacitado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoempleadoincapacitado` AS (select count(0) AS `count(*)` from `estadoempleadoincapacitado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoempleadovacaciones`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoempleadovacaciones`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoempleadovacaciones`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoempleadovacaciones` AS (select count(0) AS `count(*)` from `estadoempleadovacaciones`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoepsac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoepsac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoepsac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoepsac` AS (select count(0) AS `count(*)` from `estadoepsac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadoepsin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadoepsin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadoepsin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadoepsin` AS (select count(0) AS `count(*)` from `estadoepsin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadogrupoac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadogrupoac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadogrupoac` AS (select count(0) AS `count(*)` from `estadogrupoac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadogrupoaplazado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadogrupoaplazado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoaplazado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadogrupoaplazado` AS (select count(0) AS `count(*)` from `estadogrupoaplazado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadogrupocancelado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadogrupocancelado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupocancelado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadogrupocancelado` AS (select count(0) AS `count(*)` from `estadogrupocancelado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadogrupoenformacion`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadogrupoenformacion`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoenformacion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadogrupoenformacion` AS (select count(0) AS `count(*)` from `estadogrupoenformacion`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadogrupoin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadogrupoin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadogrupoin` AS (select count(0) AS `count(*)` from `estadogrupoin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadogrupoterminado`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadogrupoterminado`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadogrupoterminado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadogrupoterminado` AS (select count(0) AS `count(*)` from `estadogrupoterminado`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadohistorialac`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadohistorialac`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadohistorialac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadohistorialac` AS (select count(0) AS `count(*)` from `estadohistorialac`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadohistorialin`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadohistorialin`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadohistorialin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadohistorialin` AS (select count(0) AS `count(*)` from `estadohistorialin`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `contadorestadomatriculain`
--

/*!50001 DROP TABLE IF EXISTS `contadorestadomatriculain`*/;
/*!50001 DROP VIEW IF EXISTS `contadorestadomatriculain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `contadorestadomatriculain` AS (select count(0) AS `count(*)` from `estadomatriculain`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoactaac`
--

/*!50001 DROP TABLE IF EXISTS `estadoactaac`*/;
/*!50001 DROP VIEW IF EXISTS `estadoactaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoactaac` AS (select `acta`.`Cod_acta` AS `cod_acta`,`acta`.`fkdocalumno` AS `fkdocalumno` from `acta` where (`acta`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoactaanul`
--

/*!50001 DROP TABLE IF EXISTS `estadoactaanul`*/;
/*!50001 DROP VIEW IF EXISTS `estadoactaanul`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoactaanul` AS (select `acta`.`Cod_acta` AS `cod_acta`,`acta`.`fkdocalumno` AS `fkdocalumno` from `acta` where (`acta`.`fkcodestadoacta` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoactafinaly`
--

/*!50001 DROP TABLE IF EXISTS `estadoactafinaly`*/;
/*!50001 DROP VIEW IF EXISTS `estadoactafinaly`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoactafinaly` AS (select `acta`.`Cod_acta` AS `cod_acta`,`acta`.`fkdocalumno` AS `fkdocalumno` from `acta` where (`acta`.`fkcodestadoacta` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoactain`
--

/*!50001 DROP TABLE IF EXISTS `estadoactain`*/;
/*!50001 DROP VIEW IF EXISTS `estadoactain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoactain` AS (select `acta`.`Cod_acta` AS `cod_acta`,`acta`.`fkdocalumno` AS `fkdocalumno` from `acta` where (`acta`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoactaproce`
--

/*!50001 DROP TABLE IF EXISTS `estadoactaproce`*/;
/*!50001 DROP VIEW IF EXISTS `estadoactaproce`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoactaproce` AS (select `acta`.`Cod_acta` AS `cod_acta`,`acta`.`fkdocalumno` AS `fkdocalumno` from `acta` where (`acta`.`fkcodestadoacta` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoactasuspen`
--

/*!50001 DROP TABLE IF EXISTS `estadoactasuspen`*/;
/*!50001 DROP VIEW IF EXISTS `estadoactasuspen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoactasuspen` AS (select `acta`.`Cod_acta` AS `cod_acta`,`acta`.`fkdocalumno` AS `fkdocalumno` from `acta` where (`acta`.`fkcodestadoacta` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoac`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoac`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoac` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoas`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoas`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoas` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestadoalumno` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoenespera`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoenespera`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoenespera`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoenespera` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestadoalumno` = 5)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoenformacion`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoenformacion`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoenformacion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoenformacion` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestadoalumno` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoin`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoin`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoin` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoma`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoma`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoma`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoma` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestadoalumno` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnoretirado`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnoretirado`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnoretirado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnoretirado` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestadoalumno` = 6)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoalumnosuspen`
--

/*!50001 DROP TABLE IF EXISTS `estadoalumnosuspen`*/;
/*!50001 DROP VIEW IF EXISTS `estadoalumnosuspen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoalumnosuspen` AS (select `alumno`.`Doc_alumno` AS `Doc_alumno`,`alumno`.`Nombre_alumno` AS `Nombre_alumno`,`alumno`.`Apellido_alumno` AS `Apellido_alumno`,`alumno`.`telefono` AS `telefono`,`alumno`.`correo` AS `correo`,`alumno`.`fkcodestado` AS `fkcodestado`,`alumno`.`fkcodestadoalumno` AS `fkcodestadoalumno`,`alumno`.`fkcodmatricula` AS `fkcodmatricula`,`alumno`.`fkcodciudad` AS `fkcodciudad`,`alumno`.`fkcodtipodoc` AS `fkcodtipodoc` from `alumno` where (`alumno`.`fkcodestadoalumno` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoasignaturaac`
--

/*!50001 DROP TABLE IF EXISTS `estadoasignaturaac`*/;
/*!50001 DROP VIEW IF EXISTS `estadoasignaturaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoasignaturaac` AS (select `asignatura`.`Cod_asignatura` AS `Cod_asignatura`,`asignatura`.`Nombre_asiganturadenominacion` AS `Nombre_asiganturadenominacion`,`asignatura`.`fkdocalumno` AS `fkdocalumno`,`asignatura`.`fkcodcurso` AS `fkcodcurso`,`asignatura`.`fkcodcampus` AS `fkcodcampus`,`asignatura`.`fkcodtitulacion` AS `fkcodtitulacion`,`asignatura`.`fkcodconvocatoria` AS `fkcodconvocatoria`,`asignatura`.`fkcodgrupo` AS `fkcodgrupo`,`asignatura`.`fkcodempleado` AS `fkcodempleado`,`asignatura`.`fkcodestado` AS `fkcodestado`,`asignatura`.`fkcodarea` AS `fkcodarea`,`asignatura`.`fkcodcalificacion` AS `fkcodcalificacion` from `asignatura` where (`asignatura`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoasignaturain`
--

/*!50001 DROP TABLE IF EXISTS `estadoasignaturain`*/;
/*!50001 DROP VIEW IF EXISTS `estadoasignaturain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoasignaturain` AS (select `asignatura`.`Cod_asignatura` AS `Cod_asignatura`,`asignatura`.`Nombre_asiganturadenominacion` AS `Nombre_asiganturadenominacion`,`asignatura`.`fkdocalumno` AS `fkdocalumno`,`asignatura`.`fkcodcurso` AS `fkcodcurso`,`asignatura`.`fkcodcampus` AS `fkcodcampus`,`asignatura`.`fkcodtitulacion` AS `fkcodtitulacion`,`asignatura`.`fkcodconvocatoria` AS `fkcodconvocatoria`,`asignatura`.`fkcodgrupo` AS `fkcodgrupo`,`asignatura`.`fkcodempleado` AS `fkcodempleado`,`asignatura`.`fkcodestado` AS `fkcodestado`,`asignatura`.`fkcodarea` AS `fkcodarea`,`asignatura`.`fkcodcalificacion` AS `fkcodcalificacion` from `asignatura` where (`asignatura`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocalificacionanul`
--

/*!50001 DROP TABLE IF EXISTS `estadocalificacionanul`*/;
/*!50001 DROP VIEW IF EXISTS `estadocalificacionanul`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocalificacionanul` AS (select `calificacion`.`Cod_calificacion` AS `Cod_calificacion`,`calificacion`.`Descripcion` AS `Descripcion`,`calificacion`.`fkCod_estado_calificacion` AS `fkCod_estado_calificacion` from `calificacion` where (`calificacion`.`fkCod_estado_calificacion` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocalificacionvigente`
--

/*!50001 DROP TABLE IF EXISTS `estadocalificacionvigente`*/;
/*!50001 DROP VIEW IF EXISTS `estadocalificacionvigente`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocalificacionvigente` AS (select `calificacion`.`Cod_calificacion` AS `Cod_calificacion`,`calificacion`.`Descripcion` AS `Descripcion`,`calificacion`.`fkCod_estado_calificacion` AS `fkCod_estado_calificacion` from `calificacion` where (`calificacion`.`fkCod_estado_calificacion` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocampusac`
--

/*!50001 DROP TABLE IF EXISTS `estadocampusac`*/;
/*!50001 DROP VIEW IF EXISTS `estadocampusac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocampusac` AS (select `campus`.`Cod_campus` AS `Cod_campus`,`campus`.`Nombre` AS `Nombre`,`campus`.`fkcodestado` AS `fkcodestado` from `campus` where (`campus`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocampusin`
--

/*!50001 DROP TABLE IF EXISTS `estadocampusin`*/;
/*!50001 DROP VIEW IF EXISTS `estadocampusin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocampusin` AS (select `campus`.`Cod_campus` AS `Cod_campus`,`campus`.`Nombre` AS `Nombre`,`campus`.`fkcodestado` AS `fkcodestado` from `campus` where (`campus`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocargoac`
--

/*!50001 DROP TABLE IF EXISTS `estadocargoac`*/;
/*!50001 DROP VIEW IF EXISTS `estadocargoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocargoac` AS (select `cargo`.`Cod_cargo` AS `Cod_cargo`,`cargo`.`Descripcion` AS `Descripcion`,`cargo`.`fkcodestado` AS `fkcodestado` from `cargo` where (`cargo`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocargoin`
--

/*!50001 DROP TABLE IF EXISTS `estadocargoin`*/;
/*!50001 DROP VIEW IF EXISTS `estadocargoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocargoin` AS (select `cargo`.`Cod_cargo` AS `Cod_cargo`,`cargo`.`Descripcion` AS `Descripcion`,`cargo`.`fkcodestado` AS `fkcodestado` from `cargo` where (`cargo`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoconvocatoriaac`
--

/*!50001 DROP TABLE IF EXISTS `estadoconvocatoriaac`*/;
/*!50001 DROP VIEW IF EXISTS `estadoconvocatoriaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoconvocatoriaac` AS (select `convocatoria`.`Cod_convocatoria` AS `Cod_convocatoria`,`convocatoria`.`Descripcion` AS `Descripcion`,`convocatoria`.`fkcod_estado` AS `fkcod_estado` from `convocatoria` where (`convocatoria`.`fkcod_estado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoconvocatoriain`
--

/*!50001 DROP TABLE IF EXISTS `estadoconvocatoriain`*/;
/*!50001 DROP VIEW IF EXISTS `estadoconvocatoriain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoconvocatoriain` AS (select `convocatoria`.`Cod_convocatoria` AS `Cod_convocatoria`,`convocatoria`.`Descripcion` AS `Descripcion`,`convocatoria`.`fkcod_estado` AS `fkcod_estado` from `convocatoria` where (`convocatoria`.`fkcod_estado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocursoac`
--

/*!50001 DROP TABLE IF EXISTS `estadocursoac`*/;
/*!50001 DROP VIEW IF EXISTS `estadocursoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocursoac` AS (select `curso`.`Cod_curso` AS `Cod_curso`,`curso`.`Nombre_curso` AS `Nombre_curso`,`curso`.`Maximo_alumnos` AS `Maximo_alumnos`,`curso`.`Minimo_creditos_troncales` AS `Minimo_creditos_troncales`,`curso`.`Minimo_creditos_optativos` AS `Minimo_creditos_optativos`,`curso`.`fkcodestado` AS `fkcodestado`,`curso`.`fkcodestadocurso` AS `fkcodestadocurso` from `curso` where (`curso`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocursoaplazado`
--

/*!50001 DROP TABLE IF EXISTS `estadocursoaplazado`*/;
/*!50001 DROP VIEW IF EXISTS `estadocursoaplazado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocursoaplazado` AS (select `curso`.`Cod_curso` AS `Cod_curso`,`curso`.`Nombre_curso` AS `Nombre_curso`,`curso`.`Maximo_alumnos` AS `Maximo_alumnos`,`curso`.`Minimo_creditos_troncales` AS `Minimo_creditos_troncales`,`curso`.`Minimo_creditos_optativos` AS `Minimo_creditos_optativos`,`curso`.`fkcodestado` AS `fkcodestado`,`curso`.`fkcodestadocurso` AS `fkcodestadocurso` from `curso` where (`curso`.`fkcodestadocurso` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocursoenformacion`
--

/*!50001 DROP TABLE IF EXISTS `estadocursoenformacion`*/;
/*!50001 DROP VIEW IF EXISTS `estadocursoenformacion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocursoenformacion` AS (select `curso`.`Cod_curso` AS `Cod_curso`,`curso`.`Nombre_curso` AS `Nombre_curso`,`curso`.`Maximo_alumnos` AS `Maximo_alumnos`,`curso`.`Minimo_creditos_troncales` AS `Minimo_creditos_troncales`,`curso`.`Minimo_creditos_optativos` AS `Minimo_creditos_optativos`,`curso`.`fkcodestado` AS `fkcodestado`,`curso`.`fkcodestadocurso` AS `fkcodestadocurso` from `curso` where (`curso`.`fkcodestadocurso` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocursoin`
--

/*!50001 DROP TABLE IF EXISTS `estadocursoin`*/;
/*!50001 DROP VIEW IF EXISTS `estadocursoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocursoin` AS (select `curso`.`Cod_curso` AS `Cod_curso`,`curso`.`Nombre_curso` AS `Nombre_curso`,`curso`.`Maximo_alumnos` AS `Maximo_alumnos`,`curso`.`Minimo_creditos_troncales` AS `Minimo_creditos_troncales`,`curso`.`Minimo_creditos_optativos` AS `Minimo_creditos_optativos`,`curso`.`fkcodestado` AS `fkcodestado`,`curso`.`fkcodestadocurso` AS `fkcodestadocurso` from `curso` where (`curso`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocursosuspendido`
--

/*!50001 DROP TABLE IF EXISTS `estadocursosuspendido`*/;
/*!50001 DROP VIEW IF EXISTS `estadocursosuspendido`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocursosuspendido` AS (select `curso`.`Cod_curso` AS `Cod_curso`,`curso`.`Nombre_curso` AS `Nombre_curso`,`curso`.`Maximo_alumnos` AS `Maximo_alumnos`,`curso`.`Minimo_creditos_troncales` AS `Minimo_creditos_troncales`,`curso`.`Minimo_creditos_optativos` AS `Minimo_creditos_optativos`,`curso`.`fkcodestado` AS `fkcodestado`,`curso`.`fkcodestadocurso` AS `fkcodestadocurso` from `curso` where (`curso`.`fkcodestadocurso` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadocursoterminado`
--

/*!50001 DROP TABLE IF EXISTS `estadocursoterminado`*/;
/*!50001 DROP VIEW IF EXISTS `estadocursoterminado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadocursoterminado` AS (select `curso`.`Cod_curso` AS `Cod_curso`,`curso`.`Nombre_curso` AS `Nombre_curso`,`curso`.`Maximo_alumnos` AS `Maximo_alumnos`,`curso`.`Minimo_creditos_troncales` AS `Minimo_creditos_troncales`,`curso`.`Minimo_creditos_optativos` AS `Minimo_creditos_optativos`,`curso`.`fkcodestado` AS `fkcodestado`,`curso`.`fkcodestadocurso` AS `fkcodestadocurso` from `curso` where (`curso`.`fkcodestadocurso` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadodepartamentoac`
--

/*!50001 DROP TABLE IF EXISTS `estadodepartamentoac`*/;
/*!50001 DROP VIEW IF EXISTS `estadodepartamentoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadodepartamentoac` AS (select `departamento`.`cod_departamento` AS `cod_departamento`,`departamento`.`Nombre` AS `Nombre`,`departamento`.`fkcodpais` AS `fkcodpais`,`departamento`.`fkcodestado` AS `fkcodestado` from `departamento` where (`departamento`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadodepartamentoin`
--

/*!50001 DROP TABLE IF EXISTS `estadodepartamentoin`*/;
/*!50001 DROP VIEW IF EXISTS `estadodepartamentoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadodepartamentoin` AS (select `departamento`.`cod_departamento` AS `cod_departamento`,`departamento`.`Nombre` AS `Nombre`,`departamento`.`fkcodpais` AS `fkcodpais`,`departamento`.`fkcodestado` AS `fkcodestado` from `departamento` where (`departamento`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoempleadoac`
--

/*!50001 DROP TABLE IF EXISTS `estadoempleadoac`*/;
/*!50001 DROP VIEW IF EXISTS `estadoempleadoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoempleadoac` AS (select `empleado`.`Cod_empleado` AS `Cod_empleado`,`empleado`.`Documento_empleado` AS `Documento_empleado`,`empleado`.`Nombre_empleado` AS `Nombre_empleado`,`empleado`.`Apellido_empleado` AS `Apellido_empleado`,`empleado`.`Telefono_empleado` AS `Telefono_empleado`,`empleado`.`Correo_empleado` AS `Correo_empleado`,`empleado`.`fkcodciudad` AS `fkcodciudad`,`empleado`.`fkcodestado` AS `fkcodestado`,`empleado`.`fkcodestadoemp` AS `fkcodestadoemp`,`empleado`.`fkcodtipodoc` AS `fkcodtipodoc` from `empleado` where (`empleado`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoempleadocontratado`
--

/*!50001 DROP TABLE IF EXISTS `estadoempleadocontratado`*/;
/*!50001 DROP VIEW IF EXISTS `estadoempleadocontratado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoempleadocontratado` AS (select `empleado`.`Cod_empleado` AS `Cod_empleado`,`empleado`.`Documento_empleado` AS `Documento_empleado`,`empleado`.`Nombre_empleado` AS `Nombre_empleado`,`empleado`.`Apellido_empleado` AS `Apellido_empleado`,`empleado`.`Telefono_empleado` AS `Telefono_empleado`,`empleado`.`Correo_empleado` AS `Correo_empleado`,`empleado`.`fkcodciudad` AS `fkcodciudad`,`empleado`.`fkcodestado` AS `fkcodestado`,`empleado`.`fkcodestadoemp` AS `fkcodestadoemp`,`empleado`.`fkcodtipodoc` AS `fkcodtipodoc` from `empleado` where (`empleado`.`fkcodestadoemp` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoempleadodespedido`
--

/*!50001 DROP TABLE IF EXISTS `estadoempleadodespedido`*/;
/*!50001 DROP VIEW IF EXISTS `estadoempleadodespedido`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoempleadodespedido` AS (select `empleado`.`Cod_empleado` AS `Cod_empleado`,`empleado`.`Documento_empleado` AS `Documento_empleado`,`empleado`.`Nombre_empleado` AS `Nombre_empleado`,`empleado`.`Apellido_empleado` AS `Apellido_empleado`,`empleado`.`Telefono_empleado` AS `Telefono_empleado`,`empleado`.`Correo_empleado` AS `Correo_empleado`,`empleado`.`fkcodciudad` AS `fkcodciudad`,`empleado`.`fkcodestado` AS `fkcodestado`,`empleado`.`fkcodestadoemp` AS `fkcodestadoemp`,`empleado`.`fkcodtipodoc` AS `fkcodtipodoc` from `empleado` where (`empleado`.`fkcodestadoemp` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoempleadoin`
--

/*!50001 DROP TABLE IF EXISTS `estadoempleadoin`*/;
/*!50001 DROP VIEW IF EXISTS `estadoempleadoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoempleadoin` AS (select `empleado`.`Cod_empleado` AS `Cod_empleado`,`empleado`.`Documento_empleado` AS `Documento_empleado`,`empleado`.`Nombre_empleado` AS `Nombre_empleado`,`empleado`.`Apellido_empleado` AS `Apellido_empleado`,`empleado`.`Telefono_empleado` AS `Telefono_empleado`,`empleado`.`Correo_empleado` AS `Correo_empleado`,`empleado`.`fkcodciudad` AS `fkcodciudad`,`empleado`.`fkcodestado` AS `fkcodestado`,`empleado`.`fkcodestadoemp` AS `fkcodestadoemp`,`empleado`.`fkcodtipodoc` AS `fkcodtipodoc` from `empleado` where (`empleado`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoempleadoincapacitado`
--

/*!50001 DROP TABLE IF EXISTS `estadoempleadoincapacitado`*/;
/*!50001 DROP VIEW IF EXISTS `estadoempleadoincapacitado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoempleadoincapacitado` AS (select `empleado`.`Cod_empleado` AS `Cod_empleado`,`empleado`.`Documento_empleado` AS `Documento_empleado`,`empleado`.`Nombre_empleado` AS `Nombre_empleado`,`empleado`.`Apellido_empleado` AS `Apellido_empleado`,`empleado`.`Telefono_empleado` AS `Telefono_empleado`,`empleado`.`Correo_empleado` AS `Correo_empleado`,`empleado`.`fkcodciudad` AS `fkcodciudad`,`empleado`.`fkcodestado` AS `fkcodestado`,`empleado`.`fkcodestadoemp` AS `fkcodestadoemp`,`empleado`.`fkcodtipodoc` AS `fkcodtipodoc` from `empleado` where (`empleado`.`fkcodestadoemp` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoempleadovacaciones`
--

/*!50001 DROP TABLE IF EXISTS `estadoempleadovacaciones`*/;
/*!50001 DROP VIEW IF EXISTS `estadoempleadovacaciones`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoempleadovacaciones` AS (select `empleado`.`Cod_empleado` AS `Cod_empleado`,`empleado`.`Documento_empleado` AS `Documento_empleado`,`empleado`.`Nombre_empleado` AS `Nombre_empleado`,`empleado`.`Apellido_empleado` AS `Apellido_empleado`,`empleado`.`Telefono_empleado` AS `Telefono_empleado`,`empleado`.`Correo_empleado` AS `Correo_empleado`,`empleado`.`fkcodciudad` AS `fkcodciudad`,`empleado`.`fkcodestado` AS `fkcodestado`,`empleado`.`fkcodestadoemp` AS `fkcodestadoemp`,`empleado`.`fkcodtipodoc` AS `fkcodtipodoc` from `empleado` where (`empleado`.`fkcodestadoemp` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoepsac`
--

/*!50001 DROP TABLE IF EXISTS `estadoepsac`*/;
/*!50001 DROP VIEW IF EXISTS `estadoepsac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoepsac` AS (select `eps`.`Cod_eps` AS `Cod_eps`,`eps`.`Nombre` AS `Nombre`,`eps`.`fkcodestado` AS `fkcodestado` from `eps` where (`eps`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadoepsin`
--

/*!50001 DROP TABLE IF EXISTS `estadoepsin`*/;
/*!50001 DROP VIEW IF EXISTS `estadoepsin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadoepsin` AS (select `eps`.`Cod_eps` AS `Cod_eps`,`eps`.`Nombre` AS `Nombre`,`eps`.`fkcodestado` AS `fkcodestado` from `eps` where (`eps`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadogrupoac`
--

/*!50001 DROP TABLE IF EXISTS `estadogrupoac`*/;
/*!50001 DROP VIEW IF EXISTS `estadogrupoac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadogrupoac` AS (select `grupo`.`Cod_grupo` AS `Cod_grupo`,`grupo`.`Num_max_alumnos` AS `Num_max_alumnos`,`grupo`.`fkcodjornada` AS `fkcodjornada`,`grupo`.`fkcodestadogrupo` AS `fkcodestadogrupo`,`grupo`.`fkcodestado` AS `fkcodestado` from `grupo` where (`grupo`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadogrupoaplazado`
--

/*!50001 DROP TABLE IF EXISTS `estadogrupoaplazado`*/;
/*!50001 DROP VIEW IF EXISTS `estadogrupoaplazado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadogrupoaplazado` AS (select `grupo`.`Cod_grupo` AS `Cod_grupo`,`grupo`.`Num_max_alumnos` AS `Num_max_alumnos`,`grupo`.`fkcodjornada` AS `fkcodjornada`,`grupo`.`fkcodestadogrupo` AS `fkcodestadogrupo`,`grupo`.`fkcodestado` AS `fkcodestado` from `grupo` where (`grupo`.`fkcodestadogrupo` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadogrupocancelado`
--

/*!50001 DROP TABLE IF EXISTS `estadogrupocancelado`*/;
/*!50001 DROP VIEW IF EXISTS `estadogrupocancelado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadogrupocancelado` AS (select `grupo`.`Cod_grupo` AS `Cod_grupo`,`grupo`.`Num_max_alumnos` AS `Num_max_alumnos`,`grupo`.`fkcodjornada` AS `fkcodjornada`,`grupo`.`fkcodestadogrupo` AS `fkcodestadogrupo`,`grupo`.`fkcodestado` AS `fkcodestado` from `grupo` where (`grupo`.`fkcodestadogrupo` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadogrupoenformacion`
--

/*!50001 DROP TABLE IF EXISTS `estadogrupoenformacion`*/;
/*!50001 DROP VIEW IF EXISTS `estadogrupoenformacion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadogrupoenformacion` AS (select `grupo`.`Cod_grupo` AS `Cod_grupo`,`grupo`.`Num_max_alumnos` AS `Num_max_alumnos`,`grupo`.`fkcodjornada` AS `fkcodjornada`,`grupo`.`fkcodestadogrupo` AS `fkcodestadogrupo`,`grupo`.`fkcodestado` AS `fkcodestado` from `grupo` where (`grupo`.`fkcodestadogrupo` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadogrupoin`
--

/*!50001 DROP TABLE IF EXISTS `estadogrupoin`*/;
/*!50001 DROP VIEW IF EXISTS `estadogrupoin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadogrupoin` AS (select `grupo`.`Cod_grupo` AS `Cod_grupo`,`grupo`.`Num_max_alumnos` AS `Num_max_alumnos`,`grupo`.`fkcodjornada` AS `fkcodjornada`,`grupo`.`fkcodestadogrupo` AS `fkcodestadogrupo`,`grupo`.`fkcodestado` AS `fkcodestado` from `grupo` where (`grupo`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadogrupoterminado`
--

/*!50001 DROP TABLE IF EXISTS `estadogrupoterminado`*/;
/*!50001 DROP VIEW IF EXISTS `estadogrupoterminado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadogrupoterminado` AS (select `grupo`.`Cod_grupo` AS `Cod_grupo`,`grupo`.`Num_max_alumnos` AS `Num_max_alumnos`,`grupo`.`fkcodjornada` AS `fkcodjornada`,`grupo`.`fkcodestadogrupo` AS `fkcodestadogrupo`,`grupo`.`fkcodestado` AS `fkcodestado` from `grupo` where (`grupo`.`fkcodestadogrupo` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadohistorialac`
--

/*!50001 DROP TABLE IF EXISTS `estadohistorialac`*/;
/*!50001 DROP VIEW IF EXISTS `estadohistorialac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadohistorialac` AS (select `historial`.`Cod_historial` AS `Cod_historial`,`historial`.`Fecha_historial` AS `Fecha_historial`,`historial`.`fkcodestado` AS `fkcodestado`,`historial`.`fkdocalumno` AS `fkdocalumno` from `historial` where (`historial`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadohistorialin`
--

/*!50001 DROP TABLE IF EXISTS `estadohistorialin`*/;
/*!50001 DROP VIEW IF EXISTS `estadohistorialin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadohistorialin` AS (select `historial`.`Cod_historial` AS `Cod_historial`,`historial`.`Fecha_historial` AS `Fecha_historial`,`historial`.`fkcodestado` AS `fkcodestado`,`historial`.`fkdocalumno` AS `fkdocalumno` from `historial` where (`historial`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadomatriculaac`
--

/*!50001 DROP TABLE IF EXISTS `estadomatriculaac`*/;
/*!50001 DROP VIEW IF EXISTS `estadomatriculaac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadomatriculaac` AS (select `matricula`.`Cod_matricula` AS `Cod_matricula`,`matricula`.`Descripcion` AS `Descripcion`,`matricula`.`fkcodestado` AS `fkcodestado`,`matricula`.`fkcodestadomatricula` AS `fkcodestadomatricula` from `matricula` where (`matricula`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadomatriculacancelada`
--

/*!50001 DROP TABLE IF EXISTS `estadomatriculacancelada`*/;
/*!50001 DROP VIEW IF EXISTS `estadomatriculacancelada`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadomatriculacancelada` AS (select `matricula`.`Cod_matricula` AS `Cod_matricula`,`matricula`.`Descripcion` AS `Descripcion`,`matricula`.`fkcodestado` AS `fkcodestado`,`matricula`.`fkcodestadomatricula` AS `fkcodestadomatricula` from `matricula` where (`matricula`.`fkcodestadomatricula` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadomatriculadenegado`
--

/*!50001 DROP TABLE IF EXISTS `estadomatriculadenegado`*/;
/*!50001 DROP VIEW IF EXISTS `estadomatriculadenegado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadomatriculadenegado` AS (select `matricula`.`Cod_matricula` AS `Cod_matricula`,`matricula`.`Descripcion` AS `Descripcion`,`matricula`.`fkcodestado` AS `fkcodestado`,`matricula`.`fkcodestadomatricula` AS `fkcodestadomatricula` from `matricula` where (`matricula`.`fkcodestadomatricula` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadomatriculaenproceso`
--

/*!50001 DROP TABLE IF EXISTS `estadomatriculaenproceso`*/;
/*!50001 DROP VIEW IF EXISTS `estadomatriculaenproceso`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadomatriculaenproceso` AS (select `matricula`.`Cod_matricula` AS `Cod_matricula`,`matricula`.`Descripcion` AS `Descripcion`,`matricula`.`fkcodestado` AS `fkcodestado`,`matricula`.`fkcodestadomatricula` AS `fkcodestadomatricula` from `matricula` where (`matricula`.`fkcodestadomatricula` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadomatriculain`
--

/*!50001 DROP TABLE IF EXISTS `estadomatriculain`*/;
/*!50001 DROP VIEW IF EXISTS `estadomatriculain`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadomatriculain` AS (select `matricula`.`Cod_matricula` AS `Cod_matricula`,`matricula`.`Descripcion` AS `Descripcion`,`matricula`.`fkcodestado` AS `fkcodestado`,`matricula`.`fkcodestadomatricula` AS `fkcodestadomatricula` from `matricula` where (`matricula`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadomatriculamatriculado`
--

/*!50001 DROP TABLE IF EXISTS `estadomatriculamatriculado`*/;
/*!50001 DROP VIEW IF EXISTS `estadomatriculamatriculado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadomatriculamatriculado` AS (select `matricula`.`Cod_matricula` AS `Cod_matricula`,`matricula`.`Descripcion` AS `Descripcion`,`matricula`.`fkcodestado` AS `fkcodestado`,`matricula`.`fkcodestadomatricula` AS `fkcodestadomatricula` from `matricula` where (`matricula`.`fkcodestadomatricula` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadopaisac`
--

/*!50001 DROP TABLE IF EXISTS `estadopaisac`*/;
/*!50001 DROP VIEW IF EXISTS `estadopaisac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadopaisac` AS (select `pais`.`Cod_pais` AS `Cod_pais`,`pais`.`Nombre` AS `Nombre`,`pais`.`fkcodestado` AS `fkcodestado` from `pais` where (`pais`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadopaisin`
--

/*!50001 DROP TABLE IF EXISTS `estadopaisin`*/;
/*!50001 DROP VIEW IF EXISTS `estadopaisin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadopaisin` AS (select `pais`.`Cod_pais` AS `Cod_pais`,`pais`.`Nombre` AS `Nombre`,`pais`.`fkcodestado` AS `fkcodestado` from `pais` where (`pais`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadotitulacionac`
--

/*!50001 DROP TABLE IF EXISTS `estadotitulacionac`*/;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadotitulacionac` AS (select `titulacion`.`Cod_titulacion` AS `Cod_titulacion`,`titulacion`.`Numero_creditos` AS `Numero_creditos`,`titulacion`.`Carga_lectiva` AS `Carga_lectiva`,`titulacion`.`fkcodcampus` AS `fkcodcampus`,`titulacion`.`fkcodestadotitulacion` AS `fkcodestadotitulacion`,`titulacion`.`fkcodestado` AS `fkcodestado` from `titulacion` where (`titulacion`.`fkcodestado` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadotitulacionaplazado`
--

/*!50001 DROP TABLE IF EXISTS `estadotitulacionaplazado`*/;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionaplazado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadotitulacionaplazado` AS (select `titulacion`.`Cod_titulacion` AS `Cod_titulacion`,`titulacion`.`Numero_creditos` AS `Numero_creditos`,`titulacion`.`Carga_lectiva` AS `Carga_lectiva`,`titulacion`.`fkcodcampus` AS `fkcodcampus`,`titulacion`.`fkcodestadotitulacion` AS `fkcodestadotitulacion`,`titulacion`.`fkcodestado` AS `fkcodestado` from `titulacion` where (`titulacion`.`fkcodestadotitulacion` = 3)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadotitulacioncancelado`
--

/*!50001 DROP TABLE IF EXISTS `estadotitulacioncancelado`*/;
/*!50001 DROP VIEW IF EXISTS `estadotitulacioncancelado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadotitulacioncancelado` AS (select `titulacion`.`Cod_titulacion` AS `Cod_titulacion`,`titulacion`.`Numero_creditos` AS `Numero_creditos`,`titulacion`.`Carga_lectiva` AS `Carga_lectiva`,`titulacion`.`fkcodcampus` AS `fkcodcampus`,`titulacion`.`fkcodestadotitulacion` AS `fkcodestadotitulacion`,`titulacion`.`fkcodestado` AS `fkcodestado` from `titulacion` where (`titulacion`.`fkcodestadotitulacion` = 4)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadotitulacioncertificado`
--

/*!50001 DROP TABLE IF EXISTS `estadotitulacioncertificado`*/;
/*!50001 DROP VIEW IF EXISTS `estadotitulacioncertificado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadotitulacioncertificado` AS (select `titulacion`.`Cod_titulacion` AS `Cod_titulacion`,`titulacion`.`Numero_creditos` AS `Numero_creditos`,`titulacion`.`Carga_lectiva` AS `Carga_lectiva`,`titulacion`.`fkcodcampus` AS `fkcodcampus`,`titulacion`.`fkcodestadotitulacion` AS `fkcodestadotitulacion`,`titulacion`.`fkcodestado` AS `fkcodestado` from `titulacion` where (`titulacion`.`fkcodestadotitulacion` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadotitulacionenproceso`
--

/*!50001 DROP TABLE IF EXISTS `estadotitulacionenproceso`*/;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionenproceso`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadotitulacionenproceso` AS (select `titulacion`.`Cod_titulacion` AS `Cod_titulacion`,`titulacion`.`Numero_creditos` AS `Numero_creditos`,`titulacion`.`Carga_lectiva` AS `Carga_lectiva`,`titulacion`.`fkcodcampus` AS `fkcodcampus`,`titulacion`.`fkcodestadotitulacion` AS `fkcodestadotitulacion`,`titulacion`.`fkcodestado` AS `fkcodestado` from `titulacion` where (`titulacion`.`fkcodestadotitulacion` = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estadotitulacionin`
--

/*!50001 DROP TABLE IF EXISTS `estadotitulacionin`*/;
/*!50001 DROP VIEW IF EXISTS `estadotitulacionin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp850 */;
/*!50001 SET character_set_results     = cp850 */;
/*!50001 SET collation_connection      = cp850_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `estadotitulacionin` AS (select `titulacion`.`Cod_titulacion` AS `Cod_titulacion`,`titulacion`.`Numero_creditos` AS `Numero_creditos`,`titulacion`.`Carga_lectiva` AS `Carga_lectiva`,`titulacion`.`fkcodcampus` AS `fkcodcampus`,`titulacion`.`fkcodestadotitulacion` AS `fkcodestadotitulacion`,`titulacion`.`fkcodestado` AS `fkcodestado` from `titulacion` where (`titulacion`.`fkcodestado` = 2)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-13 22:23:34
